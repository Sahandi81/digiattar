<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetermineTemperamentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('determine_temperaments', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('temperament_id_answer_1');
            $table->integer('temperament_id_answer_2');
            $table->integer('temperament_id_answer_3');
            $table->integer('temperament_id_answer_4');
            $table->integer('temperament_id_answer_5')->default(0);
            $table->integer('temperament_id_answer_6')->default(0);
            $table->integer('temperament_id_answer_7')->default(0);
            $table->integer('temperament_id_answer_8')->default(0);
            $table->integer('temperament_id_answer_9')->default(0);
            $table->integer('temperament_id_answer_10')->default(0);
            $table->integer('temperament_id_answer_11')->default(0);
            $table->integer('temperament_id_answer_12')->default(0);
            $table->integer('temperament_id_answer_13')->default(0);
            $table->integer('temperament_id_answer_14')->default(0);
            $table->integer('temperament_id_answer_15')->default(0);
            $table->integer('temperament_id_answer_16')->default(0);
            $table->integer('user_id');
            $table->dateTime('created_at');
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->integer('percent_cold');
            $table->integer('percent_hot');
            $table->integer('percent_dry');
            $table->integer('percent_more');
            $table->integer('record_id');
            $table->string('token', 50)->unique('token');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('determine_temperaments');
    }
}
