<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderQueueJobTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_queue_job', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('marketer_id')->index('marketer_id');
            $table->integer('order_id')->index('order_id');
            $table->integer('period_id')->index('period_id');
            $table->integer('price')->nullable();
            $table->integer('level');
            $table->boolean('buyer')->default(false);
            $table->boolean('status')->default(false);
            $table->integer('personal_sales')->nullable()->default(0);
            $table->integer('group_sales')->nullable()->default(0);
            $table->dateTime('created_at')->nullable();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_queue_job');
    }
}
