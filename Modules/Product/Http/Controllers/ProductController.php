<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Modules\Order\Entities\OrderProductPins;
use Modules\Product\Entities\PackageType;
use Modules\Product\Entities\Pins;
use Modules\Product\Entities\PriceParameter;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductRelatedToProduct;
use Modules\Product\Entities\RelatedProducts;
use Modules\Product\Entities\Types;
use Modules\Region\Entities\Region;

class ProductController extends Controller
{
    public function index(Request $request)
    {

        $entities = Product::with(['brand', 'packageType', 'types'])->where(function ($q) use($request) {

            if ($request->has('filter')) {

                $filter = json_decode($request->get('filter'), true);

                if (isset($filter['title'])) {
                    $q->where('title', 'like', '%' . $filter['title'] . '%');
                }

                if (isset($filter['id']) && $filter['id'] > 0) {
                    $q->where('id', '=', $filter['id']);
                }

                if (isset($filter['code'])) {
                    $q->where('code', 'like', '%' . $filter['code'] . '%');
                }


                if (isset($filter['count']) && $filter['count'] != -1) {
                    if ($filter['count'] == 1) {
                        $q->where('count', '>', 0);
                    } else {
                        $q->where('count', '=', 0);
                    }
                }

                if (isset($filter['discount']) && $filter['discount'] != -1) {
                    if ($filter['discount'] == 1) {
                        $q->where('discount', '>', 0);
                    } else {
                        $q->where('discount', '=', 0);
                    }
                }

                if (isset($filter['status']) && $filter['status'] != -1) {
                    $q->where('status', $filter['status']);
                }

                if (isset($filter['brand_id']) && $filter['brand_id'] != -1) {
                    $q->where('brand_id', $filter['brand_id']);
                }

                if (isset($filter['package_type_id']) && $filter['package_type_id'] != -1) {
                    $q->where('package_type_id', $filter['package_type_id']);
                }
            }

        })
            ->orderBy($request->get('sort_field') ?? 'id', $request->get('sort_type') ?? 'desc');


        if ($request->has('excel_export') && $request->get('excel_export') == true) {
            $entities = $entities->get();
        } else {
            $entities = $entities->paginate($request->get('limit') ?? 50);
        }
        return response($entities);

    }


	public function show($id,Request $request)
	{

		$data = Cache::remember("product[$id]", 1 * 60, function () use ($id, $request) {

			$product = \Modules\Product\Entities\Product::with(['brand', 'packageType', 'files' => function ($q) {
				$q->orderBy('collection');
				$q->orderBy('order');
			}])
				->with(['comments' => function ($q) {

					$q->where('verify', 1);
					$q->with('showUser');
				}])
				->with(['newRelatedProduct'=> function($q){
						$q->with(['types' => function($q) {;
					}]);
				}])
				->with(['categories' => function ($q) use ($request) {
					if ($request->categories) {
						$q->where('id', $request->categories);
					} else {
						$q->first();
					}
				}])
				->find($id);

			$attributes = \Illuminate\Support\Facades\DB::select('call sp_frontend_product_attributes(?)', [$id]);
			$similar = \Illuminate\Support\Facades\DB::select('call sp_frontend_product_family(?)', [$id]);

			if (count($similar) == 1 && !$similar[0]->id) {
				$similar_data = [];
			} else {
				foreach ($similar as $item) {
					$similar_data[] = [
						'id' => $item->id,
						'title' => $item->title,
						'types' => json_decode($item->types),
						'slug' => $item->slug
					];
				}
			}

			return [
				'product' => $product,
				'attributes' => $attributes,
				'similar' => $similar_data,
			];

		});

		return response($data);
	}




    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function initShow($id, Request $request)
    {

        $result = Product::with(['priceParameters'])->find($id);


        if ($result) {

            return response([
                'title' =>  $result->title,
                'heading' =>  $result->heading ?? '',
                'code' =>  $result->code,
                'brand_id' =>  $result->brand_id,
                'package_type_id' =>  $result->package_type_id,
                'brand' => $result->brand,
                'package_type' => $result->packageType,
                'price_parameters' => $result->priceParameters
            ]);
        }

        return response()->json(['status' => false, 'msg' => 'request is invalid'], 200);
    }


    /**
     * @param $id
     * @param Request $request
     */
    public function initUpdate($id, Request $request)
    {

        $request->validate([
           'title'              => 'required|string',
           'brand_id'           => 'required|string',
           'package_type_id'    => 'required|string',
           'heading'            => 'string',
           'code'               => 'string|nullable',
           'price_parameters'	=> 'required|array',
        ]);

        $result = Product::updateOrCreate(['id' => $id] ,[
            'slug' 				=>  $request->get('title') == '' ? null : remove_special_char($request->get('title')),
            'title' 			=>  $request->get('title'),
            'heading' 			=>  $request->get('heading') ?? '',
            'code' 				=>  $request->get('code'),
            'brand_id' 			=>  $request->get('brand_id'),
            'package_type_id' 	=>  $request->get('package_type_id'),
        ]);


        if ($request->has('price_parameters')) {
            $result->priceParameters()->sync($request->get('price_parameters'));
        }


        if ($result) {
            return response()->json(['status' => true, 'msg' => 'عملیات موفقیت امیز بود.', 'model' => $result]);
        }

        return response()->json(['status' => false, 'msg' => 'un success'], 200);


    }

    public function initStore(Request $request)
    {

         $validator = Validator::make($request->all(), [
            'title' 						=> 'required',
            'package_type_id' 				=> 'required',
			'related'						=> 'array|min:1',
			'related.*'						=> 'array|min:1',
            'related.*.product_id'			=> 'numeric|required_with:related.*|exists:product,id',
            'related.*.product_type_id' 	=> 'numeric',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg'  =>  $validator->errors()->first()]);
        }

        if ($request->get('package_type_id') == -1) {
            return response()->json(['status' => false, 'msg'  =>  'واحد را وارد کنید.']);
        }

        if ($request->get('code') && $request->get('code') != "") {
            $code = Product::where('code', $request->get('code'))->count();
            if ($code > 0) {
                return Response()->json(['status' => false, 'msg' => 'کد قبلا ثبت شده است.']);
            }
        }

        $slug = Product::where('slug', remove_special_char($request->get('title')))->count();
        if ($slug > 0) {
            return Response()->json(['status' => false, 'msg' => 'اسلاگ قبلا ثبت شده است.']);
        }


        $model = Product::firstOrCreate([
            'created_by' 			=> Auth::id(),
            'title' 				=> $request->get('title'),
            'heading' 				=> $request->get('heading'),
            'package_type_id' 		=> $request->get('package_type_id'),
            'brand_id' 				=> $request->get('brand_id'),
            'code' 					=> $request->get('code'),
            'slug' 					=> $request->get('slug') == '' ? remove_special_char($request->get('title')) : remove_special_char($request->get('slug')),
            'meta_title' 			=> $request->get('meta_title') ?? $request->get('title'),
            'meta_description' 		=> $request->get('meta_description') ?? $request->get('title'),
        ]);


		$fields = $validator->validated();
		if (isset($fields['related'])){
			$related = [];
			foreach ($fields['related'] as $item){
				$related[] = [
					'product_id' 			=> $model->id,
					'related_product_id' 	=> $item['product_id'],
					'product_type_id' 		=> $item['product_type_id'] ?? null,
				];
			}
			ProductRelatedToProduct::insert($related);
		}


        if ($request->has('price_parameters')) {
            $model->priceParameters()->sync($request->get('price_parameters'));
        }

        if ($model) {
            return response(['status' => true, 'model' => $model]);
        }

    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function status($id, Request $request)
    {

        $model = Product::find($id);

        if ($request->get('field') == 'status') {

            $model = $model->update([
                'status' => $model->status ? 0 : 1
            ]);

        }

        if ($model) {

            return Response()->json(['status' => true, 'msg' => 'عملیات موفقیت آمیز بود.']);
        }
        return Response()->json(['status' => false, 'msg' => 'خطایی رخ داده است.']);
    }



    public function productTypes($id, Request $request)
    {

        // Get All Price Parameters
        $price_parameters = [];


        $list = [
            [
                'id' => '',
                'title' => '',
                'price' => 0,
                'discount' => 0,
                'tax' => 0,
                'weight' => 0,
                'per_count' => 1,
                'point' => 0,
                'order_point' => 0,
                'status' => 1,
                'count' => 0,
                'price_parameters' => []
            ]
        ];

        $product = Product::with(['packageType', 'types' => function($q){

        }])->find($id);

		return $product;

        foreach ($product->priceParameters as $parameter) {
            $price_parameters[] = [
                'id' => $parameter->id,
                'title' => $parameter->title,
                'children' => PriceParameter::whereDescendantOf($parameter->id)->get(),
                'selected' => '',
            ];
        }

        $list[0]['price_parameters'] = $price_parameters;


        if (isset($product->types) && count($product->types) > 0) {

            foreach ($product->types as $key => $pins) {

                $list[$key] = [
                    'id' => $pins['id'],
                    'title' => $pins['title'],
                    'price' => $pins['price'],
                    'discount' => $pins['discount'],
                    'weight' => $pins['weight'],
                    'tax' => $pins['tax'],
                    'per_count' => $pins['per_count'],
                    'point' => $pins['point'],
                    'order_point' => $pins['order_point'],
                    'status' => $pins['status'],
                    'count' => $pins['count'],
                ];

                foreach ($product->priceParameters as $k => $parameter) {

                    $selected = '';
                    foreach ($pins->priceParameters as $item) {
                        $bool = $item->isDescendantOf(PriceParameter::find($parameter['id']));
                        if ($bool) {
                            $selected = [
                                'id' => $item->id,
                                'title' => $item->title
                            ];
                            break;
                        }
                    }

                    $list[$key]['price_parameters'][] = [
                        'id' => $parameter['id'],
                        'title' => $parameter['title'],
                        'children' => PriceParameter::whereDescendantOf($parameter->id)->get(),
                        'selected' => $selected
                    ];
                }
            }
        }


        return response(['product' => $product, 'list' => $list]);
    }


    public function storeTypes($id, Request $request)
    {

        $product = Product::find($id);

        if ($request->has('pins')) {

            foreach ($request->get('pins') as $pin) {



                try { // for duplicate insert, continue if record repeated

                    $price_parameter = [];

                    $product_pins = Types::updateOrCreate(['id' => $pin['id']], [
                        'title' => $pin['title'],
                        'price' => $pin['price'],
                        'product_id' => $product->id,
                        'off_price' => $pin['price'] * (100 - $pin['discount']) / 100,
                        'discount' => $pin['discount'],
                        'tax' => $pin['tax'],
                        'per_count' => $pin['per_count'],
                        'point' => $pin['point'],
                        'order_point' => $pin['order_point'],
                        'weight' => $pin['weight'],
                        'status' => $pin['status'],
                    ]);

                    if (count($pin['price_parameters']) > 0) { // if record not price parameter
                        foreach ($pin['price_parameters'] as $parameter) {
                            if (count($parameter['selected']) > 0) {
                                if (PriceParameter::where('id', $parameter['selected']['id'])->exists()) {
                                    $price_parameter[] = $parameter['selected']['id'];
                                }
                            } else { // price parameter not selected
                                $product_pins->delete();
                            }
                        }
                    }

                    $product_pins->priceParameters()->sync($price_parameter);

                } catch (\Exception $exception) {
                    return Response()->json(['status' => true, 'msg' => $exception->getMessage()]);
                }
            }

            return Response()->json(['status' => true, 'msg' => 'عملیات موفقیت آمیز بود.']);
        }
    }




    public function productPins($id, Request $request)
    {

        $types = [];
        $product = Product::with(['packageType', 'types'])->find($id);

        foreach ($product->types as $type) {
            $types[] = $type->id;
        }

        $pins = Pins::with(['operator', 'types' => function($q) {
            $q->with('priceParameters');
        }])->where(function ($q) use($request, $types) {
                if ($request->get('product_types_id') && $request->get('product_types_id') != "") {
                    $q->where('product_types_id', $request->get('product_types_id'));
                } else {
                    $q->whereIn('product_types_id', $types);
                }
            })
            ->orderBy('id', 'desc');


        if (($request->has('excel_export') && $request->get('excel_export') == true)) {
            $pins = $pins->get();
        } else {
            $pins = $pins->paginate(50);
        };


        return response(['product' => $product, 'pins' => $pins]);
    }


    public function storePins($id, Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'product_types_id' => 'required',
            'depot_date' => 'required',
            'document_number' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg'  =>  $validator->errors()->first()]);
        }
        Pins::create([
            'operator_id' => Auth::id(),
            'product_types_id' => $request->get('product_types_id') ,
            'inc_count' => $request->get('type') == 'inc_count' ? $request->get('count') : 0,
            'dec_count' => $request->get('type') == 'dec_count' ? $request->get('count') : 0,
            'depot_date' => $request->get('depot_date'),
            'document_number' => $request->get('document_number'),
        ]);


        return response()->json(['status' => true, 'msg' => 'عملیات موفقیت آمیز بود.']);

    }


    public function edit($id, Request $request)
    {
        $product = Product::find($id);

        $product->update([
            'price' => $request->get('price') ? $request->get('price') : 0,
            'order_point' => $request->get('order_point') ? $request->get('order_point') : 0,
            'discount' => $request->get('discount') ?  $request->get('discount') : 0,
            'is_tax' => $request->get('is_tax') == '' ? 0 : $request->get('is_tax'),
            'weight' => $request->get('weight') == '' ? 0 : $request->get('weight')
        ]);


        return response()->json(['status' => true, 'msg' => 'عملیات موفقیت آمیز بود.']);
    }


    public function orderPins(Request  $request)
    {


        $entities = OrderProductPins::select('order_product_pins.product_id',
            DB::raw('sum(order_product_pins.count) as counter'),
            DB::raw('sum(order_product_pins.price) as price'),
            DB::raw('sum(order_product_pins.off_price) as off_price'),
            DB::raw('sum(order_product_pins.tax) as tax')
            )->with(['order' => function($o) {
            $o->with(['user' => function($user) {
                $user->with(['info', 'marketer']);
            },  'period' => function($p) {
                $p->with(['month']);
            }, 'address']);
        }, 'product' => function($p) {
            $p->with(['brand']);
        }])->where(function ($q) use ($request) {

            if ($request->get('product_id') && $request->get('product_id') != "") {
                $q->whereHas('product', function ($product) use($request) {
                    $product->where('id', $request->get('product_id'));
                });
            }

            if ($request->get('code') && $request->get('code') != "") {
                $q->whereHas('product', function ($product) use($request) {
                    $product->where('code', 'like', '%' . $request->get('code') . '%');
                });
            }

            if ($request->get('title') && $request->get('title') != "") {
                $q->whereHas('product', function ($product) use($request) {
                    $product->where('title', 'like', '%' . $request->get('title') . '%');
                });
            }

            if ($request->get('brand_id') && $request->get('brand_id') != "") {
                $q->whereHas('product', function ($product) use($request) {
                    $product->whereHas('brand', function ($brand) use($request) {
                        $brand->where('id', 'like',  $request->get('brand_id'));
                    });
                });
            }

            /**
             * all user filter and marketer
             */

            if ($request->get('user_id') && $request->get('user_id') != "") {
                $q->whereHas('order', function ($order) use($request) {
                    $order->where('user_id', $request->get('user_id'));
                });
            }


            if ($request->get('mobile') && $request->get('mobile') != '') {
                $q->whereHas('order', function ($order) use($request) {
                    $order->whereHas('user', function ($user) use ($request) {
                        $user->where('mobile', 'like', '%' . $request->get('mobile') . '%');
                    });
                });
            }

            if ($request->get('email') && $request->get('email') != '') {
                $q->whereHas('order', function ($order) use($request) {
                    $order->whereHas('user', function ($user) use ($request) {
                        $user->where('email', 'like', '%' . $request->get('email') . '%');
                    });
                });
            }

            if ($request->get('username') && $request->get('username') != '') {
                $q->whereHas('order', function ($order) use($request) {
                    $order->whereHas('user', function ($user) use ($request) {
                        $user->where('username', 'like', '%' . $request->get('username') . '%');
                    });
                });
            }

            if ($request->get('name') && $request->get('name') != '') {
                $q->whereHas('order', function ($order) use($request) {
                    $order->whereHas('user', function ($user) use ($request) {
                        $user->where(DB::raw('concat(name, " ", family)'), 'like', '%' . $request->get('name') . '%');
                    });
                });
            }

            if ($request->get('national_code') && $request->get('national_code') != '') {
                $q->whereHas('order', function ($order) use($request) {
                    $order->whereHas('user', function ($user) use ($request) {
                        $user->whereHas('info', function ($info) use ($request) {
                            $info->where('national_code', 'like', '%' . $request->get('national_code') . '%');
                        });
                    });
                });
            }

            if ($request->get('parent_id') && $request->get('parent_id') != '') {
                $q->whereHas('order', function ($order) use($request) {
                    $order->whereHas('user', function ($user) use ($request) {
                        $user->whereHas('marketer', function ($marketer) use ($request) {
                            $marketer->where('ancestry', 'like', DB::table('marketer')->where('id', $request->get('parent_id'))->value('ancestry') . '%');
                        });
                    });
                });
            }

            /**
             * all filter for order
             */


            if ($request->get('order_id') && $request->get('order_id') != "") {
                $q->where('order_id', $request->get('order_id'));

            }

            if ($request->get('month_id') && $request->get('month_id') != "") {
                $q->whereHas('order', function ($order) use($request) {
                    $order->whereHas('period', function ($period) use ($request) {
                        $period->where('parent_id', $request->get('month_id'));
                    });
                });
            }

            if ($request->get('period_id') && $request->get('period_id') != "") {
                $q->whereHas('order', function ($order) use($request) {
                    $order->where('period_id', $request->get('period_id'));
                });

            }

            if ($request->get('region_id') && $request->get('region_id') != "") {
                $q->whereHas('order', function ($order) use($request) {
                    $order->whereHas('address', function ($address) use ($request) {
                        $output = [];
                        foreach (Region::select('id')->descendantsAndSelf($request->get('region_id'))->toArray() as $item) {
                            $output[] = $item['id'];
                        }

                        $address->whereIn('region_id', $output);
                    });
                });
            }

            if ($request->get('from_date') && $request->get('to_date') && $request->get('from_date') != "" && $request->get('to_date') != "") {
                $q->whereHas('order', function ($order) use($request) {
                    $order->where('created_at', '>=', $request->get('from_date'));
                    $order->where('created_at', '<=', $request->get('to_date'));
                });
            } elseif ($request->get('from_date')) {
                $q->whereHas('order', function ($order) use($request) {
                    $order->where('created_at', '>=', $request->get('from_date'));
                });
            } elseif ($request->get('to_date')) {
                $q->whereHas('order', function ($order) use($request) {
                    $order->where('created_at', '<=', $request->get('to_date'));
                });
            }


            $q->whereHas('order', function ($order) {
                $order->where('status', 1);
            });

        })->orderBy($request->get('sort_field') ?? 'order_product_pins.product_id', $request->get('sort_type') ?? 'desc')
            ->groupBy('order_product_pins.product_id')
        ;

        if (($request->has('excel_export') && $request->get('excel_export') == true) || ($request->has('print') && $request->get('print') == true)) {
            $entities = $entities->get();
        } else {
            $entities = $entities->paginate($request->get('limit') ?? 100);
        }

        return response($entities);
    }

}
