<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('depot', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('from_order')->nullable();
            $table->integer('to_order')->nullable();
            $table->integer('count')->default(0);
            $table->boolean('accumulative')->default(false);
            $table->boolean('barcode')->default(false);
            $table->boolean('label')->default(false);
            $table->boolean('factor')->default(false);
            $table->boolean('exit')->default(false);
            $table->dateTime('created_at')->nullable();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('depot');
    }
}
