<?php

use Illuminate\Support\Facades\Route;
use Modules\Medical\Http\Controllers\FormQuestionController;
use Modules\Medical\Http\Controllers\MedicalController;
use Modules\Medical\Http\Controllers\VisitFromController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::group(['prefix' => '/form'], function () {

	Route::group(['prefix' => '/question'], function (){
		Route::get('/',						[FormQuestionController::class, 'index'])					->name('get.from.questions');
	});

	Route::group(['prefix' => '/answer', 'middleware' => 'auth:api'], function () {
		Route::post('/',   					[VisitFromController::class, 'store'])						->name('store.visit.from.by.client');
		Route::put('/',   					[VisitFromController::class, 'update'])						->name('update.visit.from.by.client');
	});

	Route::group(['prefix' => 'records', 'middleware' => 'auth:api'], function () {
		Route::get('/',        								[MedicalController::class, 'recordsIndex'])	->name('show.all.client.records');
		Route::get('/{medical_records}',					[MedicalController::class, 'recordShow'])	->name('show.record');
		Route::get('/{medical_records}/{medical_folder}',	[MedicalController::class, 'folderShow'])	->name('show.folder');
		Route::get('/{medical_records}/{medical_folder}/download',[MedicalController::class, 'downloadFrom'])->name('download.form');
//		Route::get('/',      				[VisitFromController::class, 'index'])						->name('show.visit.from.to.client');
	}) ;

});