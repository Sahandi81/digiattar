<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketConversationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_conversation', function (Blueprint $table) {
            $table->integer('id', true);
            $table->unsignedBigInteger('ticket_id')->index('FK_ticket_conversation_ticket');
            $table->unsignedBigInteger('created_by')->nullable()->index('FK_ticket_conversation_users');
            $table->mediumText('content');
            $table->boolean('creator_survey')->nullable();
            $table->dateTime('created_at');
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_conversation');
    }
}
