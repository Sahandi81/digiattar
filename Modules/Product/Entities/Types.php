<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\User\Entities\User;

class Types extends Model
{
	# New update, use `rebuild` api to change columns from deleted to deleted_at
	use SoftDeletes;

    protected $table = 'product_types';

    protected $primaryKey = 'id';

    protected $guarded = [];


    public function priceParameters()
    {
        return $this->belongsToMany(PriceParameter::class, 'product_types_has_price_parameter', 'product_types_id', 'price_parameter_id');
    }

    public function pins()
    {
        return $this->hasMany(Pins::class, 'product_types_id');
    }

    public function collection(){
        return $this->hasOne(ProductCollection::class,'id','product_collection_id');
    }

}
