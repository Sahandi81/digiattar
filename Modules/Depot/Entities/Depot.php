<?php

namespace Modules\Depot\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Depot extends Model
{
	# New update, use `rebuild` api to change columns from deleted to deleted_at
	use SoftDeletes;
    use HasFactory;

    protected $table = 'depot';

    protected $guarded = [];

    protected static function newFactory()
    {
        return \Modules\Depot\Database\factories\DepotFactory::new();
    }
}
