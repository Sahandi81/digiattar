<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commission', function (Blueprint $table) {
            $table->integer('id', true)->comment('for add to finance');
            $table->integer('marketer_id')->index('marketer_id');
            $table->integer('period_id')->index('period_id');
            $table->integer('amount');
            $table->boolean('status')->default(false);
            $table->string('payment_ref', 50)->nullable();
            $table->string('payment_date', 20)->nullable();
            $table->string('payment_text')->nullable();
            $table->string('payment_amount', 50)->nullable();
            $table->boolean('reason')->default(false);
            $table->dateTime('created_at')->nullable();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->softDeletes();

            $table->unique(['marketer_id', 'period_id'], 'marketer_id_period_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commission');
    }
}
