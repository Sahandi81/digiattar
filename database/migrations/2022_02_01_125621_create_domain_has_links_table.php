<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDomainHasLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domain_has_links', function (Blueprint $table) {
            $table->string('domain_key', 50);
            $table->integer('link_id');
            $table->string('value');
            $table->softDeletes();

            $table->primary(['domain_key', 'link_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domain_has_links');
    }
}
