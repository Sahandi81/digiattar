<?php

namespace Modules\FormRequest\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\User\Entities\User;

class FormRequest extends Model
{
	# New update, use `rebuild` api to change columns from deleted to deleted_at
	use SoftDeletes;
    use HasFactory;

    protected $guarded = [];

    protected $table = 'form_request';

    protected static function newFactory()
    {
        return \Modules\FormRequest\Database\factories\FormRequestFactory::new();
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function operator()
    {
        return $this->belongsTo(User::class, 'operator_id');
    }

    public function category()
    {
        return $this->belongsTo(FormCategory::class, 'category_id');
    }
}
