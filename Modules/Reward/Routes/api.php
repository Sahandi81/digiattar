<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Modules\Reward\Http\Controllers\RewardController;
use Modules\User\Entities\User;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('backend')->group(function () {

    Route::prefix('rewards')->middleware(['auth:api', 'admin_access'])->group(function () {

        Route::get('/', 							[RewardController::class, 'index']);
        Route::put('/', 							[RewardController::class, 'deposit']);
        Route::get('/saving', 						[RewardController::class, 'saving']);
        Route::get('/point', 						[RewardController::class, 'point']);
        Route::get('/retail', 						[RewardController::class, 'retail']);
        Route::get('/reagent', 						[RewardController::class, 'reagent']);
        Route::get('/rewardAsReward', 				[RewardController::class, 'rewardAsReward']);
        Route::get('/{marketer}/detail/{period}',	[RewardController::class, 'detail']);

    });
});
