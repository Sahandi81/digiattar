<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('slug')->unique('slug');
            $table->unsignedBigInteger('created_by')->index('FK_blog_content_users');
            $table->string('title');
            $table->string('heading')->nullable();
            $table->string('meta_title');
            $table->string('meta_description');
            $table->longText('content')->nullable();
            $table->mediumText('short_content')->nullable();
            $table->boolean('status')->default(true);
            $table->bigInteger('visitor')->default(0);
            $table->string('img')->nullable();
            $table->boolean('is_blog')->nullable()->default(true);
            $table->boolean('is_private')->nullable()->default(false);
            $table->boolean('is_popup')->nullable()->default(false);
            $table->dateTime('created_at')->nullable();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->softDeletes();
            $table->longText('h2_titles')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content');
    }
}
