<?php

namespace Modules\FormRequest\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class FormCategory extends Model
{
	# New update, use `rebuild` api to change columns from deleted to deleted_at
	use SoftDeletes;
    use HasFactory;

    protected $guarded = [];

    protected $table = 'form_category';

    protected static function newFactory()
    {
        return \Modules\FormRequest\Database\factories\FormCategoryFactory::new();
    }
}
