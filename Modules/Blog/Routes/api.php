<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\Blog\Http\Controllers\BlogController;
use Modules\Blog\Http\Controllers\ContentController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::prefix('/blog')->group(function () {

	Route::prefix('/content')->middleware('access')->group(function (){
		Route::get('/{id}', 					[BlogController::class, 'index']);
	});

});

Route::prefix('backend')->group(function () {

    Route::prefix('blog')->middleware(['auth:api', 'admin_access'])->group(function () {

        Route::group(['prefix' => '/categories'], function () {
            Route::get('/', 'CategoryController@index');
            Route::post('/', 'CategoryController@store');
            Route::get('/{id}', 'CategoryController@show');
            Route::put('/{id}', 'CategoryController@update');
            Route::get('/{id}/findMapState', 'CategoryController@findMapState'); // locations that node has replaced
            Route::put('/{id}/dispatchMove', 'CategoryController@dispachMove');
        });

        Route::group(['prefix' => '/menu'], function () {
            Route::get('/', 'MenuController@index');
            Route::post('/', 'MenuController@store');
            Route::get('/{id}', 'MenuController@show');
            Route::put('/{id}', 'MenuController@update');
            Route::get('/{id}/findMapState', 'MenuController@findMapState'); // locations that node has replace
            Route::put('/{id}/dispatchMove', 'MenuController@dispachMove');
        });

        Route::get('/{id}/categories', function ($id) {
            return response(\Modules\Blog\Entities\Content::find($id)->categories()->get(['id','title']));
        });

        Route::post('/{id}/categories', function ($id, Request $request) {
            $result = \Modules\Blog\Entities\Content::find($id);

            $result->categories()->detach();
            if ($request->categories) {

                $categories_result = [];

                foreach ($request->categories as $category) {

                    $ancestors = \Modules\Blog\Entities\Category::ancestorsAndSelf($category);

                    foreach ($ancestors as $key=>$ancestor) {

                        $is_main = 0;

                        if (count($ancestors) == $key + 1 && count($ancestors) > 1) {
                            $is_main = 1;
                        }

                        $categories_result[$ancestor->id] = [
                            'is_main' => $is_main,
                            'category_id' => $ancestor->id,
                            'content_id' => $id
                        ];
                    }
                }
                $result->categories()->attach($categories_result);
                if ($result) {
                    return response(['status'=> true, 'msg' => 'عملیات موفقیت آمیز']);
                }
            }
            else{
                return response(['status'=> true, 'msg' => 'دسته بندی ها با موفقیت حذف شد']);
            }
        })->middleware('access');


        Route::get('/{id}/seo', function ($id, Request $request) {

            $result = \Modules\Blog\Entities\Content::with(['tags', 'products' => function($q) {
                $q->select('id', 'title');
            }])->find($id);

            if ($result) {

                return response([
                    'slug' =>  $result->slug ?? '',
                    'meta_title' =>  $result->meta_title ?? '',
                    'meta_description' =>  $result->meta_description ?? '',
                    'content' =>  $result->content ?? '',
                    'short_content' =>  $result->short_content ?? '',
					'h2_titles' => $result->h2_titles,
					'tags' => $result->tags,
                    'products' => $result->products
                ]);
            }

            return response()->json(['status' => false, 'msg' => 'request is invalid'], 200);
        });
        Route::put('/{id}/seo', 				[BlogController::class, 'update'])					->name('blog_seo_put');


        Route::post('/init', 				[ContentController::class, 'initStore']);
        Route::put('/{id}/init', 'ContentController@initUpdate');
        Route::get('/{id}/init', 'ContentController@initShow');


        Route::get('/', 					[ContentController::class, 'index'])				->name('blog_index');
        Route::put('/{id}/status', 'ContentController@status');
    });
});
