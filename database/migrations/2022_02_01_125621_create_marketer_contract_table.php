<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarketerContractTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketer_contract', function (Blueprint $table) {
            $table->integer('marketer_id')->primary();
            $table->string('name', 50);
            $table->string('national_code', 50);
            $table->string('born', 50);
            $table->string('birth_certificate', 50);
            $table->string('father_name', 50);
            $table->string('mobile', 50);
            $table->string('gov_code', 50);
            $table->string('up_gov_code', 50);
            $table->string('up_name', 50);
            $table->string('address');
            $table->string('postal_code', 50);
            $table->string('shaba', 50);
            $table->boolean('status')->default(false);
            $table->dateTime('created_at')->nullable();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketer_contract');
    }
}
