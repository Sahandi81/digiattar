<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_request', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('operator_id')->nullable()->index('operator_id');
            $table->integer('created_by')->index('created_by');
            $table->integer('category_id')->index('category_id');
            $table->text('content')->nullable();
            $table->boolean('status')->default(false);
            $table->string('message')->nullable();
            $table->dateTime('created_at')->nullable();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_request');
    }
}
