<?php

namespace Modules\MLM\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Period extends Model
{
	# New update, use `rebuild` api to change columns from deleted to deleted_at
	use SoftDeletes;
    use HasFactory;

    protected $table = 'period';

    protected $guarded = [];

    protected static function newFactory()
    {
        return \Modules\MLM\Database\factories\PeriodFactory::new();
    }
}
