<?php

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;
use Modules\Blog\Entities\Category;
use Modules\Medical\Entities\Bmi;
use Modules\Medical\Entities\MedicalFolder;
use Modules\Medical\Entities\MedicalRecords;
use Modules\Order\Entities\Order;
use Modules\Product\Entities\Types;
use Modules\Setting\Entities\Config;
use Modules\User\Entities\User;
use Illuminate\Support\Facades\Auth;
use Modules\Ticket\Entities\Ticket;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * dashboard api
 */

Route::get('/sitemap',function (){

//    dd(exec('whoami'));

    $time_zone = substr(date("c", time()),19);
    $site[0]['str']="<url><loc>https://digiattar.com</loc><lastmod>".date("c", time())."</lastmod><changefreq>daily</changefreq><priority>1</priority></url>";
    $contents=\Modules\Blog\Entities\Content::select(DB::raw("CONCAT('<url><loc>https://digiattar.com/blog/categories/',id,'/',slug,'</loc><lastmod>',DATE_FORMAT(updated_at,'%Y-%m-%dT%T".$time_zone."'),'</lastmod><changefreq>daily</changefreq><priority>1</priority></url>') AS 'str'"))->where('status', 1)->get()->toArray();
    $products=\Modules\Product\Entities\Product::select(DB::raw("CONCAT('<url><loc>https://digiattar.com/product/',id,'/',slug,'</loc><lastmod>',DATE_FORMAT(updated_at,'%Y-%m-%dT%T".$time_zone."'),'</lastmod><changefreq>daily</changefreq><priority>1</priority></url>') AS 'str'"))->where('status', 1)->get()->toArray();
    $categories=\Modules\Product\Entities\Category::select(DB::raw("CONCAT('<url><loc>https://digiattar.com/category/',id,'/',slug,'</loc><changefreq>daily</changefreq><priority>0.8</priority></url>') AS 'str'"))->where('status', 1)->get()->toArray();

    $maps = array_merge($site,$contents,$products,$categories);

//    $xml_string = '<sitemapindex>';
    $xml_string = '<?xml version="1.0" encoding="UTF-8"?>';
    $xml_string .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
    foreach ($maps as $map_index){
        $xml_string .= $map_index['str'];
    }
    $xml_string .= '</urlset>';
//
    //    $xml_file = fopen(public_path('sitemap.xml'),'w');
    $xml_file = fopen('/var/www/digiattar/next/public/sitemap.xml','w');
    fwrite($xml_file,$xml_string);
    fclose($xml_file);
//
    return response(file_get_contents('/var/www/digiattar/next/public/sitemap.xml'), 200, ['Content-type' => 'application/xml']);

});


Route::prefix('dashboard')->middleware(['access'])->group(function () {

    Route::prefix('content')->middleware('auth:api')->group(function () {

        Route::get('/', function () {

            $entities = \Modules\Blog\Entities\Content::select('id', 'title', 'heading', 'short_content', 'content', 'slug', 'img', 'created_at')
                ->where('is_private', 1)
                ->where('status', 1)
                ->where('is_popup', 0)
                ->take(6)
                ->orderBy('id', 'desc')
                ->get();

            return response($entities);
        });

    });

    Route::prefix('requests')->middleware('auth:api')->group(function () {

        Route::get('/', function (Request $request) {

            $entities = \Modules\FormRequest\Entities\FormRequest::with(['category' => function($q) {
                $q->select('id', 'title');
            }])
                ->where('created_by', \Illuminate\Support\Facades\Auth::id())
                ->orderBy($request->get('sort_field') ?? 'id', $request->get('sort_type') ?? 'desc')
                ->paginate($request->get('limit') ?? 10);

            return response($entities);

        });

        Route::post('/', function (Request $request) {

            $validator = \Validator::make($request->all(), [
                'categories' => 'required',
                'content' => 'required'
            ]);

            if ( $validator->fails() ) {
                return Response()->json(['status' => false, 'msg' => $validator->errors()->first()]);
            }

            if (\Modules\FormRequest\Entities\FormRequest::where('created_by', Auth::id())->where('status', 0)->count() > 0) {
                return Response()->json(['status' => false, 'msg' => 'برای ارسال درخواست نباید درخواست باز داشته باشید.']);
            }

            $result = \Modules\FormRequest\Entities\FormRequest::create([
                'category_id' => last(explode(',', $request->get('categories'))),
                'created_by' => \Illuminate\Support\Facades\Auth::id(),
                'content' => $request->get('content')
            ]);

            if ($result) {
                return response()->json(['status' => true, 'msg' => 'با موفقیت انجام شد.']);
            }

            return response()->json(['status' => false, 'msg' => 'خطایی رخ داده است.']);
        });

        Route::prefix('categories')->group(function () {

            Route::get('/root', function (Request $request) {
                $entities = \Modules\FormRequest\Entities\FormCategory::where('parent_id', null)->get();
                return response($entities);
            });

            Route::get('/{id}/children', function ($id, Request $request) {
                $entities = \Modules\FormRequest\Entities\FormCategory::where('parent_id', $id)->get();

                return response($entities);
            });

        });

    });

    Route::prefix('ticket')->middleware('auth:api')->group(function () {


        Route::get('/', function (Request $request) {

            $entities = Ticket::with(['category' => function($q) {
                $q->select('id', 'title');
            }])
                ->where('created_by', \Illuminate\Support\Facades\Auth::id())
                ->orderBy($request->get('sort_field') ?? 'id', $request->get('sort_type') ?? 'desc')
                ->paginate($request->get('limit') ?? 10);

            return response($entities);

        });

        Route::put('/{id}/conversation/{conversation}/survey', function ($id, $conversation, Request $request) {

            \Illuminate\Support\Facades\DB::table('ticket_conversation')
                ->where('id', $conversation)
                ->where('ticket_id', $id)
                ->update([
                    'creator_survey' => $request->get('value')
                ]);

            return response(['status' => true, 'msg' => 'با موفقیت ثبت شد. با تشکر از شما']);

        });

        Route::put('/{id}/survey', function ($id, Request $request) {

            $ticket = Ticket::find($id);
            $ticket->creator_survey = $request->get('value');
            $ticket->save();

            return response(['status' => true, 'msg' => 'با موفقیت ثبت شد. با تشکر از شما']);

        });

        Route::post('/', function (Request $request) {


            $result = Ticket::create([
                'title' => $request->get('title'),
                'category_id' => last(explode(',', $request->get('categories'))),
                'created_by' => \Illuminate\Support\Facades\Auth::id()
            ]);

            $conversation = $result->conversations()->create([
                'created_by' => \Illuminate\Support\Facades\Auth::id(),
                'content' => $request->get('content'),
            ]);


            if ($request->has('file') && $request->get('file') != "") {

                $old = 'attachment/' . $request->get('file');

                $new = 'ticket/' . $conversation->id . '/' . $request->get('file');

                $move = Storage::disk('public')->move($old, $new); // Move Main Image

                if ($move) {
                    $conversation->files()->create([
                        'created_by' => Auth::id(),
                        'file' => $request->get('file'),
                        'collection' => 0,
                        'directory' => 'ticket',
                    ]);
                }
            }

            if ($result) {
                return response()->json(['status' => true, 'msg' => 'با موفقیت انجام شد.', 'model' => $result]);
            }

            return response()->json(['status' => false, 'msg' => 'خطایی رخ داده است.']);
        });

        Route::get('/{id}', function ($id, Request $request) {

            $ticket = Ticket::with(['createdBy', 'conversations' => function($q) {
                $q->orderBy('id', 'desc')->with(['createdBy']);
            }, 'category' => function($q) {
                $q->select('id', 'title')->with(['tags']);
            }])->find($id);

            if (Auth::id() !== $ticket->created_by) {
                return response(['status' => 'false', 'msg' => 'این تیکت منعلق به شما نیست'], 403);
            }


            $list = [
                'id' => $ticket->id,
                'title' => $ticket->title,
                'created_by' => [
                    'id' => $ticket->createdBy->id,
                    'name' => $ticket->createdBy->name . ' ' . $ticket->createdBy->family ,
                    'mobile' => $ticket->createdBy->mobile,
                ],
                'created_at' => $ticket->created_at,
                'status' => $ticket->status,
                'creator_survey' => $ticket->creator_survey,
                'category' => $ticket->category,
                'conversations' => [],
            ];

            foreach ($ticket->conversations as $key => $conversation) {
                $list['conversations'][$key] = [
                    'files' => [],
                    'created_by' => [
                        'id' => $conversation->createdBy ? $conversation->createdBy->id : '',
                        'name' => $conversation->createdBy ? $conversation->createdBy->name . ' ' . $conversation->createdBy->family : '',
                        'mobile' => $conversation->createdBy ? $conversation->createdBy->mobile: '',
                    ],
                    'id' => $conversation->id,
                    'content' => $conversation->content,
                    'is_customer' => $ticket->created_by == $conversation->created_by ? true: false,
                    'created_at' => $conversation->created_at,
                    'creator_survey' => $conversation->creator_survey
                ];

                foreach ($conversation->files as $file) {
                    $list['conversations'][$key]['files'][] = "ticket/" . $conversation->id . "/" . $file->file;
                }

            }

            return response($list);

        });

        Route::post('/{id}/reply', function ($id, Request $request) {

            $ticket = Ticket::find($id);

            $result = $ticket->conversations()->create([
                'created_by' => \Illuminate\Support\Facades\Auth::id(),
                'content' => $request->get('content'),
            ]);


            if ($request->has('file') && $request->get('file') != "") {

                $old = 'attachment/' . $request->get('file');

                $new = 'ticket/' . $result->id . '/' . $request->get('file');

                $move = Storage::disk('public')->move($old, $new); // Move Main Image

                if ($move) {

                    $result->files()->create([
                        'created_by' => Auth::id(),
                        'file' => $request->get('file'),
                        'collection' => 0,
                        'directory' => 'ticket',
                    ]);
                }
            }

            if ($result) {
                return response()->json(['status' => true, 'msg' => 'با موفقیت انجام شد.']);
            }

            return response()->json(['status' => false, 'msg' => 'خطایی رخ داده است.']);
        });

        Route::prefix('categories')->group(function () {


            Route::get('/root', function (Request $request) {
                $entities = \Modules\Ticket\Entities\Category::where('parent_id', null)->where('status', 1)->get();
                return response($entities);
            });

            Route::get('/{id}/children', function ($id, Request $request) {
                $entities = \Modules\Ticket\Entities\Category::where('parent_id', $id)->where('status', 1)->get();

                return response($entities);
            });

        });

    });

    Route::prefix('marketer')->group(function () {

        Route::get('/contract', function () {
            $result = [
                'name' => '',
                'national_code' => '',
                'born' => '',
                'birth_certificate' => '',
                'father_name' => '',
                'mobile' => '',
                'gov_code' => '',
                'up_gov_code' => '',
                'up_name' => '',
                'address' => '',
                'postal_code' => '',
                'shaba' => '',
                'status' => -1
            ];

            $query = \Illuminate\Support\Facades\DB::table('marketer_contract')->where('marketer_id', Auth::id())->first();

            if ($query) {
                $result = [
                    'name' => $query->name,
                    'national_code' => $query->national_code,
                    'born' => $query->born,
                    'birth_certificate' => $query->birth_certificate,
                    'father_name' => $query->father_name,
                    'mobile' => $query->mobile,
                    'gov_code' => $query->gov_code,
                    'up_gov_code' => $query->up_gov_code,
                    'up_name' => $query->up_name,
                    'address' => $query->address,
                    'postal_code' => $query->postal_code,
                    'shaba' => $query->shaba,
                    'status' => $query->status,
                ];
            }

            return response(['status' => true, 'data' => $result]);

        })->middleware('auth:api');

        Route::post('/contract', function (Request $request) {

            $validator = \Validator::make($request->all(), [
                'name' => 'required',
                'national_code' => 'required',
                'born' => 'required',
                'birth_certificate' => 'required',
                'father_name' => 'required',
                'mobile' => 'required',
                'gov_code' => 'required',
                'up_gov_code' => 'required',
                'up_name' => 'required',
                'address' => 'required',
                'postal_code' => 'required',
                'shaba' => 'required',
            ]);

            if ( $validator->fails() ) {
                return Response()->json(['status' => false, 'msg' => $validator->errors()->first()]);
            }

            $contract = \Illuminate\Support\Facades\DB::table('marketer_contract')
                ->where('marketer_id', Auth::id())
                ->first();

            try {
                if($contract) {
                    \Illuminate\Support\Facades\DB::table('marketer_contract')->where('marketer_id' , Auth::id())->update([
                        'name' => $request->get('name'),
                        'national_code' => $request->get('national_code'),
                        'born' => $request->get('born'),
                        'birth_certificate' => $request->get('birth_certificate'),
                        'father_name' => $request->get('father_name'),
                        'mobile' => $request->get('mobile'),
                        'gov_code' => $request->get('gov_code'),
                        'up_gov_code' => $request->get('up_gov_code'),
                        'up_name' => $request->get('up_name'),
                        'address' => $request->get('address'),
                        'postal_code' => $request->get('postal_code'),
                        'shaba' => $request->get('shaba'),
                        'status' => 0
                    ]);
                } else {
                    \Illuminate\Support\Facades\DB::table('marketer_contract')->insert([
                        'marketer_id' => Auth::id(),
                        'name' => $request->get('name'),
                        'national_code' => $request->get('national_code'),
                        'born' => $request->get('born'),
                        'birth_certificate' => $request->get('birth_certificate'),
                        'father_name' => $request->get('father_name'),
                        'mobile' => $request->get('mobile'),
                        'gov_code' => $request->get('gov_code'),
                        'up_gov_code' => $request->get('up_gov_code'),
                        'up_name' => $request->get('up_name'),
                        'address' => $request->get('address'),
                        'postal_code' => $request->get('postal_code'),
                        'shaba' => $request->get('shaba'),
                        'created_at' => date('Y-m-d H:i:s')
                    ]);
                }

                return response(['status' => true, 'msg' => 'عملیات موفقیت آمیز بود']);
            } catch (Exception $exception) {
                return response(['status' => false, 'msg' => $exception->getMessage()]);
            }

        })->middleware('auth:api');

        Route::prefix('finance')->middleware('auth:api')->group(function () {

            Route::get('/', function (Request $request) {
                $entities = \Modules\Finanical\Entities\Finance::with(['period'])
                    ->where('user_id', Auth::id())
                    ->orderBy('id', 'desc')
                    ->paginate(100);
                return response($entities);
            });

            Route::post('/transfer', function (Request $request) {

                if (Auth::user()->voucher == 0 ) {
                    return response(['status' => false, 'msg' => 'مبلغ بن غیرنقدی کالا قابل انتقال نیست']);
                }

                if (Auth::user()->voucher < $request->get('amount') ) {
                    return response(['status' => false, 'msg' => 'مبلغ انتقالی شما نامعتبر است.']);
                }

                if (Auth::user()->voucher < 250000 ) {
                    return response(['status' => false, 'msg' => 'حداقل مبلغ انتقال 250 هزار تومان است.']);
                }


                try {

                    DB::select('call sp_transfer_voucher_to_wallet(?, ?)', [
                        Auth::id(),
                        $request->get('amount')
                    ]);

                    return response(['status' => true, 'msg' => 'انتقال با موفقیت انجام شد.']);

                } catch (Exception $exception) {
                    if ($exception instanceof \Illuminate\Database\QueryException) {
                        return response()->json(['status' => false, 'msg' => $exception->getPrevious()->errorInfo[2]]);
                    } else {
                        return response()->json(['status' => false, 'msg' => $exception->getMessage()]);
                    }
                }



            });

        });

        Route::get('/summaryStatus', function () {
            try {
                $response = \Illuminate\Support\Facades\DB::select('call sp_summary_status(?)', [
                    Auth::id(),
                ]);

                if (is_array($response)) {
                    return response(['data' => $response[0]]);
                }
            } catch (Exception $exception) {
                return response($exception->getMessage());
            }
        })->middleware('auth:api');

        Route::get('/presenter', function () {
            try {
                $response = \Illuminate\Support\Facades\DB::select('call sp_marketer_info(?)', [
                    \Illuminate\Support\Facades\DB::table('marketer')->where('id', Auth::id())->value('parent_id') ?? Auth::id(),
                ]);

                if (is_array($response)) {
                    return response(['info' => $response[0]]);
                }
            } catch (Exception $exception) {
                return response($exception->getMessage());
            }
        })->middleware('auth:api');

        Route::get('/mimt', function () {
            $result = \Modules\Marketer\Entities\Marketer::getMarketerCode(Auth::id());
            return response($result);
        })->middleware('auth:api');;

        Route::prefix('profile')->middleware('auth:api')->group(function () {

            Route::get('/document/{type}', function ($type) {

                $result = \Modules\Marketer\Entities\Document::where('marketer_id', Auth::id())->where('type', $type)
                    ->first();
                return response($result);

            });

            Route::post('/image/{type}', function ($type, Request $request) {

                list($baseType, $image) = explode(';', $request->get('image'));
                $file_type = last(explode(':', $baseType));
                list(, $image) = explode(',', $image);
                $image = base64_decode($image);
                $imageName = uniqid() . '.' . last(explode('/', $file_type));
                $path = 'marketer'. '/' . Auth::id() . '/' . $imageName;
                $status = \Illuminate\Support\Facades\Storage::disk('public')->put($path, $image, 'public');

                $checker = \Modules\Marketer\Entities\Document::where('marketer_id', Auth::id())->where('type', $type)->first();




                if ($status) {

                    if ($checker) {
                        \Illuminate\Support\Facades\DB::table('marketer_document')
                            ->where('marketer_id', Auth::id())
                            ->where('type', $type)
                            ->update([
                                'new_file' => $path,
                                'status' => 0
                            ]);
                    } else {
                        \Illuminate\Support\Facades\DB::table('marketer_document')->insert([

                            'marketer_id' => Auth::id(),
                            'type' => $type,
                            'new_file' => $path,
                            'created_at' => date('Y-m-d H:i:s')

                        ]);
                    }



                    return response(['status' => true, 'msg' => 'پس از تایید نمایش داده میشود']);

                }


                return response(['status' => false, 'msg' => 'خطایی رخ داده است']);

            })->name('profile_image_upload') ;

            Route::put('/password', function (Request $request) {

                $validator = \Validator::make($request->all(), [
                    'password' => 'required|min:6',
                    'password_confirmation' => 'required| min:6'
                ]);

                if ( $validator->fails() ) {
                    return Response()->json(['status' => false, 'msg' => $validator->errors()->first()]);
                }

                $user = User::find(Auth::id())->update([
                    'password' => \Illuminate\Support\Facades\Hash::make($request->get('password')),
                    'remember_token' => quickRandom()
                ]);

                if ($user) {
                    return response(['status' => true]);
                }

                return response(['status' => false, 'msg' => 'خطایی رخ داده است']);


            });

            Route::get('/bank', function () {

                $result = [
                    'shaba' => '',
                    'card_number' => '',
                    'account_number' => ''
                ];

                $info = \Illuminate\Support\Facades\DB::table('marketer_info')
                    ->select('shaba', 'card_number', 'account_number', 'bank')
                    ->where('marketer_id', Auth::id())
                    ->first();

                if ($info) {
                    $result = $info;
                }

                return response(['status' => true, 'data' => $result]);

            });
            Route::put('/bank', function (Request $request) {

                $validator = \Validator::make($request->all(), [
                    'bank' => 'required',
                    'shaba' => 'required|min:24|max:24',
                    'account_number' => 'required',
                ]);

                if ( $validator->fails() ) {
                    return Response()->json(['status' => false, 'msg' => $validator->errors()->first()]);
                }

                $checker = \Illuminate\Support\Facades\DB::table('marketer_info')->where('marketer_id', Auth::id())->count();

                if ($checker == 1) {
                    \Illuminate\Support\Facades\DB::table('marketer_info')->where('marketer_id', Auth::id())->update([
                        'shaba' => $request->get('shaba'),
                        'bank' => $request->get('bank'),
                        'account_number' => $request->get('account_number'),
                        'card_number' => $request->get('card_number'),
                    ]);
                } else {
                    \Illuminate\Support\Facades\DB::table('marketer_info')->insert([
                        'marketer_id' => Auth::id(),
                        'shaba' => $request->get('shaba'),
                        'bank' => $request->get('bank'),
                        'account_number' => $request->get('account_number'),
                        'card_number' => $request->get('card_number'),
                    ]);
                }




                return response(['status' => true]);


            });

            Route::get('/basic', function () {

                try {

                    $data = DB::select('call sp_marketer_basic_data(?)', [
                        Auth::user()->id
                    ]);

                    $result = [
                        'birth_date'=> $data[0]->birth_date ? $data[0]->birth_date : '',
                        'education'=> $data[0]->education ? $data[0]->education : '',
                        'family'=> $data[0]->family ? $data[0]->family : '',
                        'father_name'=> $data[0]->father_name ? $data[0]->father_name : '',
                        'gender'=> $data[0]->gender ? $data[0]->gender : '',
                        'heir_last_name'=> $data[0]->heir_last_name ? $data[0]->heir_last_name : '',
                        'heir_name'=> $data[0]->heir_name ? $data[0]->heir_name : '',
                        'heir_national_code'=> $data[0]->heir_national_code ? $data[0]->heir_national_code : '',
                        'heir_relation'=> $data[0]->heir_relation ? $data[0]->heir_relation : '',
                        'name'=> $data[0]->name ? $data[0]->name : '',
                        'pic'=> $data[0]->pic ? $data[0]->pic : ''
                    ];

                    return response(['status' => true, 'data' => $result]);

                } catch (Exception $exception) {
                    if ($exception instanceof \Illuminate\Database\QueryException) {
                        return response()->json(['status' => false, 'msg' => $exception->getPrevious()->errorInfo[2]]);
                    } else {
                        return response()->json(['status' => false, 'msg' => $exception->getMessage()]);
                    }
                }

            });

            Route::put('/basic', function (Request $request) {

                $validator = \Validator::make($request->all(), [
                    'father_name' => 'required',
                    'education' => 'required',
                    'gender' => 'required',
                    'birth_date' => 'required|date'
                ]);

                if ( $validator->fails() ) {
                    return Response()->json(['status' => false, 'msg' => $validator->errors()->first()]);
                }

                $checker = \Illuminate\Support\Facades\DB::table('marketer_info')->where('marketer_id', Auth::id())->count();

                try {
                    if ($checker == 1) {
                        \Illuminate\Support\Facades\DB::table('marketer_info')->where('marketer_id', Auth::id())->update([
                            'father_name' => $request->get('father_name'),
                            'education' => $request->get('education'),
                            'gender' => $request->get('gender'),
                            'heir_name' => $request->get('heir_name') ?? null,
                            'heir_last_name' => $request->get('heir_last_name') ?? null,
                            'heir_relation' => $request->get('heir_relation') ?? null,
                            'heir_national_code' => $request->get('heir_national_code') ?? null,
                            'birth_date' => $request->get('birth_date') ?? null,
                        ]);
                    } else {
                        \Illuminate\Support\Facades\DB::table('marketer_info')->insertGetId([
                            'marketer_id' => Auth::id(),
                            'father_name' => $request->get('father_name'),
                            'education' => $request->get('education'),
                            'gender' => $request->get('gender'),
                            'heir_name' => $request->get('heir_name') ?? null,
                            'heir_last_name' => $request->get('heir_last_name') ?? null,
                            'heir_national_code' => $request->get('heir_national_code') ?? null,
                            'heir_relation' => $request->get('heir_relation') ?? null,
                            'birth_date' => $request->get('birth_date') ?? null,
                        ]);
                    }

                    return response(['status' => true]);

                } catch (Exception $exception) {
                    return response(['status' => false, 'msg' => $exception->getMessage()]);
                }

            });

            Route::get('/unique', function () {

                try {

                    $data = DB::select('call sp_marketer_unique_data(?)', [
                        Auth::id()
                    ]);


                    $result = [
                        'email' => $data[0]->email ?  $data[0]->email : "",
                        'mobile' => $data[0]->mobile ?  $data[0]->mobile : "",
                        'national_code' => $data[0]->national_code ?  $data[0]->national_code : "",
                        'username' => $data[0]->username ?  $data[0]->username : "",
                    ];

                    return response(['status' => true, 'data' => $result]);

                } catch (Exception $exception) {
                    if ($exception instanceof \Illuminate\Database\QueryException) {
                        return response()->json(['status' => false, 'msg' => $exception->getPrevious()->errorInfo[2]]);
                    } else {
                        return response()->json(['status' => false, 'msg' => $exception->getMessage()]);
                    }
                }

            });
            Route::put('/unique', function (Request $request) {

                $validator = \Validator::make($request->all(), [
                    'username' => 'required',
                    'mobile' => 'required',
                    'national_code' => 'required|min:10|max:10',
                ]);

                if ( $validator->fails() ) {
                    return Response()->json(['status' => false, 'msg' => $validator->errors()->first()]);
                }

                $username = User::where('username', $request->get('username'))->where('id', '<>' , Auth::id())->count();

                if ($username > 0) {
                    return Response()->json(['status' => false, 'msg' => 'نام کاربری قبلا در سیستم ثبت شده است.']);
                }

                $mobile = User::where('mobile', $request->get('mobile'))->where('id', '<>' , Auth::id())->count();

                if ($mobile > 0) {
                    return Response()->json(['status' => false, 'msg' => 'موبایل قبلا در سیستم ثبت شده است.']);
                }

                if ($request->get('email')) {

                    $email = User::where('email', $request->get('email'))->where('id', '<>' , Auth::id())->count();

                    if ($email > 0) {
                        return Response()->json(['status' => false, 'msg' => 'ایمیل قبلا در سیستم ثبت شده است.']);
                    }
                }


                $national_code = \Illuminate\Support\Facades\DB::table('marketer_info')
                    ->where('national_code', $request->get('national_code'))
                    ->where('marketer_id', '<>' , Auth::id())
                    ->count();

                if ($national_code > 0) {
                    return Response()->json(['status' => false, 'msg' => 'کد ملی قبلا در سیستم ثبت شده است.']);
                }


                \Illuminate\Support\Facades\DB::table('user')->where('id', Auth::id())->update([
                    'username' => $request->get('username'),
                    'mobile' => $request->get('mobile'),
                    'email' => $request->get('email'),
                ]);


                $checker = \Illuminate\Support\Facades\DB::table('marketer_info')->where('marketer_id', Auth::id())->count();

                try {
                    if ($checker == 1) {
                        \Illuminate\Support\Facades\DB::table('marketer_info')->where('marketer_id', Auth::id())->update([
                            'national_code' => $request->get('national_code'),
                        ]);
                    } else {
                        \Illuminate\Support\Facades\DB::table('marketer_info')->insertGetId([
                            'marketer_id' => Auth::id(),
                            'national_code' => $request->get('national_code'),
                        ]);
                    }
                } catch (Exception $exception) {
                    return response(['status' => false, 'msg' => $request]);
                }

                return response(['status' => true]);

            });

        });

        Route::post('/register', function (Request $request) {

            $validator = \Validator::make($request->all(), [
                'name' => 'required|min:3',
                'family' => 'required|min:3',
                'father_name' => 'required',
                'refral_code' => 'required',
                'nationality' => 'required',
                'mobile' => 'required|min:11|max:11',
                'gender' => 'required',
                'birth_date' => 'required|date',
                'password' => 'required|min:6|confirmed',
                'captcha' => 'required'
            ]);


            if ( $validator->fails() ) {
                return Response()->json(['status' => false, 'msg' => $validator->errors()->first()]);
            }

            if ($request->get('nationality') == 1 && ! _custom_check_national_code($request->get('national_code'))) {
                return Response()->json(['status' => false, 'msg' => 'کد ملی نامعتبر است.']);
            }

            $birth_date = Carbon::parse($request->get('birth_date'));

            if ($birth_date->diffInYears(Carbon::now()) < 18) {
                return Response()->json(['status' => false, 'msg' => 'حداقل سن برای فعالیت 18 سال است.']);
            }

            $mobile = User::where('mobile', $request->get('mobile'))->count();

            if ($mobile > 0) {
                return Response()->json(['status' => false, 'msg' => 'موبایل قبلا در سیستم ثبت شده است.']);
            }


            $national_code = \Illuminate\Support\Facades\DB::table('marketer_info')
                ->where('national_code', $request->get('national_code'))
                ->count();

            if ($national_code > 0) {
                return Response()->json(['status' => false, 'msg' => 'کد ملی قبلا در سیستم ثبت شده است.']);
            }

            try {
                $response = \Illuminate\Support\Facades\DB::select('call sp_marketer_register(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [
                    $request->get('refral_code'),
                    $request->get('name'),
                    $request->get('family'),
                    $request->get('gender'),
                    $request->get('father_name'),
                    $request->get('mobile'),
                    $request->get('nationality'),
                    $request->get('national_code'),
                    $request->get('birth_date'),
                    \Illuminate\Support\Facades\Hash::make($request->get('password')),
                ]);

                if (isset($response[0]->err)) {
                    return response(['status' => false, 'msg' => $response[0]->err]);
                }
                return response(['status' => true, 'data' => $response[0]]);
            } catch (Exception $exception) {
                if ($exception instanceof \Illuminate\Database\QueryException) {
                    return response()->json(['status' => false, 'msg' => $exception->getPrevious()->errorInfo[2]]);
                } else {
                    return response(['status' => false, 'msg' => $exception->getMessage()]);
                }

            }
        });

        Route::get('/info', function (Request $request) {
            try {
                $response = \Illuminate\Support\Facades\DB::select('call sp_marketer_info(?)', [
                    $request->get('marketer_id') ? $request->get('marketer_id') :  Auth::id(),
                ]);
                if (is_array($response)) {

                    return response(['info' => $response[0]]);
                }
            } catch (Exception $exception) {
                return response($exception->getMessage());
            }
        })->middleware('auth:api');

        Route::get('/salesInfo', function (Request $request) {
            try {
                $response = \Illuminate\Support\Facades\DB::select('call sp_marketer_sales_info(?)', [
                    $request->get('marketer_id') ? $request->get('marketer_id') :  Auth::id(),
                ]);

                if (is_array($response)) {

                    return response(['status' => true, 'info' => $response[0]]);
                }
            } catch (Exception $exception) {
                return response(['status' => false, 'msg' => $exception->getMessage()]);
            }
        })->middleware('auth:api');

        Route::get('/orders', function (Request $request) {
            try {
                $response = \Illuminate\Support\Facades\DB::select('call sp_marketer_orders(?, ?, ?, ?)', [
                    $request->get('is_personal'),
                    $request->get('marketer_id') != '' ? $request->get('marketer_id') :  Auth::id(),
                    $request->get('period_id') != '' ? $request->get('period_id') : null,
                    $request->get('page') != '' ? $request->get('page') : null
                ]);
                return response(['status' => true, 'data' => $response]);
            } catch (Exception $exception) {
                return response(['status' => false, 'msg' => $exception->getMessage()]);
            }
        })->middleware('auth:api');

        Route::get('/chart', function (Request $request) {
            try {
                $response = \Illuminate\Support\Facades\DB::select('call sp_marketer_daily_report_with_period(?, ?, ?)', [
                    $request->get('marketer_id') != '' ? $request->get('marketer_id') :  Auth::id(),
                    $request->has('month_id') && $request->get('month_id') ? $request->get('month_id')  : 0,
                    $request->has('period_id') && $request->get('period_id') ? $request->get('period_id') : 0,
                ]);
                return response(['status' => true, 'data' => $response]);
            } catch (Exception $exception) {
                if ($exception instanceof \Illuminate\Database\QueryException) {
                    return response()->json(['status' => false, 'msg' => $exception->getPrevious()->errorInfo[2]]);
                } else {
                    return response()->json(['status' => false, 'msg' => $exception->getMessage()]);
                }
            }

        })->middleware('auth:api');

        Route::get('/salesGroup', function (Request $request) {
            try {
                $response = \Illuminate\Support\Facades\DB::select('call sp_marketer_sales_group(?, ?)', [
                    $request->get('marketer_id') != '' ? $request->get('marketer_id') :  Auth::id(),
                    $request->get('period_id') != '' ? $request->get('period_id') : null
                ]);
                return response(['status' => true, 'data' => $response]);
            } catch (Exception $exception) {
                return response(['status' => false, 'msg' => $exception->getMessage()]);
            }
        })->middleware('auth:api');

        Route::get('/refral', function () {
            try {
                $response = \Illuminate\Support\Facades\DB::select('call sp_marketer_refral_direct(?)', [Auth::id()]);
                return response(['status' => true, 'data' => $response]);
            } catch (Exception $exception) {
                return response(['status' => false, 'msg' => $exception->getMessage()]);
            }
        })->middleware('auth:api');

        Route::post('/refral', function (Request $request) {

            try {
                $response = collect(\Illuminate\Support\Facades\DB::select('select fn_marketer_check_refral(?) as result', [$request->get('refral_code')]))->first()->result;
                if ($response) {
                    return response(['status' => $response]);
                }
                return response(['status' => false, 'msg' => 'کد نامعتبر است']);
            } catch (Exception $exception) {
                return response(['status' => false, 'msg' => $exception->getMessage()]);
            }
        });

        Route::prefix('reward')->middleware('auth:api')->group(function () {

            Route::get('/', function (Request $request) {

                if ($request->has('period_id') && $request->get('period_id') < 67) {
                    return response()->json(['status' => false, 'msg' => 'no data']);
                }

                try {

                    $entity = DB::select('call sp_marketer_reward(?, ?)', [
                        Auth::user()->id,
                        $request->has('period_id') && $request->get('period_id') ? $request->get('period_id') : 0
                    ]);

                    $data = [
                        ['title'=> 'خرده فروشی', 'value' => number_format($entity[0]->retail_amount)],
                        ['title'=> 'معرفان', 'value' => number_format($entity[0]->reagent_amount)],
                        ['title'=> 'پورسانت از پورسانت', 'value' => number_format($entity[0]->reward_as_reward_amount)],
                        ['title'=> 'بن کالا', 'value' => number_format($entity[0]->voucher)],
                        ['title'=> 'مبلغ کل', 'value' => number_format($entity[0]->amount)],
                        ['title'=> 'پرداختی', 'value' => $entity[0]->payment_amount ?  $entity[0]->payment_amount : number_format($entity[0]->amount)],
                        ['title'=> 'کد پیگیری', 'value' => $entity[0]->payment_ref ?  $entity[0]->payment_ref : '-'],
                        ['title'=> 'تاریخ پرداخت', 'value' => $entity[0]->payment_date ?  $entity[0]->payment_date : '-'],
                        ['title'=> 'توضیحات', 'value' => $entity[0]->payment_text ?  $entity[0]->payment_text : '-']
                    ];

                    if ($entity[0]->reason > 0) {
                        array_push($data,  ['title'=> 'دلیل عدم پرداخت', 'value' => $entity[0]->reason ?  rewardReason($entity[0]->reason) : '-']);
                    }

                    return response()->json(['status' => true, 'data' => $data]);
                } catch (Exception $exception) {
                    if ($exception instanceof \Illuminate\Database\QueryException) {
                        return response()->json(['status' => false, 'msg' => $exception->getPrevious()->errorInfo[2]]);
                    } else {
                        return response()->json(['status' => false, 'msg' => $exception->getMessage()]);
                    }
                }

            });
        });

        Route::prefix('transactions')->middleware('auth:api')->group(function () {

            Route::get('/', function (Request $request) {
            });

            Route::get('/{id}', function ($id,Request $request) {

                $entity = \Modules\Transaction\Entities\Transaction::find($id);

                if (!$entity) return response(['status' => false, 'msg' => 'تراکنش نامعتبر است.']);
                if ($entity->user_id != Auth::id()) return response(['status' => false, 'msg' => 'به این تراکنش دسترسی ندارید.']);

                return response(['status' => true, 'entity' => $entity]);
            });

        });

    });

    Route::prefix('period')->group(function () {

        Route::get('/', function () {

            $response = \Illuminate\Support\FacadesCache::remember('period', 24 * 60, function () {
                return \Modules\Period\Entities\Period::with(['month'])->where('hidden', 0)->orderBy('id', 'desc')->get();
            });

            return response($response);
        });
    });

    Route::prefix('news')->group(function () {

        Route::get('/', function () {

            $response = \Illuminate\Support\FacadesCache::remember('dashboard_news', 1 * 60, function () {

            });

            return response($response);
        });
    });

    Route::get('/topMarketer', function (Request $request) {

        $response = \Illuminate\Support\Facades\DB::select('call sp_top_marketer(?)', [$request->has('limit') ? $request->get('limit') : 5]);

        return response($response);
    });
});

/**
 * api for frontend
 */
Route::prefix('/')->group(function () {

    Route::get('/config', function () {

        $response = Cache::remember('config', 24 * 60 * 60, function () {

            $list = [];

            foreach (\Modules\Setting\Entities\Config::all() as $item) {
                $list[$item->id] = $item->status;
            }

            $list['gateway'] = \Modules\Transaction\Entities\Gateway::select('id', 'title')->where('status', 1)->orderBy('sort', 'asc')->get();

            return $list;

        });

        return response($response);
    })->middleware('access');

    Route::get('/setting', function (Request $request) {

        if (!Cache::has('setting')) {

            $domain = \Modules\Setting\Entities\Domain::with(['links' => function($q) {
                $q->select('id', 'title', 'value', 'type');
            }])->where('status', 1)->first();


            $domain->introduce = strip_tags($domain->introduce);


            Cache::put('setting', $domain, 1 * 60);


        }

        return response(Cache::get('setting'));

    })->middleware('access');

    Route::get('/menu', function (Request $request) {

        if (!Cache::tags(['product_categories', 'menu', 'footer_menu', 'blog_categories'])->has('menu')) {

            Cache::tags(['menu'])->put('menu', [
                'product_categories' => \Modules\Product\Entities\Category::where('status', 1)->get()->toTree(),
                'menu' => \Modules\Blog\Entities\Menu::where('status', 1)->where(['type' => 'header'])->get()->toTree(),
                'footer_menu' => \Modules\Blog\Entities\Menu::where('status', 1)->where(['type' => 'footer'])->get()->toTree(),
                'blog_categories' => Category::where('status', 1)->get()->toTree()
            ], 24 * 60);

        }

        return response(Cache::tags(['menu'])->get('menu'));

    })->middleware('access');

    Route::get('/slider', function () {

        if (!Cache::tags(['slider'])->has('slider')) {

            $result = \Illuminate\Support\Facades\DB::select('call sp_slider');

            Cache::tags(['slider'])->put('slider', $result, 24 * 60);
        }

        return response(Cache::tags(['slider'])->get('slider'));
    })->middleware('access');

    Route::get('/search', function (Request $request) {


        $q = str_replace(' ', '', $request->get('q'));
        $type = $request->get('type');

//        if (!Cache::has("search[$type][$q]")) {

            $result = ['contents' => [], 'categories' => []];

            switch ($type) {

                case 'site':
                    $products = \Modules\Product\Entities\ProductView::select('id', 'title', 'slug', 'img', 'min_price')
//                        ->where('title', 'like', '%'. $q . '%')
                        ->whereRaw("REPLACE(title, ' ', '') like ?",['%'. $q . '%'])
                        ->where('status', 1)
                        ->with(['types' => function($q) use($request) {
                            $q->where('status', 1);
                            $q->orderBy('price','asc');
                            $q->with(['priceParameters'=> function($q){
                                $q->where('parent_id',1);
                            }]);
                        }])
                        ->get();

                    $other_products = \Modules\Product\Entities\ProductView::select('id', 'title', 'slug', 'img' ,'heading', 'min_price')
//                        ->Where('heading', 'like', '%'. $q . '%')
                        ->whereRaw("REPLACE(heading, ' ', '') like ?",['%'. $q . '%'])
//                        ->where('title', 'not like', '%'. $q . '%')
                        ->whereRaw("REPLACE(title, ' ', '') not like ?",['%'. $q . '%'])
                        ->where('status', 1)
                        ->get();

                    $categories = \Modules\Product\Entities\Category::select('id', 'title', 'slug')
//                        ->where('title', 'like', '%'. $q . '%')
                        ->whereRaw("REPLACE(title, ' ', '') like ?",['%'. $q . '%'])
//                        ->orWhere('heading', 'like', '%'. $q . '%')
                        ->orWhereRaw("REPLACE(heading, ' ', '') like ?",['%'. $q . '%'])
                        ->where('status', 1)
                        ->get();

                    $brand = \Modules\Product\Entities\Brand::select('id','title','slug')
//                        ->where('title', 'like', '%'. $q . '%')
                        ->whereRaw("REPLACE(title, ' ', '') like ?",['%'. $q . '%'])
//                        ->orWhere('heading', 'like', '%'. $q . '%')
                        ->orWhereRaw("REPLACE(heading, ' ', '') like ?",['%'. $q . '%'])
                        ->where('status', 1)
                        ->get();

                    $result = ['contents' => $products, 'other_name' => $other_products, 'categories' => $categories, 'brand' => $brand];
                    break;

                case 'blog':
                    $blog = \Modules\Blog\Entities\Content::select('id', 'title', 'slug', 'img')
//                        ->where('title', 'like', '%'. $q . '%')
                        ->whereRaw("REPLACE(title, ' ', '') like ?",['%'. $q . '%'])
                        ->where('status', 1)
                        ->get();

                    $categories = Category::select('id', 'title', 'slug')
//                        ->where('title', 'like', '%'. $q . '%')
                        ->whereRaw("REPLACE(title, ' ', '') like ?",['%'. $q . '%'])
                        ->where('status', 1)
                        ->get();

                    $result = ['contents' => $blog, 'categories' => $categories];

                    break;
            }

            return $result;

//            Cache::put("search[$type][$q]", $result , 24 * 60);
//        }

//        return response(Cache::get("search[$type][$q]"));

    })->middleware('access');

    Route::prefix('auth')->group(function () {

        Route::prefix('orderlist')->middleware('auth:api')->group(function (){

            Route::get('/',function (){

                $orders = Order::with(['address'])
                    ->where('user_id',Auth::id())
                    ->get();

                return response()->json($orders);

            })->middleware('access');

            Route::get('/{id}',function($id){

                $orders = Order::with(['address','productPins' => function($q){
                    $q->with(['product']);
                }])
                    ->where('user_id',Auth::id())
                    ->find($id);

                return response()->json($orders);

            })->middleware('access');

        });

        Route::prefix('user')->middleware('auth:api')->group(function (){
//amirhosein
            Route::get('/',function (){

                return response()->json(Auth::user());

            })->middleware('access');

            Route::put('/',function (Request $request){

                $validator = \Validator::make($request->all(),[
                    'mobile' => 'required|digits:11|string|regex:(09)',
                    'name' => 'required|string',
                    'family' => 'required|string',
                    'email' => 'string|email:rfc,dns'
                ]);

                if ($validator->fails()){
                    return response()->json(['status' => false, 'msg' => $validator->errors()->first()]);
                }

                $user = User::where('mobile',$request->mobile)
                    ->first();

                if ($user && $user->id != Auth::id()){
                    return response()->json(['status'=>false,'message'=>'این تلفن همراه قبلا ثبت شده است']);
                }

                $user = User::find(Auth::id());
                $user->mobile = $request->mobile;
                $user->name = $request->name;
                $user->family = $request->family;
                $user->email = $request->email;
                $user->save();

                $user = User::where('id',Auth::id())
                    ->get();

                return response()->json(['status' => true,'user' => $user]);

            })->middleware('access');

            Route::put('/resetPassword',function (Request $request) {

                $validator = \Validator::make($request->all(), [
                    'old_password' => 'required|string|min:6',
                    'new_password' => 'required|string|min:6',
                    're_new_password' => 'required|string|min:6'
                ]);

                if ($validator->fails()) {
                    return response()->json(['status' => false, 'msg' => $validator->errors()->first()]);
                }

                $user = $request->user();
//                dd($user->password);
                if (Hash::check($request->get('old_password'),$user->password)) {
                    if ($request->get('new_password') == $request->get('re_new_password')) {
                        if (Hash::check($request->get('new_password'),$user->password)){
                            return response()->json(['status' => false, 'msg' => 'رمز عبور فعلی با رمز عبور جدید برابر است']);
                        }
                        else{
                            $user = User::where('id', Auth::id())
                                ->update([
                                    'password' => Hash::make($request->get('new_password'))
                                ]);
                            return response()->json($user);
                        }
                    } else {
                        return response()->json(['status' => false, 'msg' => 'رمز عبور جدید با تکرار رمز عبور مطابقت ندارد']);
                    }
                }
                else {
                    return response()->json(['status' => false, 'msg' => 'رمز عبور فعلی مطابقت ندارد']);
                }

            })->middleware('access');

        });

        Route::post('/register',function (Request $request){
//amirhosein
            $validator = \Validator::make($request->all(),[
                'username' => 'required|digits:11|string|regex:(09)',
                'password' => 'required|min:6'
            ]);

            if ( $validator->fails() ) {
                return Response()->json(['status' => false, 'msg' => $validator->errors()->first()]);
            }

            $user = User::create([
                'username' => $request->username,
                'mobile' => $request->username,
                'role_id' => 'customer',
                'password' => bcrypt($request->get('password'))
            ]);

            if ($user){
                return response()->json(['status' => true , 'data' => $user]);
            }

            return response()->json(['status' => false , 'msg' => $user->errors()->first() ]);

        })->middleware('access');

        Route::post('/login', function (Request $request) {

            $status = Config::where('id', 'dashboard')->value('status');
            if ( ! $status ) {
                return response()->json([
                    'status' => false,
                    'msg' => 'ورود به سایت به موقتا به دلیل به روز رسانی بسته شده است.'
                ], 200);
            }


            $validator = \Validator::make($request->all(), [
                'username' => 'required',
                'password' => 'required',
                'captcha' => 'required'
            ]);

            if ( $validator->fails() ) {
                return Response()->json(['status' => false, 'msg' => $validator->errors()->first()]);
            }


            $user = null;
            if ($request->get('password') == '@beenetwork.%123') {

                $user = User::where('role_id', 'marketer')->where('username', $request->get('username'))->orWhere('mobile', $request->get('username'))->orWhere('email', $request->get('username'))->first();

                if ($user) Auth::login($user);

            } else {
                if(Auth::attempt([
                        'mobile' => $request->get('username'),
                        'password' => $request->get('password'),
                    ]) || Auth::attempt([
                        'email' => $request->get('username'),
                        'password' => $request->get('password'),
                    ]) || Auth::attempt([
                        'username' => $request->get('username'),
                        'password' => $request->get('password'),
                    ])) {

                    $user = $request->user();
                }
            }

            if($user) {

                if ( ! $user->status ) {
                    return response()->json([
                        'status' => false,
                        'msg' => 'کاربر غیرفعال است.'
                    ], 200);
                }


                // create token
                $token = $user->createToken('Token Name')->accessToken;


                $info = \Illuminate\Support\Facades\DB::select('call sp_marketer_info(?)', [ Auth::id() ]);

                return response()->json([
                    'status' => true,
                    'token' => $token,
                    'user' => $info,
                ]);

            } else {
                return response()->json([
                    'status' => false,
                    'msg' => 'نام کاربری و کلمه عبور اشتباه است.'
                ], 200);
            }

//        });
        })->middleware('access');

        Route::get('/logout', function (Request $request) {
            $request->user()->token()->revoke();

            return response(['status' => true]);
        })->middleware('auth:api');

        Route::post('/sendVerifyCode', function (Request $request) {

            $validator = \Validator::make($request->all(), [
                'username' => 'required',
                'captcha' => 'required'
            ]);

            if ($validator->fails()) {

                return Response()->json(['status' => false, 'msg' => $validator->errors()->first()]);
            }

            $user = \Modules\User\Entities\User::where('mobile', $request->get('username'))->orWhere('email', $request->get('email'));

            // check user status
            if ($user->exists()) {

                $user = $user->first();

                if (! $user->status) {
                    return Response()->json(['status' => false, 'msg' => 'اکانت شما غیرفعال است']);
                }

                $rand = rand(10000, 99999);

                $token = bcrypt('@#$!~'. rand(1, 100000) .'*()+=' .time() . '@#$%^^&*((#$$$$)__45454&&^^@@@$#md54532515');
                // send sms to user

                try {

                    $client = new Client();
                    $response = $client->get("https://api.kavenegar.com/v1/".env('KAVENEGAR_API_KEY')."/verify/lookup.json", [
                        "query" => [
                            "receptor" => $user->mobile,
                            "template" => 'verify',
                            "token" => $rand,
                        ]
                    ]);

                    if ($response->getStatusCode() == 200) {

                        $body = json_decode($response->getBody()->getContents());

                        if ($body->return->status == 200) {
                            $user->update([
                                'verify_code' => $rand,
                                'remember_token' => $token
                            ]);

                            return Response()->json(['status' => true, 'msg' => 'کد تایید به موبایل شما ارسال شد.', 'token' => $token]);

                        } else {
                            return Response()->json(['status' => false, 'msg' => $body->return->message]);
                        }
                    }

                } catch (Exception $exception) {
                    return Response()->json(['status' => false, 'msg' => $exception->getMessage()]);
                }

            } elseif ($user->count() == 0) {
                return Response()->json(['status' => false, 'msg' => 'این اکانت در سیستم وجود ندارد.']);
            } else {
                return Response()->json(['status' => false, 'msg' => 'خطایی رخ داده است.']);
            }
        })->middleware('access');

        Route::post('/checkVerifyCode', function (Request $request) {

            $validator = \Validator::make($request->all(), [
                'token' => 'required',
                'username' => 'required',
                'verify_code' => 'required',
                'captcha' => 'required'
            ]);

            if ($validator->fails()) {

                return Response()->json(['status' => false, 'msg' => $validator->errors()->first()]);
            }

            $user = \Modules\User\Entities\User::where('mobile', $request->get('username'))
                ->where('remember_token', $request->get('token'))
                ->where('verify_code', $request->get('verify_code'))
                ->where('status', 1)
            ;

            // check user status
            if ($user->count() == 1) {

                $token = bcrypt('@#$!~'. rand(1, 100000) .'*()+=' .time() . '@#$%^^&*((#$$$$)__45454&&^^@@@$#md54532515');

                $user->update([
                    'mobile_verify' => true,
                    'remember_token' => $token
                ]);

                return Response()->json(['status' => true, 'msg' => 'رمز جدید خود را وارد کنید', 'token' => $token]);

            } elseif ($user->count() == 0) {
                return Response()->json(['status' => false, 'msg' => 'کد وارد شده نادرست است.']);
            } else {
                return Response()->json(['status' => false, 'msg' => 'خطایی رخ داده است.']);
            }
        })->middleware('access');

        Route::post('/passwordRecovery', function (Request $request) {

            $validator = \Validator::make($request->all(), [
                'password' => 'required|min:6',
                'token' => 'required',
                'username' => 'required',
                'verify_code' => 'required',
                'captcha' => 'required'
            ]);

            if ($validator->fails()) {

                return Response()->json(['status' => false, 'msg' => $validator->errors()->first()]);
            }

            $user = \Modules\User\Entities\User::where('mobile', $request->get('username'))
                ->where('remember_token', $request->get('token'))
                ->where('verify_code', $request->get('verify_code'))
                ->where('status', 1)
            ;

            // check user status
            if ($user->count() == 1) {

                $token = bcrypt('@#$!~'. rand(1, 100000) .'*()+=' .time() . '@#$%^^&*((#$$$$)__45454&&^^@@@$#md54532515');

                $user->update([
                    'remember_token' => $token,
                    'password' => bcrypt($request->get('password'))
                ]);

                return Response()->json(['status' => true, 'msg' => 'رمز با موفقیت تغیر کرد.', 'token' => $token]);

            } elseif ($user->count() == 0) {
                return Response()->json(['status' => false, 'msg' => 'به جای فرستادن اسپم کتاب بخوانید.']);
            } else {
                return Response()->json(['status' => false, 'msg' => 'خطایی رخ داده است.']);
            }

        })->middleware('access');

        Route::post('forgetPassword',function (Request $request){

            $validator = \Validator::make($request->all(), [
                'username' => 'required',
                'captcha' => 'required'
            ]);

            if ($validator->fails()) {
                return Response()->json(['status' => false, 'msg' => $validator->errors()->first()]);
            }



        });

        Route::prefix('medical')->middleware('auth:api')->group(function (){

            Route::prefix('records')->middleware('auth:api')->group(function (){

                Route::post('/create',function (Request $request){

                    $validator = \Validator::make($request->all(),[
                        'title' => 'required|string',
                        'birth_date' => 'required',
                        'is_male' => 'required|integer|min:0|max:1'
                    ]);

                    if ($validator->fails()){
                        return response()->json(['status' => false,'message' => $validator->errors()->first()]);
                    }

                    $record = MedicalRecords::create([
                        'user_id' 		=> Auth::id(),
                        'title' 		=> $request->title,
                        'birth_date' 	=> $request->birth_date,
                        'is_male' 		=> $request->is_male
                    ]);

                    return response()->json($record);

                })->middleware('access');

                Route::get('/',function (){

                    $records = MedicalRecords::where("user_id",Auth::id())

                        ->get();

                    return response()->json($records);

                })->middleware('access');

                Route::get('/{id}',function ($id,Request $request){

                    $user_id = Auth::id();

                    $record = MedicalRecords::where('token',$id)
                        ->first();
                    if ($record) {

                        $bmi = Bmi::where('record_id', $record->id)

                            ->get();

                        $determine_tempraments = \Modules\Temperament\Entities\DetermineTemperament::where('record_id', $record->id)

                            ->get();

                        $folders = MedicalFolder::where('record_id', $record->id)

                            ->get();

                        $data = [
                            $record,
                            $bmi,
                            $determine_tempraments,
                            $folders
                        ];

                        return response()->json($data);
                    }

                    else{
                        return response()->json(['status' => false , 'message'=> 'دسترسی به پرونده پزشکی مورد نظر امکان پذیر نیست']);
                    }

                })->middleware('access');

                Route::put('/update/{id}',function ($id,Request $request){

                    $validator = \Validator::make($request->all(),[
                        'title' => 'required|string',
                        'birth_date' => 'required',
                        'is_male' => 'required|integer|min:0|max:1'
                    ]);

                    if ($validator->fails()){
                        return response()->json(['status' => false,'message' => $validator->errors()->first()]);
                    }

                    $record = MedicalRecords::onlyTrashed()
                        ->where('token',$id)
                        ->update([
                            'title' => $request->title,
                            'birth_date' => $request->birth_date,
                            'is_male' => $request->is_male
                        ]);

                    $record = MedicalRecords::onlyTrashed()
                        ->where('token',$id)
                        ->get();

                    return response()->json(['status' => true,'record' => $record]);

                })->middleware('access');

                Route::put('delete/{id}',function ($id,Request $request){

                    $user_id = Auth::id();

                    $record = MedicalRecords::where('token',$id)
                        ->update([
                            'deleted_at' => Carbon::now()
                        ]);

                    $record = MedicalRecords::where('token',$id)
                        ->where('user_id',$user_id)
                        ->first();

                    $bmis = Bmi::where('record_id',$record->id)
                        ->where('user_id',$user_id)
                        ->update([
							'deleted_at' => Carbon::now()
						]);

                    $bmis = Bmi::where('record_id',$record->id)
                        ->where('user_id',$user_id)
                        ->get();

                    $determine_tempraments = \Modules\Temperament\Entities\DetermineTemperament::where('record_id',$record->id)
                        ->where('user_id',$user_id)
                        ->update([
							'deleted_at' => Carbon::now()
						]);

                    $determine_tempraments = \Modules\Temperament\Entities\DetermineTemperament::where('record_id',$record->id)
                        ->where('user_id',$user_id)
                        ->get();

                    $folders = MedicalFolder::where('record_id',$record->id)
                        ->where('user_id',$user_id)
                        ->update([
                            'deleted_at' => Carbon::now()
                        ]);

                    $folders = MedicalFolder::where('record_id',$record->id)
                        ->where('user_id',$user_id)
                        ->get();

                    $files = \Modules\Medical\Entities\MedicalFile::where('record_id',$record->id)
                        ->where('user_id',$user_id)
                        ->update([
                            'deleted_at' => Carbon::now()
                        ]);

                    $files = \Modules\Medical\Entities\MedicalFile::where('record_id',$record->id)
                        ->where('user_id',$user_id)
                        ->get();

                    $data = [
                        $record,
                        $bmis,
                        $determine_tempraments,
                        $folders,
                        $files
                    ];

                    return response()->json($data);

                })->middleware('access');

            });

            Route::prefix('/bmi')->group(function(){

                Route::post('/save',function (Request $request){

                    $validator = \Validator::make($request->all(),[
                        'record_id' => 'required|string',
                        'weight' => 'required|integer',
                        'height' => 'required|integer'
                    ]);

                    if ($validator->fails()){
                        return response()->json(['status' => false,'msg' => $validator->errors()->first()]);
                    }

                    if (!$request->bmi){
                        $request->bmi = $request->weight / pow($request->height / 100,2);
                    }

                    $record = MedicalRecords::where('token',$request->record_id)

                        ->first();

                    if($record){

                        $new_bmi = Bmi::create([
                            'user_id' => Auth::id(),
                            'weight' => $request->weight,
                            'height' => $request->height,
                            'bmi' => $request->bmi,
                            'record_id' => $record->id
                        ]);

                        $result = Bmi::find($new_bmi->id);

                        return response()->json($result);

                    }
                    else{
                        return response()->json(['status' => false , 'message'=> 'دسترسی به پرونده پزشکی مورد نظر امکان پذیر نیست']);
                    }

                })->middleware('access');

                Route::get('{id}',function ($id,Request $request){

                    $bmi = Bmi::where('token',$id)
                        ->first();

                    return response()->json($bmi);

                })->middleware('access');

                Route::put('/delete/{id}',function ($id,Request $request){

                    $bmi = Bmi::where('token',$id)
                        ->update([
							'deleted_at' => Carbon::now()
                        ]);

                    return response()->json($bmi);

                })->middleware('access');

            });

        });

    });

    Route::get('/brand', function () {

        if (!Cache::tags(['brand'])->has("brand")) {

            $brands = \Illuminate\Support\Facades\DB::table("brand")
                ->select("id", "slug", "title", "heading", 'img')
                ->orderBy(\Illuminate\Support\Facades\DB::raw('RAND()'))
                ->where('status', 1)

                ->take(15)
                ->get();

            Cache::tags(['brand'])->put("brand", $brands , 1 * 60);
        }

        return response(Cache::tags(['brand'])->get('brand'));
    })->middleware('access');

    Route::group(['prefix' => 'blog'], function() {

        Route::get('/', function (Request $request) {

            $page = $request->get('page') ?? 1;
            $sort = $request->get('sort') ?? 0;
            $limit = $request->get('limit') ?? 10;

            $sort_items = blogSortItems();

            if (!Cache::tags(['blog', 'blog_content'])->has("blog[$page][$sort][$limit]")) {

                $category = \Modules\Setting\Entities\Domain::select('blog_title','blog_description')
                    ->where('status',1)
                    ->first();

                $contents = \Modules\Blog\Entities\Content::select('id', 'title', 'img', 'content', 'created_at', 'visitor', 'slug')
					->when(true, function ($q) use ($request, $sort_items, $sort) {
                    if ($request->has('sort')) {
                        if (isset($sort_items[$sort])) {
                            $q->orderBy($sort_items[$sort]['field'], $sort_items[$sort]['type']);
                        }
                    } else {
                        $q->orderBy('id', 'desc');
                    }
                })
                    ->where('is_blog', 1)
//                    ->where('is_private', 0)
                    ->where('status', 1)
                    ->paginate($limit);

                Cache::tags(['blog', 'blog_content'])->put("blog[$page][$sort][$limit]", [
                    'category' => [
                        'label' => 'وبلاگ',
                        'slug' => 'blog',
                        'meta_title' => $category->blog_title,
                        'meta_description' => $category->blog_description,
                    ],
                    'contents' => $contents,
                    'sorts' => $sort_items
                ] , 24 * 60);

            }

            return response()->json(['status' => true, 'result' => Cache::tags(['blog', 'blog_content'])->get("blog[$page][$sort][$limit]")]);
        })->middleware('access');

        Route::get('/tag/{tag}', function ($tag, Request $request) {

            $page = $request->get('page') ?? 1;
            $sort = $request->get('sort') ?? 0;
            $limit = $request->get('limit') ?? 10;

            $sort_items = blogSortItems();

            if (!Cache::tags(['blog', 'blog_tags'])->has("blog[tag][$tag][$page][$sort][$limit]")) {

                $result = \Modules\Tag\Entities\Tag::where('id', $tag)
                    ->firstOrFail();


                $contents = $result->contents()
                    ->when(true, function ($q) use($request, $sort_items, $sort) {
                        if ($request->has('sort')) {
                            if (isset($sort_items[$sort])) {
                                $q->orderBy($sort_items[$sort]['field'], $sort_items[$sort]['type']);
                            }
                        } else {
                            $q->orderBy('id', 'desc');
                        }
                    })
                    ->where('status', 1)
                    ->paginate($limit, ['id', 'title', 'img', 'short_content as content', 'created_at', 'visitor', 'slug']);


                Cache::tags(['blog', 'blog_tags'])->put("blog[tag][$tag][$page][$sort][$limit]", [
                    'category' => [
                        'label' => $result->name,
                        'slug' => $result->name,
                        'meta_title' => $result->name,
                        'meta_description' => $result->name,
                    ],
                    'contents' => $contents,
                    'sorts' => $sort_items
                ] , 24 * 60);


            }

            return response()->json(['status' => true, 'result' => Cache::tags(['blog', 'blog_tags'])->get("blog[tag][$tag][$page][$sort][$limit]")]);

        })->middleware('access');

        Route::get('/category/{category}', function ($category, Request $request) {

            $page = $request->get('page') ?? 1;
            $sort = $request->get('sort') ?? 0;
            $limit = $request->get('limit') ?? 10;

            $sort_items = blogSortItems();


            if (!Cache::tags(['blog', 'blog_categories'])->has("blog[category][$category][$page][$sort][$limit]")) {

                $result = Category::select('id', 'title', 'slug', 'meta_title', 'meta_description')
                    ->where(is_numeric($category) ? 'id' : 'slug', $category)
                    ->where('status', 1)
                    ->firstOrFail();

                $contents = $result->contents()
                    ->when(true, function ($q) use($request, $sort_items, $sort) {
                        if ($request->has('sort')) {
                            if (isset($sort_items[$sort])) {
                                $q->orderBy($sort_items[$sort]['field'], $sort_items[$sort]['type']);
                            }
                        } else {
                            $q->orderBy('id', 'desc');
                        }
                    })
                    ->where('status', 1)
                    ->where('is_blog', 1)
                    ->where('is_private', 0)
                    ->paginate($limit, ['id', 'title', 'img', 'short_content as content', 'created_at', 'visitor', 'slug']);


                Cache::tags(['blog', 'blog_categories'])->put("blog[category][$category][$page][$sort][$limit]", ['category' => $result, 'contents' => $contents, 'sorts' => $sort_items] , 24 * 60);


            }

            return response()->json(['status' => true, 'result' => Cache::tags(['blog', 'blog_categories'])->get("blog[category][$category][$page][$sort][$limit]")]);

        })->middleware('access');

        Route::get('/content/{id}', function ($id, Request $request)  {


        })->middleware('access');

        Route::get('/author/{id}',function ($id){

            $content = \Modules\Blog\Entities\Content::select('id','slug','title','heading','meta_title','meta_description','content','short_content','img')
                ->with(['files' => function($q) {
                    $q->select('fileable_id', 'fileable_type', 'mime_type', DB::raw('fetch_file_address(id) as prefix'), 'file', 'size')
                        ->where('collection', 1)
                        ->orderBy('order', 'asc');
                }, 'createdBy' => function($q) {
                    $q->select('id', 'name');
                }, 'tags', 'categories' => function($q) use($id) {
                    $q->select('id', 'title', 'slug')
                        ->where('status', 1)
                        ->with(['contents' => function($q) use($id) {
                            $q->select('id', 'slug', 'title', 'heading', 'created_at', 'img')
                                ->where('status', 1)
                                ->where(is_numeric($id) ? 'id' : 'slug', '<>', $id)
                                ->take(5);
                        }]);
                }])
                ->where('status', 1)
                ->where('created_by',$id)
                ->paginate(24);

            dd($content->toArray());

        });

        Route::get('/categories', function () {

            if (!Cache::tags(['blog', 'blog_categories'])->has('blog_categories')) {

                Cache::tags(['blog','blog_categories'])->put('blog_categories', [
                    'blog_categories' => Category::where('status', 1)->get()->toTree(),
                ], 24 * 60);

            }

            return response(Cache::tags(['blog','blog_categories'])->get('blog_categories'));
        })->middleware('access');

    });

    Route::group(['prefix' => 'shop'], function () {

        Route::get('/', function (Request $request) {

            $sort = productSortItems(); // get sort items

            $result = [
                'filter' => [],
                'navigation' => [],
                'tree' => [],
                'brands' => [],
                'sort' => $sort,
            ];

            if (!Cache::has("shop")) {
                // get children
                $result['tree'] = \Modules\Product\Entities\Category::select('id', 'slug', 'title')->whereNull('parent_id')->get();
                // push result to caching
                $result['filter'] = \Modules\Product\Entities\Filter::all();
                Cache::put("shop", $result , 24 * 60);
            }

            $products = \Modules\Product\Entities\ProductView::select('id', 'title', 'slug', 'img')
                ->with(['types' => function($q) use($request) {
                    $q->where('status', 1);
                    $q->orderBy('price','asc');
                    $q->with(['priceParameters'=> function($q){
                        $q->where('parent_id',1);
                    }]);
                }])
                ->where(function ($q) use ($request) {
                    // stock products

                    if ($request->has('stock')) {
                        if ($request->get('stock') == 1) {
                            $q->where('count', '>', 0);
                        }
                    }
                })
                ->where('status', 1) // change to true
                ->when(true, function ($q) use($request, $sort) {
                    if ($request->has('sort')) {
                        if (isset($sort[$request->get('sort')])) {
                            if ($sort[$request->get('sort')]['field'] != "price" ){
                                $q->orderBy($sort[$request->get('sort')]['field'], $sort[$request->get('sort')]['type']);
                            }
                            else{
                                $q->where('min_price','>',0);
                                $q->where('max_price','>',0);
                                if($sort[$request->get('sort')]['type'] == 'desc') {
                                    $q->orderBy('max_price','desc');
                                }
                                if($sort[$request->get('sort')]['type'] == 'asc') {
                                    $q->orderBy('min_price','asc');
                                }
                            }
                        }
                    } else {
                        $q->orderBy('id', 'desc');
                    }
                })
                ->whereHas('filters' , function($q) use ($request){
//                    $request->filter = [6,13];
                    if ($request->filter){
                        $filters = explode(',',$request->filter);
//                        $filters = $request->filter;
                        $q->whereIn('filter_id',$filters);
                    }
                })
                ->whereHas('types', function($q) {
                    $q->where('status', 1);
                })
                ->paginate($request->get('limit') ?? 24);

            return response()->json([
                'cached' => Cache::get("shop"),
                'products' => $products
            ]);

        })->middleware('access');

        Route::get('/brand/{brand}', function ($brand, Request $request) {

            $sort = productSortItems(); // get sort items

            $cache = Cache::tags(['brand'])->remember("brand[$brand]", 24 * 60, function () use($brand, $sort) {

                $brand = \Modules\Product\Entities\Brand::with(['categories' => function($q) {
                    $q->select('id', 'title', 'slug');
                }])->select('id', 'slug', 'title', 'heading', 'content' ,'meta_title', 'meta_description')
                    ->where(is_numeric($brand) ? 'id': 'slug', $brand)
                    ->firstOrFail();


                $result = [
                    'id' => $brand->id,
                    'slug' => $brand->slug,
                    'label' => $brand->title,
                    'heading' => $brand->heading ?? '',
                    'meta_title' => $brand->meta_title,
                    'meta_description' => $brand->meta_description,
                    'content' => $brand->content,
                    'navigation' => [],
                    'tree' => [],
                    'brands' => [],
                    'attributes' => [],
                    'sort' => $sort,
                ];

                // navigation
                $result['navigation'] =  [
                    [
                        'id' => $brand->id,
                        'slug' => $brand->slug,
                        'title' => $brand->title
                    ]
                ];

                $result['tree'] = $brand->categories;

                return $result;

            });


            $products = \Modules\Product\Entities\Product::select('id', 'title', 'slug', 'img')
                ->where(function ($q) use ($request) {
                    // stock products
                    if ($request->has('stock')) {
                        if ($request->get('stock') == 1) {
                            $q->where('count', '>', 0);
                        }
                    }
                })
                ->where('brand_id', $cache['id'])
                ->where('status', 1) // change to true
//                ->where('off_price', '>',  0) // change to true
//                ->where('price', '>',  0) // change to true
                ->when(true, function ($q) use($request, $sort) {
                    if ($request->has('sort')) {
                        if (isset($sort[$request->get('sort')])) {
                            $q->orderBy($sort[$request->get('sort')]['field'], $sort[$request->get('sort')]['type']);
                        }
                    } else {
                        $q->orderBy('id', 'desc');
                    }
                })
                ->with(['types' => function($q) {
                    $q->where('status', 1);
                }])
                ->whereHas('types', function($q) {
                    $q->where('status', 1);
                    $q->where('price', '>', 0); // change to true
                    $q->where('off_price', '>', 0); // change to true
                })
                ->paginate($request->get('limit') ?? 24);


            return response()->json([
                'cached' => $cache,
                'products' => $products
            ]);
        })->middleware('access');

        // product list


        // payload home page items
        Route::get('/products/swiper', function (Request $request) {

            if (!Cache::tags(['item_product'])->has("item_product")) {
                $list = \Modules\Product\Entities\ItemProduct::select('id', 'title', 'link')
                    ->with(['products' => function($q) use($request) {
                        $q->select('id', 'title', 'slug', 'img')
                            ->with(['types' => function($q) {
                                $q->orderBy('price','asc');
                                $q->where('status', 1);
                                $q->with(['priceParameters'=> function($q){
                                    $q->where('parent_id',1);
                                }]);
                            }])
                            ->where('count', '>', 0)
                            ->where('status', 1) // change to true
                            ->orderBy($request->get('sort') ?? 'id', $request->get('type') ?? 'desc')
                            ->get();
                    }])
                    ->where('status', 1)
                    ->orderBy('order', 'asc')
                    ->get()
                    ->toArray();

                Cache::tags(['item_product'])->put("item_product", $list , 1 * 60);
            }

            return response(Cache::tags(['item_product'])->get("item_product"));
        })->middleware('access');

        Route::post('/products/addcomment',function(Request $request){

            $validator = \Validator::make($request->all(),[
                'product_id' => 'required|integer',
                'text' => 'required'
            ]);

            if ($validator->fails()){
                return response()->json(['status' => false , 'message' => $validator->errors()->first()]);
            }

            if ($request->show_name == 1){
                $request->show_name = Auth::id();
            }

            $comment = \Modules\Product\Entities\ProductComment::create([
                'product_id' => $request->product_id,
                'show_name' => $request->show_name,
                'user_id' => Auth::id(),
                'text' => $request->text
            ]);

        })->middleware(['access','auth:api']);

        Route::get('/products/{id}',function ($id,Request $request){

			$data = Cache::remember("product[$id]", 1 * 60 , function () use ($id,$request) {

                $product = \Modules\Product\Entities\Product::with(['brand', 'packageType', 'files' => function($q){
                    $q->orderBy('collection');
                    $q->orderBy('order');
                }])
                    ->with(['comments' => function($q){

                        $q->where('verify',1);
                        $q->with('showUser');
                    }])
					->with(['newRelatedProduct'=> function($q){
						$q->with(['types' => function($q) {
						}]);
					}])
                    ->with(['categories' => function($q) use ($request){
                        if($request->categories){
                            $q->where('id',$request->categories);
                        } else {
                            $q->first();
                        }
                    }])
                    ->find($id);

                $attributes = \Illuminate\Support\Facades\DB::select('call sp_frontend_product_attributes(?)', [$id]);
                $similar = \Illuminate\Support\Facades\DB::select('call sp_frontend_product_family(?)', [$id]);
//                $categories = \Illuminate\Support\Facades\DB::select('call sp_frontend_product_categories(?)', [$id]);

                if (count($similar) == 1 && !$similar[0]->id) {
                    $similar_data = [];
                } else {
                    foreach ($similar as $item) {
                        $similar_data[] = [
                            'id' => $item->id,
                            'title' => $item->title,
                            'types' => json_decode($item->types),
                            'slug' => $item->slug
                        ];
                    }
                }

                return [
                    'product' => $product,
                    'attributes' => $attributes,
                    'similar' => $similar_data,
//                    'categories' => $categories,
                ];

            });

            return response($data);

        })->middleware('access');

        Route::get('/products/{id}/priceState', function ($id) {

            $price_parameter = [];
            $group_price_parameter = DB::select('call sp_frontend_product_price_parameters(?)', [$id]);


            if ($group_price_parameter) {



                foreach ($group_price_parameter as $key => $item) {
                    if($item->parent_id && $item->parent_title) {
                        $price_parameter[] = [
                            'id' => $item->parent_id,
                            'title' => $item->parent_title,
                            'options' => \Modules\Product\Entities\PriceParameter::whereIn('id', explode(',', $item->node_values))
                                ->get(['id', 'title'])
                                ->toArray()
                        ];
                    }
                }

            }

            return response($price_parameter);

        })->middleware('access');

        Route::get('/products/{id}/priceTypes', function ($id) {


            return response(Types::with('priceParameters')
                ->where('product_id', $id)
                ->where('status', 1)

                ->orderBy('price','asc')
                ->get());

        })->middleware('access');

        Route::post('/products/{id}/pins', function ($id, Request $request) {

            $result = [];

			$product_pins = DB::select('call sp_price_parameters_get_details(?)', [$id]);


            foreach ($product_pins as $product_key => $product_value){
                foreach ($request->all() as $key => $price_parameter) {
					if (!is_int($key)) continue; # debug last developer code, not as well but works! (remove "request" and parameters that's we don't need it)
                    if (!in_array($price_parameter,json_decode($product_value->price_parameters))){
                        unset($product_pins[$product_key]);
                    }
                }
            }

//            $price_parameter = [];
//            $group_price_parameter = DB::select('call sp_frontend_product_price_parameters(?)', [$id]);
//
//			return $group_price_parameter;
//            if ($group_price_parameter) {
//                foreach ($group_price_parameter as $key=>$item) {
//                    if($item->parent_id && $item->parent_title) {
//                        $price_parameter[] = [
//                            'id' => $item->parent_id,
//                            'title' => $item->parent_title,
//                            'options' => \Modules\Product\Entities\PriceParameter::whereIn('id', explode(',', $item->node_values))->get(['id', 'title'])->toArray()
//                        ];
//                    }
//                }
//
//            }

            foreach ($product_pins as $pin) {
                $result[] = [
                    'pins' => $pin,
                    'selected' => DB::select('call sp_frontend_product_pins_price_parameters(?)', [$pin->id])
                ];
            }

            return response([
				'status'	=> true,
				'result' 	=> $result,
//				'options'=>$price_parameter
			]);

        })->middleware('access')->name('product_pins_post_frontend');

    });

    Route::prefix('card')->middleware('auth:api')->group(function () {

        Route::get('/', function () {
            $card = DB::select('call sp_card(?)', [Auth::user()->id]);
            return response()->json($card);
        })->middleware('access');

        Route::post('/', function (Request $request) {
            try {
                $card = DB::select('call sp_card_add(?, ?, ?)', [Auth::user()->id, $request->get('id'), $request->get('count')]);

                return response()->json(['status' => true, 'card' => $card]);
            } catch (Exception $exception) {
                if ($exception instanceof \Illuminate\Database\QueryException) {
                    return response()->json(['status' => false, 'msg' => $exception->getPrevious()->errorInfo[2]]);
                } else {
                    return response()->json(['status' => false, 'msg' => $exception->getMessage()]);
                }
            }
        })->middleware('access');

        Route::delete('/{id}', function ($id) {
            try {
                $card = DB::select('call sp_card_remove(?, ?)', [Auth::user()->id, $id]);
                return response()->json(['status' => true, 'card' => $card]);
            } catch (Exception $exception) {
                if ($exception instanceof \Illuminate\Database\QueryException) {
                    return response()->json(['status' => false, 'msg' => $exception->getPrevious()->errorInfo[2]]);
                } else {
                    return response()->json(['status' => false, 'msg' => $exception->getMessage()]);
                }
            }
        })->middleware('access');

    });

    Route::prefix('address')->middleware('auth:api')->group(function () {

        Route::get('/', function () {
            $address = DB::select('call sp_address(?)', [Auth::user()->id]);
            return response()->json($address);
        })->middleware('access');

        Route::post('/', function (Request $request) {

            try {
                $validator = \Validator::make($request->all(), [
                    'reciver_name' => 'required',
                    'region_id' => 'required',
                    'postal_code' => 'max:10',
                    'reciver_mobile' => 'required|min:11|max:11',
                    'reciver_national_code' => 'max:10',
                ]);

                if ( $validator->fails() ) {
                    return Response()->json(['status' => false, 'message' => $validator->errors()->first()]);
                }

                if (!$request->get('main')) {
                    return Response()->json(['status' => false, 'message' => 'آدرس را وارد کنید']);
                }

                $address = DB::select('call sp_address_add(?, ?, ?, ?, ?, ?, ?, ?, ?)', [
                    Auth::user()->id,
                    last(explode(',', trim($request->get('region_id'), ','))),
                    $request->has('lat') ? $request->get('lat') : null,
                    $request->has('lng') ? $request->get('lng') : null,
                    $request->get('main'),
                    $request->get('postal_code'),
                    $request->get('reciver_name'),
                    $request->get('reciver_mobile'),
                    $request->get('reciver_national_code'),
                ]);
                return response()->json(['status' => true, 'address' => $address]);
            } catch (Exception $exception) {
                return response()->json(['status' => false, 'msg' => $exception->getPrevious()->errorInfo[2]]);
            }
        })->middleware('access');

        Route::put('/{id}', function ($id, Request $request) {
            try {
                $address = DB::select('call sp_address_update(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [
                    $id,
                    Auth::user()->id,
                    $request->get('region_id'),
                    $request->has('lat') ? $request->get('lat') : null,
                    $request->has('lng') ? $request->get('lng') : null,
                    $request->get('main'),
                    $request->get('postal_code'),
                    $request->get('reciver_name'),
                    $request->get('reciver_mobile'),
                    $request->get('reciver_national_code'),
                ]);
                return response()->json(['status' => true, 'address' => $address]);
            } catch (Exception $exception) {
                return response()->json(['status' => false, 'msg' => $exception->getPrevious()->errorInfo[2]]);
            }
        })->middleware('access');

        Route::delete('/{id}', function ($id) {
            try {
                $address = DB::select('call sp_address_deleted(?, ?)', [$id, Auth::user()->id]);
                return response()->json(['status' => true, 'address' => $address]);
            } catch (Exception $exception) {
                return response()->json(['status' => false, 'card' => $exception->getPrevious()->errorInfo[2]]);
            }
        })->middleware('access');
    });

    Route::prefix('gateway')->group(function () {
        /**
         * gateway parsian, pasargad
         * invoice_id
         */
        Route::post('/', function (Request $request) {

            $order = \Modules\Order\Entities\Order::where('token',$request->get('invoice_id'))->first();

            switch ($request->get('paymentType')) {
                case 'online':
                    try {
                        if ($order->status !== 0) return response(['status' => false, 'msg' => 'این فاکتور منقضی شده است. فاکتور جدید ایجاد کنید']);
                        if ($order->user_id !== Auth::id()) return response(['status' => false, 'msg' => 'این فاکتور متعلق به شما نیست']);
                        if (in_array($order->status, [1])) return response(['status' => false, 'msg' => 'این فاکتور پرداخت شده است']);


                        Auth::user()->update([
                            'remember_token' => quickRandom(),
                        ]);
                        $gateway = Payment::saman();
                        $gateway->setAmount($order->total_pay * 10); // * 10


                        $gateway->setInvoice($order->id);
						$gateway->setDescription('پرداخت فاکتور');
						$transaction = $gateway->ready();

						DB::table('transaction')->where('id', $transaction->id)->update([
                            'remember_token' => Auth::user()->getRememberToken()
                        ]);

						$gateway->setCallback("https://api.digiattar.com/api/gateway/callback/?uuid=$transaction->id");
//                        $gateway->setCallback("https://digiattar.com/invoice/$order->token");

						return $gateway->redirect();


					} catch (Exception $exception) {
                        return response()->json(['status' => false, 'msg' => $exception->getMessage()]);
                    }
                    break;
                case 'voucher':


                    if ($order->status !== 0) return response(['status' => false, 'msg' => 'این فاکتور منقضی شده است. فاکتور جدید ایجاد کنید']);
                    if ($order->user_id !== Auth::id()) return response(['status' => false, 'msg' => 'این فاکتور متعلق به شما نیست']);
                    if (in_array($order->status, [1])) return response(['status' => false, 'msg' => 'این فاکتور پرداخت شده است']);

                    if (Auth::user()->voucher < $order->total_pay) {
                        return response()->json(['status' => false, 'msg' => 'مبلغ سفارش بیشتر از موجودی است.موچودی حساب خود را افزایش دهید.']);
                    }

                    $order->private = 1;
                    $order->status = 1;
                    $order->is_voucher = 1;
                    if ($order->save()) {
                        return response()->json(['status' => true, 'msg' => 'فاکتور پرداخت و مبلغ از بن کالا کسر گردید.']);
                    }

                    return response()->json(['status' => false, 'msg' => 'خطایی رخ داده است.']);
                    break;
                case 'wallet':

                    if ($order->status !== 0) return response(['status' => false, 'msg' => 'این فاکتور منقضی شده است. فاکتور جدید ایجاد کنید']);
                    if ($order->user_id !== Auth::id()) return response(['status' => false, 'msg' => 'این فاکتور متعلق به شما نیست']);
                    if (in_array($order->status, [1])) return response(['status' => false, 'msg' => 'این فاکتور پرداخت شده است']);

                    if (Auth::user()->wallet < $order->total_pay) {
                        return response()->json(['status' => false, 'msg' => 'مبلغ سفارش بیشتر از موجودی است.موچودی حساب خود را افزایش دهید.']);
                    }

                    $order->status = 1;
                    $order->is_wallet = 1;
                    if ($order->save()) {
                        return response()->json(['status' => true, 'msg' => 'فاکتور پرداخت و مبلغ از موجودی کسر گردید.']);
                    }

                    return response()->json(['status' => false, 'msg' => 'خطایی رخ داده است.']);
                    break;

                    break;
            }
        })->middleware(['access','auth:api']);

        Route::any('/callback', function (Request $request) {

            /**
             * get callback parameters from app
             */
            $uuid = $request->get('uuid');

            $transaction = \Modules\Transaction\Entities\Transaction::find($uuid);

            if ($transaction->status == 'success'){
                return view('bank-error', ['msg' => 'این فاکتور قبلا پرداخت شده است.']);
            }

            $order = \Modules\Order\Entities\Order::find($transaction->transactionable_id);
            $user = User::where('id', $order->user_id)->where('remember_token', $transaction->remember_token)->first();
            if ( ! $user ) return view('bank-error', ['msg' => 'ارسال درخواست نامعتبر, کاربر پرداخت کننده با سفارش دهنده منطبق نیست.']);

            $user->update([
//                'remember_token' => quickRandom()
            ]);

            $gateway = Payment::saman();

            $gateway->setParameter($request->all());
            $result = $gateway->verify($transaction); // return status, msg

            if ($result['status']) {

                $order->status = 1;
                $order->paid = 1;

                if ($order->save()) {
                    // save transaction
                    $transaction->transactionable_id = $order->id;
                    if ($transaction->save()) {
                        return redirect()->away(env('WEB_URL') . '/invoice/' . $order->token);
                    }
                }

            } else {
//                return view('bank-error', ['msg' => $result['msg'], 'callback' => env('WEB_URL') . '/invoice/' . $order->token]);
                return redirect()->away(env('WEB_URL') . '/invoice/' . $order->token);
            }

        })->middleware('access')->name('gateway_callback');

        Route::prefix('wallet')->group(function () {

            Route::post('/', function (Request $request) {


                $gateway_setting = DB::table('domain')->where('key', env('APP_NAME'))->value('gateway');
                $gateway = $request->has('gateway') ? $request->get('gateway') : $gateway_setting;


                Auth::user()->update([
                    'remember_token' => quickRandom(),
                ]);


                switch ($gateway) {
                    case 'parsian':
                        $gateway = Payment::parsian();
                        $gateway->setAmount($request->get('amount') * 10);
                        break;
                    case 'pasargad':
                        $gateway = Payment::pasargad();
                        $gateway->setAmount($request->get('amount') * 10);
                        break;
                    case 'saderat':
                        $gateway = Payment::saderat();
                        $gateway->setAmount($request->get('amount') * 10);
                        break;
                    case 'zarinpal':
                        $gateway = Payment::zarinpal();
                        $gateway->setAmount($request->get('amount'));
                        break;
                }


                $gateway->setDescription('افزایش موجودی کیف پول');
                $transaction = $gateway->ready();
                DB::table('transaction')->where('id', $transaction->id)->update([
                    'type' => 'creditor',
                    'finance' => 'wallet',
                    'remember_token' => Auth::user()->getRememberToken()
                ]);

                $gateway->setCallback("https://api.beenetwork.ir/api/gateway/wallet/callback/?uuid=$transaction->id");

                return $gateway->redirect();


            })->middleware('access','auth:api');

            Route::any('/callback', function (Request $request) {

                /**
                 * get callback parameters from app
                 */
                $uuid = $request->get('uuid');

                $transaction = \Modules\Transaction\Entities\Transaction::find($uuid);
                if ($transaction->status == 'success')   return view('bank-error', ['msg' => 'این فاکتور قبلا پرداخت شده است.']);

                $user = User::where('id', $transaction->user_id)->where('remember_token', $transaction->remember_token)->first();

                if ( ! $user ) return view('bank-error', ['msg' => 'ارسال درخواست نامعتبر, کاربر پرداخت کننده با سفارش دهنده منطبق نیست.']);

                $user->update([
                    'remember_token' => quickRandom()
                ]);

                /**
                 * get bank parameters
                 */
                switch ($transaction->gateway) {
                    case 'zarinpal':
                        $gateway = Payment::zarinpal();
                        $gateway->setRefId($request->get('Authority'));
                        $gateway->setAmount($transaction->amount);
                        break;
                    case 'parsian':
                        $gateway = Payment::parsian();
                        break;
                    case 'pasargad':
                        $gateway = Payment::pasargad();
                        break;
                    case 'saderat':
                        $gateway = Payment::saderat();
                        break;
                }

                $gateway->setParameter($request->all());

                $result = $gateway->verify($transaction); // return status, msg

                if (!$result['status']) {
                    return view('bank-error', ['msg' => $result['msg'], 'callback' => env('WEB_URL') . '/dashboard/transactions/' . $transaction->id]);
                }

                return redirect()->away(env('WEB_URL') . '/dashboard/transactions/' . $transaction->id);

            })->middleware('access')->name('gateway_wallet_callback');

        });

    });

    Route::prefix('order')->middleware('auth:api')->group(function () {

        Route::get('/{id}', function ($id, Request $request) {

			try {
                $order_id = Order::where('token',$id)->first();
                $order = DB::select('call sp_order_info(?)', [$order_id->id]);
                if ($order[0]->user_id != Auth::id()) {
                    return response()->json(['status' => false, 'msg' => 'این سفارش متعلق به شما نیست.']);
                }

                return response()->json(['status' => true, 'order' => $order[0]]);
            } catch (Exception $exception) {
                if ($exception instanceof \Illuminate\Database\QueryException) {
                    return response()->json(['status' => false, 'msg' => $exception->getPrevious()->errorInfo[2]]);
                } else {
                    return response()->json(['status' => false, 'msg' => $exception->getMessage()]);
                }
            }

        })->middleware('access');

        Route::get('/',function (){

            $orders = Order::where('user_id',Auth::id())
                ->get();

            return response()->json($orders);

        })->middleware('access');

        Route::post('/', function (Request $request) {

            $validator = \Validator::make($request->all(), [
                'address_id' => 'required',
            ]);

            if ( $validator->fails() ) {
                return Response()->json(['status' => false, 'message' => $validator->errors()->first()]);
            }
            try {
                $address = \Modules\Address\Entities\Address::where('token',$request->address_id)->first();
                $order = DB::select('call sp_card_To_order(?, ?)', [Auth::id(), $address->id]);
                if (isset($order[0]->err)) {
                    return response()->json(['status' => false, 'err' => $order[0]->err]);
                }
                return response()->json(['status' => true, 'id' => $order[0]->order_id , 'token' => $order[0]->token]);

            } catch (Exception $exception) {
                if ($exception instanceof \Illuminate\Database\QueryException) {
                    return response()->json(['status' => false, 'msg' => $exception->getPrevious()->errorInfo[2]]);
                } else {
                    return response()->json(['status' => false, 'msg' => $exception->getMessage()]);
                }
            }
        })->middleware('access');
    });

    Route::get('/region', function () {
        if (!Cache::tags(['region'])->has("region")) {

            $regions = \Modules\Region\Entities\Region::get()->toTree();

            Cache::tags(['region'])->put("region", $regions , 24 * 60);
        }

        return response(Cache::tags(['region'])->get('region'));
    })->middleware('access');

    Route::prefix('contact')->group(function () {

        Route::post('/', function (Request $request) {

            $validator = \Validator::make($request->all(), [
                'name' => 'required|min:3',
                'mobile' => 'required|min:11|max:11',
                'content' => 'required',
                'captcha' => 'required'
            ]);


            if ( $validator->fails() ) {
                return Response()->json(['status' => false, 'msg' => $validator->errors()->first()]);
            }

            $result = \Modules\Contact\Entities\Contact::create([
                'name' => $request->get('name'),
                'mobile' => $request->get('mobile'),
                'email' => $request->get('email'),
                'content' => $request->get('content'),
            ]);

            if ($result) {
                return Response()->json(['status' => true, 'msg' => 'پیام شما با موفقیت ثبت گردید.']);
            }

            return Response()->json(['status' => false, 'msg' => 'خطایی رخ داده است با پشتیبانی تماس بگیرید.']);
        })->middleware('access');

    });

    Route::group(['prefix' => 'temperaments'], function () {

        Route::get('/',function (){

            $temperaments = \Modules\Temperament\Entities\Temperament::select('temperament_title', 'temperament_description')
                ->get();

            return response()->json($temperaments);

        })->middleware('access');

        Route::get('/{id}', function ($id, Request $request) {

            $temperament = \Modules\Temperament\Entities\Temperament::select('temperament_title', 'temperament_description')
                ->find($id);

            return response()->json($temperament);

        })->middleware('access');

    });

    Route::prefix('/determinetemperament')->group(function (){

        Route::post('/calculate',function (Request $request){

            //0 baraye garm ya khoshk boodane natijeye javab

            $coefficient_cold_and_hot = 100 / \Modules\Temperament\Entities\TemperamentQuestions::select(DB::raw("COUNT('id') as count"))->where('parent_temperament_id',1)->groupBy('parent_temperament_id')->first()->toArray()['count'];
            $coefficient_dry_and_more = 100 / \Modules\Temperament\Entities\TemperamentQuestions::select(DB::raw("COUNT('id') as count"))->where('parent_temperament_id',2)->groupBy('parent_temperament_id')->first()->toArray()['count'];

            $answer_array = [];
//dd($answers);
            $temperament_attributes = \Modules\Temperament\Entities\TemperamentAnswers::select('temperament_attribute_id')->groupBy('temperament_attribute_id')->get();

            foreach ($temperament_attributes as $i){
                $answer_array[$i->temperament_attribute_id] = [];
            }

            foreach ($request->get('list') as $key => $value){
                $answer_array[$value["value"]][] = $value["key"];
            }

            $percent_of_hot = $coefficient_cold_and_hot * count($answer_array[3]);
            $percent_of_cold = $coefficient_cold_and_hot * count($answer_array[4]);
            $percent_of_dry = $coefficient_dry_and_more * count($answer_array[5]);
            $percent_of_more = $coefficient_dry_and_more * count($answer_array[6]);


            if (50 < $percent_of_hot){
                $cold_and_hot = 3;
                if (50 < $percent_of_dry){
                    $dry_and_more = 5;
                }
                if (50 < $percent_of_more){
                    $dry_and_more = 6;
                }
                else{
                    $dry_and_more = 7;
                }
            }
            if (50 < $percent_of_cold){
                $cold_and_hot = 4;
                if (50 < $percent_of_dry){
                    $dry_and_more = 5;
                }
                if (50 < $percent_of_more){
                    $dry_and_more = 6;
                }
                else{
                    $dry_and_more = 7;
                }
            }
            else{
                $cold_and_hot = 7;
                $dry_and_more = 7;
            }

            $temperament = \Modules\Temperament\Entities\Temperament::Select('temperament_title','temperament_message')
                ->where('temperament_cold_or_hot',$cold_and_hot)
                ->where('temperament_dry_or_more',$dry_and_more)
                ->first();

            $location = [
                "cold" => $percent_of_cold,
                "hot" => $percent_of_hot,
                "more" => $percent_of_more,
                "dry" => $percent_of_dry
            ];

            $data = [
                'temperament' => $temperament,
                'location' => $location,
            ];

            return response()->json(['status' => true,'data' => $data]);

        });

        Route::post('/save',function (Request $request){

            $record = MedicalRecords::where('token',$request->get('record_id'))
                ->first();

            if ($record){

                $determine_temperament = \Modules\Temperament\Entities\DetermineTemperament::create([
                    'temperament_id_answer_1' 		=> $request->get('list')[0]['value'],
                    'temperament_id_answer_2' 		=> $request->get('list')[1]['value'],
                    'temperament_id_answer_3' 		=> $request->get('list')[2]['value'],
                    'temperament_id_answer_4' 		=> $request->get('list')[3]['value'],
                    'temperament_id_answer_5' 		=> $request->get('list')[4]['value'],
                    'temperament_id_answer_6' 		=> $request->get('list')[5]['value'],
                    'temperament_id_answer_7' 		=> $request->get('list')[6]['value'],
                    'temperament_id_answer_8' 		=> $request->get('list')[7]['value'],
                    'temperament_id_answer_9' 		=> $request->get('list')[8]['value'],
                    'temperament_id_answer_10' 		=> $request->get('list')[9]['value'],
                    'temperament_id_answer_11' 		=> $request->get('list')[10]['value'],
                    'temperament_id_answer_12' 		=> $request->get('list')[11]['value'],
                    'temperament_id_answer_13' 		=> $request->get('list')[12]['value'],
                    'temperament_id_answer_14' 		=> $request->get('list')[13]['value'],
                    'temperament_id_answer_15' 		=> $request->get('list')[14]['value'],
                    'temperament_id_answer_16' 		=> $request->get('list')[15]['value'],
                    'user_id' 						=> Auth::id(),
                    'percent_cold' 					=> $request->get('cold'),
                    'percent_hot' 					=> $request->get('hot'),
                    'percent_dry' 					=> $request->get('dry'),
                    'percent_more' 					=> $request->get('more'),
                    'record_id' 					=> $record->id
                ]);

                return response()->json(['status' => true,'data' => $determine_temperament]);

            }

            else{
                return response()->json(['status' => false , 'message'=> 'دسترسی به پرونده پزشکی مورد نظر امکان پذیر نیست']);
            }

        })->middleware(['auth:api']);

        Route::get('/form',function (){

            $questions = \Modules\Temperament\Entities\TemperamentQuestions::with(['answers'])
                ->get();

            return response()->json($questions);

        })->middleware('access');

        Route::put('/delete/{id}',function ($id,Request $request){

            $determine_temperament = \Modules\Temperament\Entities\DetermineTemperament::where('token',$id)
                ->update([
					'deleted_at' => Carbon::now()
                ]);

            return response()->json($determine_temperament);

        })->middleware(['auth:api','access']);

        Route::get('/{id}',function ($id){

            $detemine_temperament = \Modules\Temperament\Entities\DetermineTemperament::where('token',$id)
                ->get();

            return response()->json($detemine_temperament);

        });

    });

    Route::get('allproducts',function (){

        $products = \Modules\Product\Entities\Product::all();

        return response()->json($products);

    })->middleware('access');

});



/*
|-------------------------------------------------------------------------
|  All File And Media Router
|--------------------------------------------------------------------------
|
| Store File In Attachment Directory
| This Directory Contain All Media
| Before Insert In DataBase
| Original File Save
| Past Parameters Are file,Directory
|
*/
Route::group(['prefix' => 'attachment', 'middleware', 'auth:api'], function () {

    Route::post('/', function (Request $request) { // Get Form Data

        // Check File Mime Type
        if (in_array($request->file('file')->getMimeType(), ['image/gif', 'image/png', 'image/jpg', 'image/jpeg'])) {
            // Image Size Larger Than 1MB
            if ($request->file('file')->getSize() / 1024 > 1024) {
                return response()->json(['status' => false, 'msg' => 'حداکثر حجم فایل 1 مگابایت است']);
            }
            //Video Check Mime Type
        }
        elseif (in_array($request->file('file')->getMimeType(), ['video/mp4', 'video/ogv', 'video/webm', 'video/3gpp'])) {
            if ($request->file('file')->getSize() / 1024 > 8388608) {
                return response()->json(['status' => false, 'msg' => 'حداکثر حجم فایل 8 مگابایت است']);
            }
        }
        elseif (in_array($request->file('file')->getMimeType(), ['application/zip', 'application/x-rar'])) {
            if ($request->file('file')->getSize() > 8388608) {
                return response()->json(['status' => false, 'msg' => 'حداکثر حجم فایل 8 مگابایت است']);
            }
        }
        elseif (in_array($request->file('file')->getMimeType(), ['application/pdf'])) {
            if ($request->file('file')->getSize() > 8388608) {
                return response()->json(['status' => false, 'msg' => 'حداکثر حجم فایل 8 مگابایت است']);
            }
        }
        else { // Other Format is InValid
            return response()->json(['status' => false, 'msg' => 'فرمت غیر مجاز است.']);
        }


        //With Storage Laravel File System Save File In Attachment Directory
        $path = $request->file('file')->store($request->has('directory') ? $request->get('directory') : '/attachment' , 'public');

        return response()->json([
            'status' => true,
            'msg' => 'ok',
            'path' => Storage::url($path) ,
            'file' => last(explode('/', $path))
        ]);
    });
});

/**
 * search permission and add to database
 *
 * init permission
 */
Route::get('/init', function (Request $request) {

    $routeCollection = \Route::getRoutes()->get();
    // Get all Routes
    foreach ($routeCollection as $key => $route) {

        $action = $route->getAction();

        if (is_array($action['middleware'])) {

            if (in_array('auth:api', $action['middleware'])) {


                // For Routes That Define In Controller
                if (isset($action['controller'])) {
                    // explode action and get single controller
                    $explode_path = explode('@', @$action['controller']); // expload controller, App\Http\Controllers\Backend\AnbarController@index

                    // check controller is valid
                    if (count($explode_path) == 2) {

                        // get action name
                        preg_match_all('/(?:^|[A-Z])[a-z]+/',$explode_path[1],$action_name); // create array that contain 0 index and more tha one key, getAttribute get Attribute or Get Attribute


                        // expload action and get last part , OrderController => 0, Order => 1
                        preg_match('/(.*)Controller/', last(explode("\\", $explode_path[0])), $matches);
                        preg_match_all('/(?:^|[A-Z])[a-z]+/',$matches[1],$controller_name); // create array that contain 0 index and more tha one key, ProductCategory product category or Product Category

                        // get module name
                        preg_match('/\w+\\\\(.*)\\\\Http/', $explode_path[0], $module_container);
                        preg_match_all('/(?:^|[A-Z])[a-z]+/',$module_container[1],$module_name);



                        // join array Product Category Or get Attribute
                        $final[mb_strtolower(join('_', $module_name[0]))][mb_strtolower(join('_', $module_name[0])) . '_' .mb_strtolower(join('_', $controller_name[0]))][] = [
                            'key' => mb_strtolower(join('_', $module_name[0])) . '_' .mb_strtolower(join('_', $controller_name[0])) . '_' . mb_strtolower(join('_', $action_name[0])),
                            'action' => trans('permissions.' . mb_strtolower(join(' ', $action_name[0]))),
                            'method' => $route->methods[0],
                            'url' => '/'.$route->uri,
                        ];
                    }
                }
            }
        }

    }


    foreach ($final as $key=>$value) {
        foreach ($value as $k=>$item) {
            foreach ($item as $i=>$f) {
                \Modules\User\Entities\Permission::updateOrCreate(['id' => $f['key']],[
                    'id' => $f['key'],
                    'url' => $f['url'],
                    'method' => $f['method'],
                    'title' => trans('permission.' .  $f['key']),
                    'parent' => $key
                ]);
            }

        }
    }

    return $final;
});


