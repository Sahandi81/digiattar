<?php

namespace Modules\Medical\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class MedicalRecords extends Model
{
	# New update, use `rebuild` api to change columns from deleted to deleted_at
	use SoftDeletes;
    use HasFactory;

    protected $fillable = [
        'user_id',
        'title',
        'birth_date',
        'is_male',
    ];

	protected $hidden = [
		'user_id',
		'deleted_at',
	];

    protected $table = 'medical_records';

	public function folders(): \Illuminate\Database\Eloquent\Relations\HasMany
	{
		return $this->hasMany(MedicalFolder::class, 'record_id', 'id');
	}

}
