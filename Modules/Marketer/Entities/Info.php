<?php

namespace Modules\Marketer\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Info extends Model
{
	# New update, use `rebuild` api to change columns from deleted to deleted_at
	use SoftDeletes;

    protected $table = 'marketer_info';

    protected $primaryKey = 'marketer_id';

    public $incrementing = false;

    public $timestamps = false;

    protected $guarded = [];



}
