<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductHasFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_has_filters', function (Blueprint $table) {
            $table->integer('product_id');
            $table->integer('filter_id');
            $table->boolean('is_main')->default(false);
            $table->integer('created_by')->default(1);
            $table->softDeletes();

            $table->primary(['product_id', 'filter_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_has_filters');
    }
}
