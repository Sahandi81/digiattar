<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepotOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('depot_order', function (Blueprint $table) {
            $table->integer('depot_id');
            $table->integer('order_id');
            $table->tinyInteger('factor')->default(0);
            $table->tinyInteger('label')->default(0);
            $table->softDeletes();

            $table->primary(['depot_id', 'order_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('depot_order');
    }
}
