<?php

use Illuminate\Support\Facades\Route;
use Modules\Order\Http\Controllers\OrderController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('backend')->group(function () {

    Route::prefix('orders')->middleware(['auth:api', 'admin_access'])->group(function () {

        Route::get('/', 							[OrderController::class, 'index']);
        Route::get('/{id}', 						[OrderController::class, 'invoice']);
        Route::post('/{id}/status',					[OrderController::class, 'updateStatus'])						->name('admin_update_status');
        Route::get('/{id}/notes', 'OrderController@notes');
        Route::post('/{id}/notes', 'OrderController@addNotes');
        Route::get('/{id}/array', 					[OrderController::class, 'printArrayInvoice']);

    });


});
