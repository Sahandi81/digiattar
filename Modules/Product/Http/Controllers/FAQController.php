<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Response;
use Modules\Product\Entities\FAQ;

class FAQController extends Controller
{

	public function destroy(int $id): \Illuminate\Http\JsonResponse
	{
		if (FAQ::findOrFail($id)->delete()){
			return Response::json(['status' => true, 'با موفقیت حذف شد.']);
		}
		return Response::json(['status' => false,'msg' => 'مشکلی پیش آمده است.']);
	}

}
