<?php

namespace Modules\Region\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Region\Entities\Region;

class RegionController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function index(Request $request)
    {
        return response(Region::defaultOrder()->get()->toTree());
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     *
     * fetch all place for replace node
     */
    public function findMapState($id)
    {

        return response([
            'item' => Region::select('id', 'title')->find($id),
            'list' => Region::where('id', '<>' , $id)->get(),
        ]);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function dispachMove($id, Request $request)
    {
        $parent = $request->get('parent');

        try {
            $node = Region::find($id);
            $node->parent_id = $parent;
            $node->save();

        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()]);
        }

        return response()->json(['status' => true, 'msg' => 'انتقال با موفقیت انجام شد.']);

    }

    public function store(Request $request)
    {

        $frm = json_decode($request->get('form'), true);


        $validator = \Validator::make($frm, [
            'title' => 'required',
        ]);

        if ($validator->fails()) {
            return Response()->json(['status' => false, 'msg' => $validator->errors()->first()]);
        }

        $new_node = explode(',', trim($frm['title'], ','));
        $selected = $request->get('checked');
        if (!empty($selected)) {
            foreach ($selected as $item) {
                foreach ($new_node as $new) {
                    $node = new Region([
                        'title' => trim($new),
                    ]);
                    $node->appendToNode(Region::find($item));
                    $node->save();
                }
            }
            return response()->json(['status' => true, 'msg' => 'با موفقیت ایجاد شدند.']);
        } else {
            foreach ($new_node as $new) {
                Region::create([
                    'title' => $new,
                ]);
            }
            return response()->json(['status' => true, 'msg' => 'با موفقیت ایجاد شد.']);
        }
    }

    public function show($id, Request $request) {

        $entity = Region::find($id);

        $list = [
            'id' 			=> $entity->id,
            'title' 		=> $entity->title,
            'is_posting' 	=> $entity->is_posting,
            'is_bearing' 	=> $entity->is_bearing,
            'is_delivery' 	=> $entity->is_delivery,
            'in_person' 	=> $entity->in_person,
            'status' 		=> $entity->status,
            'deleted' 		=> $entity->deleted,
        ];
        return response($list);
    }


    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {


        $validator = \Validator::make($request->all(), [
            'title' => 'required',
        ]);

        if ($validator->fails()) {

            return Response()->json(['status' => false, 'msg' => $validator->errors()->first()]);
        }


        $result = Region::find($id);

        $frm = $request->all();

        $result->update([
            'title' 		=> $frm['title'],
            'status' 		=> $frm['status'] ?? 0,
//            'deleted' 		=> $frm['deleted'] ?? 0,
            'is_posting' 	=> $frm['is_posting'] ?? 0,
            'is_bearing' 	=> $frm['is_bearing'] ?? 0,
            'is_delivery' 	=> $frm['is_delivery'] ?? 0,
            'in_person' 	=> $frm['in_person'] ?? 0,
        ]);
		if ($frm['deleted']){
			$result->delete(); # :(( hey
		}

        $nodes = Region::descendantsAndSelf($id);

        foreach ($nodes as $node) {
            $node->status 			= $frm['status'] ?? 0;
//            $node->deleted = $frm['deleted'] ?? 0;
            $node->is_posting 		=  $frm['is_posting'] ?? 0;
            $node->is_bearing 		=  $frm['is_bearing'] ?? 0;
            $node->is_delivery 		=  $frm['is_delivery'] ?? 0;
            $node->in_person 		=  $frm['in_person'] ?? 0;
            $node->save();
			if ($frm['deleted']){
				$node->delete(); # :(( hey
			}
        }


        return response()->json(['status' => true, 'msg' => 'با موفقیت به روز رسانی گردید']);
    }

}
