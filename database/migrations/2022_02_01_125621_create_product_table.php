<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('created_by')->default(1)->index('created_by');
            $table->bigInteger('modified_by')->nullable()->index('modified');
            $table->unsignedBigInteger('brand_id')->nullable()->index('FK_products_brand');
            $table->integer('package_type_id')->index('package_type_id');
            $table->string('hash', 50)->nullable()->unique('hash_code');
            $table->string('slug')->nullable();
            $table->string('code', 50)->nullable();
            $table->string('title');
            $table->string('heading')->nullable();
            $table->unsignedInteger('count')->default('0');
            $table->unsignedInteger('visitor')->default('0');
            $table->boolean('status')->default(true)->index('status');
            $table->longText('content')->nullable();
            $table->mediumText('short_content')->nullable();
            $table->string('meta_title')->nullable();
            $table->mediumText('meta_description')->nullable();
            $table->string('img')->nullable();
            $table->string('license', 50)->nullable();
            $table->boolean('is_pack')->default(false);
            $table->dateTime('created_at')->nullable();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
