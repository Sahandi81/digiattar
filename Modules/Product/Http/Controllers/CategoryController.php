<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Modules\Product\Entities\Attribute;
use Modules\Product\Entities\Brand;
use Modules\Product\Entities\Category;
use Modules\Product\Entities\FAQ;
use Modules\Product\Entities\Filter;
use Modules\Product\Entities\Types;


# After delete useless methods rename currently methods. for ex: indexProducts ==> index
class CategoryController extends Controller
{

	public function indexProducts(Request $request, string $category): \Illuminate\Http\JsonResponse
	{

		$sort = productSortItems(); // get sort items
		$result = [
			'filter' 			=> [],
			'navigation' 		=> [],
			'tree' 				=> [],
			'brands' 			=> [],
			'sort' 				=> $sort,
		];

		if (! Cache::has("shop")) {
			// get children
			$result['tree'] = \Modules\Product\Entities\Category::select('id', 'slug', 'title')->whereNull('parent_id')->get();
			// push result to caching
			$result['filter'] = \Modules\Product\Entities\Filter::all();
			Cache::put("shop", $result , 24 * 60);
		}

		$cache = Cache::tags(['product_categories'])->remember("category[$category]", 24 * 60, function () use($category, $sort) {


			$category = \Modules\Product\Entities\Category::select('id', 'slug', 'title', 'heading' ,'meta_title', 'meta_description', 'content')
				->where(is_numeric($category) ? 'id' : 'slug', $category)
				->first();


			$result = [
				'id' 				=> $category->id,
				'slug' 				=> $category->title,
				'label' 			=> $category->label,
				'heading' 			=> $category->heading ?? '',
				'meta_title' 		=> $category->meta_title,
				'meta_description' 	=> $category->meta_description,
				'content' 			=> $category->content,
				'navigation' 		=> [],
				'tree' 				=> [],
				'brands' 			=> [],
				'attributes' 		=> [],
				'sort' 				=> $sort,
			];


			// navigation
			$result['navigation'] =  \Modules\Product\Entities\Category::select('id', 'slug', 'title')->ancestorsAndSelf($category->id);

			// get children
			$result['tree'] = \Modules\Product\Entities\Category::select('id', 'slug', 'title')->where('parent_id', $category->id)->get();

			// get all brands
			$result['brands'] = $category->brands()->select('id', 'slug', 'title')->where('status', 1)->get();

			// get all attr
			$result['attributes'] = $category->attributes()->select('id', 'title', 'slug', 'has_link', 'content')->with(['tags'])->where('status', 1)->get();

			//get all filter
			$result['filter'] = \Modules\Product\Entities\Filter::where('status',1)->get();

			return ['result' => $result, 'category' => $category];

		});

		$products = $cache['category']->products()->select('id', 'title', 'slug', 'img')
			->where(function ($q) use ($request) {
				// stock products
				if ($request->has('stock')) {
					if ($request->get('stock') == 1) {
						$q->where('count', '>', 0); // change to >
					}
				}
				// brand filter
				if ($request->has('brands') && $request->get('brands')) {
					$q->whereIn('brand_id', $request->get('brands'));
				}
			})
			->where('status', 1) // change to true
			->when(true, function ($q) use($request, $sort) {

				if ($request->has('sort')) {
					if (isset($sort[$request->get('sort')])) {

						if (in_array($sort[$request->get('sort')]['field'], ['price', 'sales_number', 'discount', 'title'])){
							$q->orderBy(Types::select($sort[$request->get('sort')]['field'])->whereColumn('product_types.product_id', 'product.id')
								->orderByDesc($sort[$request->get('sort')]['field'])->limit(1), $sort[$request->get('sort')]['type']);
						}

					}
				} else {
					$q->orderBy('id', 'desc');
				}
			})
			->with(['types' => function($q) {
				$q->where('status', 1);
				$q->orderBy('price','asc');
				$q->with(['priceParameters'=> function($q){
					$q->where('parent_id',1);
				}]);
			}])
			->whereHas('types', function($q) {
				$q->where('price', '>', 0); // change to true
				$q->where('off_price', '>', 0); // change to true
			})
			->with('filters' , function ($q) use ($request){
				if ($request->filter){
					$filters = $request->filter;
					$q->whereIn('filter_id',$filters);
				}
			})
			->whereHas('attributes' , function($q) use ($request){
				if ($request->selected_tags){
					$q->whereIn('tag_id',$request->selected_tags);
				}
			})
			->paginate($request->get('limit') ?? 24);

		return response()->json([
			'cached' => $cache['result'],
			'products' => $products
		]);

	}




























	# I don't have any idea wtf is these methods, if u feel the same way just press backspace key ..::Sahandi81::..
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     */
    public function index(Request $request)
    {
        return response(Category::defaultOrder()->get()->toTree());
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|Response
     *
     * fetch all place for replace node
     */
    public function findMapState($id)
    {

        return response([
            'item' => Category::select('id', 'title')->find($id),
            'list' => Category::where('id', '<>' , $id)->get(),
        ]);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function dispachMove($id, Request $request)
    {
        $parent = $request->get('parent');

        try {
            $node = Category::find($id);
            $node->parent_id = $parent;
            $node->save();

        } catch (\Exception $exception) {
            return response()->json(['status' => false, 'message' => $exception->getMessage()]);
        }

        return response()->json(['status' => true, 'msg' => 'انتقال با موفقیت انجام شد.']);

    }

    public function store(Request $request)
    {

        $frm = json_decode($request->get('form'), true);


        $validator = \Validator::make($frm, [
            'title' => 'required',
        ]);

        if ($validator->fails()) {
            return Response()->json(['status' => false, 'msg' => $validator->errors()->first()]);
        }

        $new_node = explode(',', trim($frm['title'], ','));
        $selected = $request->get('checked');
        if (!empty($selected)) {
            foreach ($selected as $item) {
                foreach ($new_node as $new) {
                    $node = new Category([
                        'title' => trim($new),
                    ]);
                    $node->appendToNode(Category::find($item));
                    $node->save();
                }
            }
            return response()->json(['status' => true, 'msg' => 'با موفقیت ایجاد شدند.']);
        } else {
            foreach ($new_node as $new) {
                Category::create([
                    'title' => $new,
                ]);
            }
            return response()->json(['status' => true, 'msg' => 'با موفقیت ایجاد شد.']);
        }
    }

    public function show($id, Request $request) {
        $entity = Category::with('faqs')->find($id);
        return response($entity);
    }


    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'title' => 'required',
			'FAQ'	=> 'array|min:1',
			'FAQ.*'	=> 'array|min:2',
        ]);

        if ($validator->fails()) {

            return Response()->json(['status' => false, 'msg' => $validator->errors()->first()]);
        }

		# AnyWhere see 'Fields', UPDATED BY #SAHANDI81, just updated, code isn't mine -_-
		$fields = $validator->validated();

        $result = Category::find($id);

        $frm = $request->all();

        $result->update([
            'status' 			=> $frm['status'],
            'title' 			=> $frm['title'],
            'slug' 				=> $frm['slug'] ? remove_special_char($frm['slug']) : remove_special_char($frm['title']),
            'meta_title' 		=> $frm['meta_title'] ?? $frm['title'],
            'meta_description' 	=> $frm['meta_description'] ?? $frm['title'],
//            'content' => $frm['content'],
        ]);

//        $nodes = Category::descendantsAndSelf($id); # get children, wtf? and set deleted true (commented for now)
		try {
			# Delete records need specific API, BUT we go from wrong way, why ? CUZ we don't want to update frontEnd,
				# again why? "WE DON'T HAVE TIME !", why? really I don't know anymore
			if ($frm['deleted']){
				 Category::where('parent_id', $id)->delete();
			}
		} catch (\PDOException $exception){
			return response()->json(['status' => false, 'msg' => $exception->getMessage(), $exception->getLine(), $exception->getFile()]);

		}


		if (isset($fields['FAQ'])){
			foreach ($fields['FAQ'] as $FAQ) {
				$FAQ['category_id'] = $id;
				$result->faqs()->updateOrCreate(['id' => $FAQ['id'] ?? null], $FAQ);
			}
		}


        return response()->json(['status' => true, 'msg' => 'با موفقیت به روز رسانی گردید', 'details' => Category::with('faqs')->find($id)]);
    }


    public function filters($id, $title, Request $request) {

        $list = [];
        switch ($title) {
            case 'brands':

                $list['data'] = DB::select('call sp_product_category_assignable_brands_with_checked(?, ?, ?)', [$id, $request->get('page'), $request->get('title') ?? '']);

                $list['total'] = $request->get('title') == '' ? Brand::count() : count($list['data']);

                break;
            case 'attributes':

                $list['data'] = DB::select('call sp_product_category_assignable_attributes_with_checked(?, ?, ?)', [$id, $request->get('page'), $request->get('title') ?? '']);

                $list['total'] = $request->get('title') == '' ? Attribute::count() : count($list['data']);
                break;
            case 'filters':

                $list['data'] = DB::select('call sp_product_category_assignable_filters_with_checked(?, ?, ?)', [$id, $request->get('page'), $request->get('title') ?? '']);

                $list['total'] = $request->get('title') == '' ? Filter::get()->toTree()->count() : count($list['data']);
                break;
        }

        return response($list);
    }



    public function selectedFilters($id, $title) {
        $model = Category::find($id);
        switch ($title) {
            case 'brands':
                return response($model->brands);
                break;
            case 'attributes':
                return response($model->attributes);
                break;
            case 'filters':
                return response($model->filters);
                break;
        }
    }

    /**
     * @param $id
     * @param $title
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function storeFilters($id, $title, Request $request) {
        switch ($title) {
            case 'brands':
                $entities = Category::descendantsAndSelf($id);
                foreach ($entities as $entity) {
                    $entity->brands()->attach([$request->get('id') => ['type' => 'brand']]);
                }

                return response(['status' => true, 'msg' => 'ok']);
                break;
            case 'attributes':
                $entities = Category::descendantsAndSelf($id);
                foreach ($entities as $entity) {
                    try {
                        $entity->attributes()->attach([$request->get('id') => ['type' => 'attribute']]);
                    } catch (\Exception $exception) {
                        continue;
                    }
                }
                return response(['status' => true, 'msg' => 'ok']);
                break;
            case 'filters':
                $entities = Category::descendantsAndSelf($id);
                foreach ($entities as $entity) {
                    $entity->filters()->attach([$request->get('id') => ['type' => 'filter']]);
                }
                return response(['status' => true, 'msg' => 'ok']);
                break;
        }
    }

    public function removeFilters($id, $title, Request $request) {

        switch ($title) {
            case 'brands':
                $entities = Category::descendantsAndSelf($id);
                foreach ($entities as $entity) {
                    DB::table('product_category_assignable')
                        ->where('category_id', $entity->id)
                        ->where('assignable_id', $request->get('id'))
                        ->where('type', 'brand')
                        ->delete();
                }

                return response(['status' => true, 'msg' => 'ok']);
                break;
            case 'attributes':
                $entities = Category::descendantsAndSelf($id);
                foreach ($entities as $entity) {
                    DB::table('product_category_assignable')
                        ->where('category_id', $entity->id)
                        ->where('assignable_id', $request->get('id'))
                        ->where('type', 'attribute')
                        ->delete();
                }
                return response(['status' => true, 'msg' => 'ok']);
                break;
            case 'filters':
                $entities = Category::descendantsAndSelf($id);
                foreach ($entities as $entity) {
                    DB::table('product_category_assignable')
                        ->where('category_id', $entity->id)
                        ->where('assignable_id', $request->get('id'))
                        ->where('type', 'filter')
                        ->delete();
                }
                return response(['status' => true, 'msg' => 'ok']);
                break;
        }
    }

}
