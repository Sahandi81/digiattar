<?php

namespace Modules\Asnaf\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Product\Entities\Product;

class AsnafProduct extends Model
{
	# New update, use `rebuild` api to change columns from deleted to deleted_at
	use SoftDeletes;
    use HasFactory;

    protected $table = 'asnaf_product';

    protected $guarded = [];

    protected $primaryKey = 'id';

    protected static function newFactory()
    {
        return \Modules\Asnaf\Database\factories\AsnafProductFactory::new();
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }
}
