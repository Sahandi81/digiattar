<?php

namespace App\Jobs\Marketer;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Modules\Marketer\Entities\Marketer;

class MarketerRevoke implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $marketer_id;

    /**
     * MarketerRevoke constructor.
     * @param $marketer_id
     */
    public function __construct($marketer_id)
    {
        $this->marketer_id = $marketer_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        Marketer::revoke($this->marketer_id);

        Marketer::where('id', $this->marketer_id)->delete();
    }
}
