<?php

namespace Modules\Like\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Modules\Like\Entities\Like;
use Modules\Product\Entities\Product;

class LikeController extends Controller
{

	public function store($id): \Illuminate\Http\JsonResponse
	{
		$product = Product::find($id);

		if ($product){

			$result = DB::selectOne('call sp_like_system(?, ?)', [Auth::id(), $id]);
			if ($result->msg == 'SUCCESS')
				return Response::json(['status' => true, 'msg' => 'محصول با موفقیت به لیست علاقه‌مندی شما افزوده شد.']);
			elseif ($result->msg == 'DUPLICATE_LIKE')
				return Response::json(['status' => false, 'msg' => 'محصول مورد نظر از قبل به علاقه‌مندی اضافه شده است.']);
			else
				return Response::json(['status' => false, 'like_system return bad data.', 'file' => __FILE__, 'line' => __LINE__]);
		}

		return Response::json(['status' => false, 'msg' => 'محصول مورد نظر یافت نشد.']);
	}

	public function destroy($id): \Illuminate\Http\JsonResponse
	{
		$product = Product::find($id);
		if ($product){
			$result = Like::where('parent_id', $id)->where('user_id', Auth::id())->delete();
			if ($result)
				return Response::json(['status' => true, 'msg' => 'محصول با موفقیت از لیست علاقه‌مندی حذف گردید.']);
			else
				return Response::json(['status' => false, 'msg' => 'قبلا محصول مورد نظر را از لیست علاقه‌مندی خود حذف کرده‌اید.']); # Actually doesn't mean like this BUT np

		}
		return Response::json(['status' => false, 'msg' => 'محصول مورد نظر یافت نشد.']);
	}

}
