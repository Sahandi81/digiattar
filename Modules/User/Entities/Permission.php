<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permission extends Model
{
	# New update, use `rebuild` api to change columns from deleted to deleted_at
	use SoftDeletes;

    protected $table = 'permission';

    protected $primaryKey = 'id';

    protected $guarded = [];

    protected $keyType = 'string';

    protected $hidden = ['pivot'];
}
