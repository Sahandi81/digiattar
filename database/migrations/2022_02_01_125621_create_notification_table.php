<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('user_id');
            $table->integer('period_id')->nullable();
            $table->text('text')->nullable()->comment('اگر خالی باشد هنوز پیام نرفته است');
            $table->string('template', 50)->nullable();
            $table->string('token1', 50)->nullable();
            $table->string('token2', 50)->nullable();
            $table->string('token3', 50)->nullable();
            $table->string('token4', 50)->nullable()->comment('4 space');
            $table->string('token5', 50)->nullable()->comment('8  space');
            $table->boolean('status')->default(false)->comment('pending, success, faild, onQueue');
            $table->longText('error_log')->nullable();
            $table->boolean('level')->default(false)->comment('high, medium, low');
            $table->dateTime('created_at')->nullable();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->softDeletes();

            $table->index(['user_id', 'period_id', 'status'], 'user_id_period_id_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification');
    }
}
