<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToDomainHasLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('domain_has_links', function (Blueprint $table) {
            $table->foreign(['domain_key'], 'FK_domain_has_links_domain')->references(['key'])->on('domain')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('domain_has_links', function (Blueprint $table) {
            $table->dropForeign('FK_domain_has_links_domain');
        });
    }
}
