<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTriggerCreatedAtToVisitFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::select('
        CREATE DEFINER = CURRENT_USER
 		TRIGGER created_at	
		BEFORE INSERT	
		ON visit_forms 
		FOR EACH ROW
        BEGIN

			SET NEW.created_at = NOW();
	
		END
');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trigger_created_at_to_visit_forms');
    }
}
