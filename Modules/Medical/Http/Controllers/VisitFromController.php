<?php

namespace Modules\Medical\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Modules\Medical\Entities\VisitForm;
use Modules\Medical\Http\Requests\VisitFromRequest;
use Modules\User\Entities\User;

class VisitFromController extends Controller
{

	public function store(VisitFromRequest $request): \Illuminate\Http\JsonResponse
	{
		$fields = $request->validated();
		foreach ($fields['answers'] as $key => $answer){
			$fields['answers'][$key]['user_id'] 	= Auth::id();
			$fields['answers'][$key]['folder_id'] 	= $fields['folder_id'];
		}

		try {

			if (! VisitForm::insert($fields['answers'])){
				return Response::json(['status' => false, 'msg' => 'مشکلی پیش آمده است']);
			}
			return Response::json(['status' => true, 'msg' => 'با موفقیت ثبت شد.']);

		} catch (\PDOException $exception){
			return Response::json(['status' => false, 'msg' => 'مجاز به ایجاد یک فرم دیگر در این پوشه نیستید.']);
		}
	}


	public function update(Request $request): \Illuminate\Http\JsonResponse
	{
		$fields = $request->validate([
			'form_question_id'  		=> 'required|numeric',
			'answer'			  		=> 'required|string',
		]);

		$form = User::with([
			'form' => fn($q) => $q->where('id', $fields['form_question_id'])->first()
		])
			->find(Auth::id());

		if (! $form->form) {
			return Response::json(['status' => false, 'msg' => 'نتیجه مناسبی یافت نشد.']);
		}

		$form->form
			->update(['answer' => $fields['answer']]);

		return Response::json(['status' => true, 'msg' => 'سوال مورد نظر با موفقیت به روز رسانی شد.']);

	}

}
