<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use http\Env\Url;
use Illuminate\Http\Request;
use Illuminate\Support\Env;
use Illuminate\Support\Facades\App;
use Modules\User\Entities\Log;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class Access
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        if ($request->has('encrypt'))
            return $next($request);
        
        if (App::environment() == 'local') {
            return $next($request);
        }
        // url that not using this middleware
        if (in_array($request->route()->getName(), [
            'category_update',
            'gateway_callback',
            'gateway_wallet_callback',
            'put_role_permissions',
            'domain_links',
            'profile_image_upload',
            'asnaf_product_post',
            'asnaf_branch_post',
            'product_seo_put',
            'blog_seo_put',
            'product_types_store',
            'product_post_attribute',
            'product_pins_post_frontend'
        ])) {
            return $next($request);
        }

        // openssl decrypt
        $key = hex2bin("c0ff70cc197a07dff1fb709688170426");
        $iv =  hex2bin("f8b4e45085a1045902c3c69c80e67a7c");

        $decrypted = openssl_decrypt($request->get('request'), 'AES-128-CBC', $key, OPENSSL_ZERO_PADDING, $iv);

        $decrypted = trim($decrypted); // you have to trim it according to https://stackoverflow.com/a/29511152


        foreach (explode(';', $decrypted) as $item) {

            if (preg_match('/(.*):({.*})/', $item, $matches)) {
                $request->request->add([trim($matches[1])  => $matches[2]]);
            } elseif (preg_match('/(.*):(\[.*\])/', $item, $matches)) {
                $request->request->add([trim($matches[1])  => json_decode($matches[2])]);
            } elseif (preg_match('/.*:.*/', $item)) {

                $input = explode(':', $item);

                // ignore for admin
                if (!preg_match('/backend/', $request->route()->getPrefix())) {

                    if (!in_array($input[1], ['captcha'])) {

                        if (!preg_match('/{.*}/', $input[0])) {
                            if (preg_match('/select|union|select|insert|drop|delete|update|cast|create|convert|alter|declare|order|script|md5|benchmark|encode/', strtolower($input[0]))) {
                                $msg = "firewall - code 01  $input[0] with value $input[1]";
                                return response(['status' => false, 'msg' => $msg], 403);
                            }
                        }

                        if (!preg_match('/{.*}/', $input[1])) {
                            if (preg_match('/\'|\"|\<|\>|select|union|select|insert|drop|delete|update|cast|create|convert|alter|declare|order|script|md5|benchmark|encode/', strtolower($input[1]))) {
                                $msg = "firewall - code 02 input $input[0] with value $input[1]";
                                return response(['status' => false, 'msg' => $msg], 403);
                            }
                        }
                    }
                }

                if ($input[1] == 'null' || $input[1] == "" || $input[1] == "NULL") $input[1]= null;
                if (is_string($input[1]) && $input[1] == 'true' || $input[1] == "true") $input[1] = 1;
                if (is_string($input[1]) && $input[1] == 'false' || $input[1] == "false") $input[1] = 0;

                $request->request->add([trim($input[0])  => $input[1]]);

            }


            $logger = new Logger('inputs');
            $logger->pushHandler(new StreamHandler(storage_path().'/logs/inputs.log'));
            $logger->info($input[0]. ': ' . $input[1]);


        }

//        if ( Carbon::now()->unix() > $request->get('time')) {
//            return response(['status' => false, 'msg' => 'firewall - code 04'], 400);
//        }


        if (env('APP_ENV') === 'production') {

            if (! in_array($request->get('origin'), ['digiattar.com', 'admin.digiattar.com', 'localhost'])) {
                $msg = 'firewall - code 05';
                return response(['status' => false, 'msg' => $msg], 400);
            }

        }


        if ($request->has('captcha')) {
            $secret   = env('captcha');
            $response = file_get_contents(
                "https://www.google.com/recaptcha/api/siteverify?secret=" . $secret . "&response=" . $request->get('captcha') . "&remoteip=" . $_SERVER['REMOTE_ADDR']
            );
            // use json_decode to extract json response
            $response = json_decode($response);

            if ($response->success === false) {
                return response(['status' => false, 'msg' => 'به دلایل تشخیص ربات صفحه رو مجدد بارگزاری نمایید و در صورت استفاده از فیلترشکن آن را خاموش کنید.']);
            }


            if ($response->score < 0.5) {
                return response(['status' => false, 'msg' => 'به دلایل تشخیص ربات لطفا چند دقیقه بعد مجددا امتحان کنید']);
            }
        }


        unset($request['request'], $request['time'], $request['random'], $request['origin']);


        return $next($request);
    }
}
