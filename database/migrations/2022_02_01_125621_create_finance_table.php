<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinanceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('finance', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->string('financeable_id', 50)->nullable()->index('financeable_id');
            $table->string('financeable_type')->nullable();
            $table->integer('user_id')->index('user_id');
            $table->integer('period_id')->index('period_id');
            $table->string('title')->nullable();
            $table->bigInteger('creditor')->default(0);
            $table->bigInteger('debtor')->default(0);
            $table->enum('type', ['voucher', 'wallet'])->default('voucher');
            $table->timestamp('created_at')->useCurrentOnUpdate()->useCurrent();
            $table->softDeletes();

            $table->index(['financeable_id', 'financeable_type'], 'financeable_id_financeable_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('finance');
    }
}
