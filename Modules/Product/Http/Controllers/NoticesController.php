<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Modules\Product\Entities\Notices;
use Modules\Product\Entities\Product;

class NoticesController extends Controller
{

	public function store($id): \Illuminate\Http\JsonResponse
	{
		$product = Product::find($id);

		if ($product) {
			$result = DB::selectOne('call sp_reminder(?, ?)', [Auth::id(), $id]);
			if ($result->msg == 'SUCCESS')
				return Response::json(['status' => true, 'msg' => 'به هنگام موجود شدن محصول به شما اطلاع رسانی خواهد شد.']);
			elseif ($result->msg == 'DUPLICATE_LIKE')
				return Response::json(['status' => false, 'msg' => 'قبلا یادآوری این محصول برای شما فعال شده است.']);
			else
				return Response::json(['status' => false, 'like_system return bad data.', 'file' => __FILE__, 'line' => __LINE__]);
		}

		return Response::json(['status' => false, 'msg' => 'محصول مورد نظر یافت نشد.']);
	}


	public function destroy($id): \Illuminate\Http\JsonResponse
	{
		$product = Product::find($id);
		if ($product){
			$result = Notices::where('product_id', $id)->where('user_id', Auth::id())->delete();
			if ($result)
				return Response::json(['status' => true, 'msg' => 'یادآوری محصول غیرفعال شد.']);
			else
				return Response::json(['status' => false, 'msg' => 'قبلا یادآوری محصول مورد نظر را غیرفعال کرده‌اید.']); # Actually doesn't mean like this BUT np

		}
		return Response::json(['status' => false, 'msg' => 'محصول مورد نظر یافت نشد.']);
	}

}
