<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReagentCommissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reagent_commission', function (Blueprint $table) {
            $table->integer('marketer_id');
            $table->integer('period_id');
            $table->enum('node', ['A', 'B', 'C', 'D', 'E']);
            $table->bigInteger('lft_sales')->default(0);
            $table->bigInteger('rgt_sales')->default(0);
            $table->bigInteger('lft_wallet')->default(0);
            $table->bigInteger('rgt_wallet')->default(0);
            $table->bigInteger('value')->default(0);
            $table->enum('type', ['continuous', 'conclusion'])->default('continuous');
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->softDeletes();

            $table->primary(['marketer_id', 'period_id', 'node']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reagent_commission');
    }
}
