<?php


return [
    "asnaf_asnaf_branch_list" => "لیست شعب",

    "asnaf_asnaf_branch_store" => "ایجاد شعبه جدید",

    "asnaf_asnaf_product_list" => "لیست محصولات ارسالی به اصناف",

    "asnaf_asnaf_product_store" => "ارسال محصول جدید به اصناف",

    "blog_category_dispach_move" => "جا به جایی یک دسته",

    "blog_category_find_map_state" => "لیست موارد جابه جایی یک دسته",

    "blog_category_index" => "دسته بندی وبلاگ",

    "blog_category_show" => "نمایش یک دسته بندی از وبلاگ",

    "blog_category_store" => "ایجاد دسته بندی وبلاگ",

    "blog_category_update" => "ویرایش یک دسته از وبلاگ",

    "blog_content_index" => "لیست پست های وبلاگ",

    "blog_content_init_show" => "نمایش اطلاعات اولیه یک پست",

    "blog_content_init_store" => "ذخیره اطلاعات اولیه پست",

    "blog_content_init_update" => "اپدیت اطلاعات اولیه یک پست",

    "blog_content_show" => "نمایش پست",

    "blog_content_status" => "تغییر وضعیت یک پست",

    "blog_content_store" => "ایجاد یک پست",

    "blog_content_update" => "ویرایش یک پست",

    "blog_menu_dispach_move" => "جابه جایی یک دسته از منو",

    "blog_menu_find_map_state" => "لیست موارد جابه جایی یک دسته از منو",

    "blog_menu_index" => "نمایش منوهای سایت",

    "blog_menu_show" => "نمایش یک دسته از منو",

    "blog_menu_store" => "ذخیره یک دسته از منو",

    "blog_menu_update" => "ویرایش یک دسته از منو",

    "contact_contact_change_status" => "تغییر وضعیت یک دسته از منو",

    "contact_contact_index" => "ارتباط با ما",

    "depot_depot_index" => "انبار",

    "finanical_finance_group" => "امور مالی و کیف پول همراه با جزئیات",

    "finanical_finance_index" => "امور مالی و کیف پول کلی",

    "form_request_form_request_confirmation" => "تایید درخواست",

    "form_request_form_request_index" => "لیست درخواست ها",

    "form_request_form_request_store" => "ایجاد یک درخواست",

    "gallery_gallery_index" => "گالری تصاویر و اسلایدر",

    "gallery_gallery_show" => "نمایش محتوای یک اسلایدر و یا گالری تصاویر",

    "gallery_gallery_status" => "تغییر وضعیت یک اسلایدر و یا گالری تصاویر",

    "gallery_gallery_store" => "ایجاد یک اسلایدر و یا گالری تصاویر",

    "gallery_gallery_update" => "به روز رسانی یک اسلایدر و یا گالری تصاویر",

    "marketer_marketer_add_address" => "ایجاد ادرس برای بازاریاب",

    "marketer_marketer_address" => "لیست ادرس های بازاریاب",

    "marketer_marketer_change_password" => "تغییر رمز بازارایاب",

    "marketer_marketer_change_status" => "تغییر وضعیت بازاریاب",

    "marketer_marketer_get_marketer_code" => "گرفتن کد وزارت برای بازاریاب",

    "marketer_marketer_index" => "لیست بازاریاب ها",

    "marketer_marketer_marketer_map_reports" => "عملکرد بازاریاب",

    "marketer_marketer_marketer_sales_daily_report" => "نمودار عملکرد بازاریاب",

    "marketer_marketer_revoke" => "عملیات لغو و بازگردانی بازاریاب",

    "marketer_marketer_show" => "نمایش اطلاعات بازارایاب",

    "marketer_marketer_store" => "ایجاد یک بازاریاب",

    "marketer_marketer_update" => "ویرایش اطلاعات بازارایاب",

    "notification_notification_index" => "صف ارسال و نوتیفیکیشن",

    "order_order_add_notes" => "ایجاد یادداشت برای سفارش",

    "order_order_index" => "لیست سفارشات",

    "order_order_invoice" => "نمایش فاکتور",

    "order_order_notes" => "نمایش یادداشت های یک سفارش",

    "order_order_print_array_invoice" => "پرینت گروهی سفارشات",

    "period_period_export" => "خروجی دوره های زمانی",

    "period_period_index" => "لیست دوره های زمانی",

    "period_period_map_report" => "خلاصه وضعیت دوره",

    "period_period_qualified" => "خروجی واجدین شرایط پورسانت",

    "period_period_sales_daily_report" => "نمودار رشد دوره زمانی",

    "product_attribute_index" => "ویژگی های محصول",

    "product_attribute_show" => "نمایش ویژگی های محصول",

    "product_attribute_status" => "فعال و غیرفعال کردن ویژگی ها",

    "product_attribute_store" => "ایجاد یک ویژگی برای محصول",

    "product_attribute_update" => "ویرایش ویژگی های محصول",

    "product_brand_index" => "لیست برندها",

    "product_brand_show" => "نمایش یک برند",

    "product_brand_status" => "تغییر وضعیت برند",

    "product_brand_store" => "ایجاد یک برند",

    "product_brand_update" => "ویرایش یک برند",

    "product_category_dispach_move" => "جابه جایی دسته بندی محصول",

    "product_category_filters" => "فیلترهای دلخواه دسته بندی محصول",

    "product_category_find_map_state" => "لیست موارد جابه جایی دسته بندی محصول",

    "product_category_index" => "دسته بندی محصول",

    "product_category_remove_filters" => "حذف فیلترهای دلخواه دسته بندی محصول",

    "product_category_selected_filters" => "لیست فیلترهای انتخاب شده از دسته بندی محصول",

    "product_category_show" => "نمایش دسته بندی محصول",

    "product_category_store" => "ایجاد دسته بندی محصول",
    "product_category_update" => "ویرایش دسته بندی محصول",

    "product_item_product_index" => "لیست محصولات ایتمی صفحه اول سایت",

    "product_item_product_show" => "نمایش اطلاعات محصولات ایتمی صفحه اول سایت",

    "product_item_product_status" => "تغییر وضعیت محصولات ایتمی صفحه اول سایت",

    "product_item_product_store" => "ایجاد محصولات ایتمی صفحه اول سایت",

    "product_item_product_update" => "ویرایش محصولات ایتمی صفحه اول",

    "product_package_type_index" => "نوع بسته بندی محصول",

    "product_package_type_show" => "نمایش یک نوع بسته بندی محصول",

    "product_package_type_store" => "ایجاد نوع بسته بندی محصول",

    "product_package_type_update" => "ویرایش نوع بسته بندی محصول",

    "product_product_edit" => "ویرایش محصول",

    "product_product_index" => "نمایش محصولات",

    "product_product_init_show" => "نمایش اطلاعات اولیه محصول",

    "product_product_init_store" => "ایجاد اطلاعات اولیه محصول",

    "product_product_init_update" => "ویرایش اطلاعات اولیه محصول",

    "product_product_order_pins" => "لاگ کسر موجودی ها",

    "product_product_product_pins" => "لاگ موجودی های محصول",

    "product_product_status" => "تغییر وضعیت محصول",

    "product_product_store_pins" => "ویرایش و تغییر موجودی محصول",

    "reward_reward_deposit" => "تعیین وضعیت پرداخت پاداش",

    "reward_reward_detail" => "جزیئیات یک پاداش",

    "reward_reward_index" => "نمایش پورسانت ها",

    "reward_reward_point" => "امتیازهای",

    "reward_reward_reagent" => "معرفان",

    "reward_reward_retail" => "خرده فروشی",

    "reward_reward_reward_as_reward" => "پورسانت از پورسانت",

    "reward_reward_saving" => "نمایش پتانسیل رشد",

    "setting_setting_read" => "نمایش تنظیمات کلی",

    "setting_setting_read_domain_links" => "نمایش تنظیمات لینک ها ( لود شدن )",

    "setting_setting_read_sticky" => "نمایش تنظیمات رادیویی ( لود شدن )",

    "setting_setting_update" => "ویرایش تنطیمات کلی",

    "setting_setting_update_domain_links" => "تنظیمات لینک ها",

    "setting_setting_update_sticky" => "تنظیمات رادیویی سایت",

    "ticket_category_dispach_move" => "تغییر موقعیت یک دسته",

    "ticket_category_document" => "ایجاد راهنما برای تیکت",

    "ticket_category_find_map_state" => "لیست جابه جایی های یک نود",

    "ticket_category_index" => "دسته بندی",

    "ticket_category_show" => "اطلاعات یک دسته بندی",

    "ticket_category_store" => "ایجاد دسته بندی",

    "ticket_category_update" => "ویرایش یک دسته بندی",

    "ticket_ticket_conversations" => "نمایش پاسخ های تیکت",

    "ticket_ticket_delete_conversation" => "حذف پاسخ تیکت",

    "ticket_ticket_index" => "نمایش تیکت ها",

    "ticket_ticket_operator_report" => "عملکرد اپراتور",

    "ticket_ticket_store" => "ایجاد تیکت از ادمین",

    "ticket_ticket_store_conversations" => "پاسخ به تیکت",

    "ticket_ticket_update" => "تغییر وضعیت تیکت",

    "transaction_transaction_index" => "نمایش تراکنش ها",

    "transaction_transaction_show" => "نمایش اطلاعات تراکنش",

    "user_login_logout" => "خروج کاربر",

    "user_role_has_permissions" => "نقش های کنونی کاربر",

    "user_role_index" => "نمایش نقش ها",

    "user_role_set_permissions" => "تعیین سطح دسترسی",

    "user_role_show" => "نقش ها",

    "user_role_store" => "تغییر نقش",

    "user_user_change_password" => "تغییر پسورد",

    "user_user_change_status" => "تغییر وضعیت کاربر",

    "user_user_index" => "لیست کاربران",

    "user_user_log" => "لاگ کابران",

    "user_user_show" => "نمایش اطلاعات کاربر",

    "user_user_store" => "ایجاد کاربر",

    "user_user_update" => "ویرایش اطلاعات کاربر",


];



?>
