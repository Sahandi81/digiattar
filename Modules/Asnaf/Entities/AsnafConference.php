<?php

namespace Modules\Asnaf\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class AsnafConference extends Model
{
	# New update, use `rebuild` api to change columns from deleted to deleted_at
	use SoftDeletes;
    use HasFactory;

    protected $table = 'asnaf_conference';

    protected $guarded = [];

    protected $primaryKey = 'id';

    protected static function newFactory()
    {
        return \Modules\Asnaf\Database\factories\AsnafConferenceFactory::new();
    }
}
