<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ProductView extends Model
{
	# New update, use `rebuild` api to change columns from deleted to deleted_at
	use SoftDeletes;
    use HasFactory;

    protected $fillable = [];

    protected static function newFactory()
    {
        return \Modules\Product\Database\factories\ProductViewFactory::new();
    }

    protected $table = "product_view";

    public function types()
    {
        return $this->hasMany(Types::class,'product_id');
    }

    // one to many relations
    public function files()
    {
        return $this->morphMany(File::class, 'fileable');
    }

    public function filters()
    {
        return $this->belongsToMany(Filter::class, 'product_has_filters', 'product_id', 'filter_id');
    }

}
