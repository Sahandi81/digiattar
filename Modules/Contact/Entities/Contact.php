<?php

namespace Modules\Contact\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Contact extends Model
{
	# New update, use `rebuild` api to change columns from deleted to deleted_at
	use SoftDeletes;
    use HasFactory;

    protected $table = 'contact';
    protected $guarded = [];

    protected static function newFactory()
    {
        return \Modules\Contact\Database\factories\ContactFactory::new();
    }
}
