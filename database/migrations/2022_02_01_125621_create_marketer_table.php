<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarketerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketer', function (Blueprint $table) {
            $table->integer('id')->primary();
            $table->integer('parent_id')->nullable()->index('parent_id');
            $table->integer('level')->index('level');
            $table->boolean('position')->index('position');
            $table->string('ancestry', 2000)->index('ancestry');
            $table->string('marketer_code', 20)->nullable()->unique('marketer_code');
            $table->integer('subset')->default(0);
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketer');
    }
}
