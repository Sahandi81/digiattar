<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddH2TitlesToContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('content', function (Blueprint $table) {
			$table->json('h2_titles')->nullable(); # Why nullable? CUZ ===>>> SQLSTATE[23000]: Integrity constraint violation: 4025 CONSTRAINT `content.h2_titles` failed for `digi`.`content` (SQL: alter table `content` add `h2_titles` json not null)
        }); # u can fix it later
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('content', function (Blueprint $table) {
			$table->dropColumn('h2_titles');
        });
    }
}
