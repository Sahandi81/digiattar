<?php

namespace Modules\Period\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Period\Entities\Period;

class PeriodController extends Controller
{


    public function index(Request $request)
    {


        $entities = Period::with(['month'])->where(function ($q) use($request) {

                if ($request->get('month_id') && $request->get('month_id') != "") {
                    $q->where('parent_id', $request->get('month_id'));
                }

                if ($request->get('period_id') && $request->get('period_id') != "") {
                    $q->where('id', $request->get('period_id'));
                }

            })->orderBy($request->get('sort_field') ?? 'id', $request->get('sort_type') ?? 'desc');

        if ($request->has('excel_export') && $request->get('excel_export') == true) {
            $entities = $entities->get();
        } else {
            $entities = $entities->paginate($request->get('limit') ?? 100);
        }


        return response($entities);

    }

    public function salesDailyReport(Request $request)
    {
        try {

            $report = \Illuminate\Support\Facades\DB::select('call sp_sales_daily_report(?, ?)', [
                $request->has('month_id') && $request->get('month_id') ? $request->get('month_id')  : 0,
                $request->has('period_id') && $request->get('period_id') ? $request->get('period_id') : 0,
            ]);

            return response($report);

        } catch (\Exception $exception) {
            if ($exception instanceof \Illuminate\Database\QueryException) {
                return response()->json(['status' => false, 'msg' => $exception->getPrevious()->errorInfo[2]]);
            } else {
                return response()->json(['status' => false, 'msg' => $exception->getMessage()]);
            }
        }
    }

    public function mapReport(Request $request)
    {
        try {

            $report = \Illuminate\Support\Facades\DB::select('call sp_map_report(?,?)', [
                $request->has('month_id') && $request->get('month_id') ? $request->get('month_id')  : 0,
                $request->has('period_id') && $request->get('period_id') ? $request->get('period_id') : 0,
            ]);

            return response($report);

        } catch (\Exception $exception) {
            if ($exception instanceof \Illuminate\Database\QueryException) {
                return response()->json(['status' => false, 'msg' => $exception->getPrevious()->errorInfo[2]]);
            } else {
                return response()->json(['status' => false, 'msg' => $exception->getMessage()]);
            }
        }
    }

    public function export($period_id)
    {

        try {

            $data = DB::select('call sp_reward_make_excel(?)', [$period_id]);

            return response(['status' => true, 'data' => $data]);

        } catch (\Exception $exception) {
            if ($exception instanceof \Illuminate\Database\QueryException) {
                return response()->json(['status' => false, 'msg' => $exception->getPrevious()->errorInfo[2]]);
            } else {
                return response()->json(['status' => false, 'msg' => $exception->getMessage()]);
            }
        }
    }

    public function qualified($period_id)
    {
        try {

            $data = DB::select('call sp_marketer_qualified(?)', [$period_id]);

            return response(['status' => true, 'data' => $data]);

        } catch (\Exception $exception) {
            if ($exception instanceof \Illuminate\Database\QueryException) {
                return response()->json(['status' => false, 'msg' => $exception->getPrevious()->errorInfo[2]]);
            } else {
                return response()->json(['status' => false, 'msg' => $exception->getMessage()]);
            }
        }
    }
}
