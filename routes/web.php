<?php

use App\Http\Controllers\RebuildDatabaseController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('rebuild', 							[RebuildDatabaseController::class, 'updateDeletedColumns']);

Route::get('rebuild/add_deleted_at_table', 		[RebuildDatabaseController::class, 'addDeletedAt']);



Route::get('/invoice/{token}', function ($token){
	return redirect(env('WEB_URL') .DIRECTORY_SEPARATOR. 'invoice' .DIRECTORY_SEPARATOR. $token);
});


