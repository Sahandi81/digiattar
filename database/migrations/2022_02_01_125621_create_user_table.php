<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('role_id', 50)->default('marketer')->index('role_id');
            $table->string('email', 100)->nullable()->unique('email');
            $table->string('mobile', 50)->nullable()->unique('mobile');
            $table->string('username', 50)->nullable()->unique('username');
            $table->string('name', 50)->nullable();
            $table->string('family', 50)->nullable();
            $table->integer('verify_code')->nullable();
            $table->boolean('mobile_verify')->default(false);
            $table->boolean('status')->default(true);
            $table->string('password')->nullable();
            $table->rememberToken();
            $table->integer('wallet')->default(0);
            $table->integer('voucher')->default(0);
            $table->dateTime('created_at')->nullable();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->string('pic', 100)->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
