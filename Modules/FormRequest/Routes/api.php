<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('backend')->group(function () {

    Route::prefix('requests')->middleware(['auth:api', 'admin_access'])->group(function () {

        Route::group(['prefix' => '/categories'], function () {
            Route::get('/', function () {
                return response(\Modules\FormRequest\Entities\FormCategory::whereNull('parent_id')->get());
            });
        });


        Route::get('/', 'FormRequestController@index');
        Route::put('/{id}', 'FormRequestController@confirmation');

    });
});
