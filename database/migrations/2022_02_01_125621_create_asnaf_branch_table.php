<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAsnafBranchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asnaf_branch', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('branch_code', 50);
            $table->integer('city')->index('city');
            $table->string('manager_name', 50);
            $table->string('manager_mobile', 11);
            $table->string('manager_nationalcode', 10);
            $table->string('postal_code', 10);
            $table->text('address');
            $table->integer('request_id');
            $table->string('message');
            $table->boolean('status')->default(false);
            $table->dateTime('created_at')->nullable();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asnaf_branch');
    }
}
