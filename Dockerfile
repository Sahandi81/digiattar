FROM php:7.4-fpm


# Set working directory
WORKDIR /var/www/digiattar/api

# Copy composer.lock and composer.json into the working directory
COPY composer.lock composer.json /var/www/digiattar/api/


# Install dependencies for the operating system software
RUN apt-get update && apt-get install -y \
    build-essential \
    libpng-dev \
    libjpeg62-turbo-dev \
    libfreetype6-dev \
    locales \
    zip \
    jpegoptim optipng pngquant gifsicle \
    vim \
    libzip-dev \
    unzip \
    git \
    libonig-dev \
    curl \
    nano \
    supervisor

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install extensions for php
RUN docker-php-ext-install pdo_mysql zip exif pcntl

ENV REDIS_VERSION 5.3.0

RUN curl -L -o /tmp/redis.tar.gz https://github.com/phpredis/phpredis/archive/$REDIS_VERSION.tar.gz \
    && tar xfz /tmp/redis.tar.gz \
    && rm -r /tmp/redis.tar.gz \
    && mkdir -p /usr/src/php/ext \
    && mv phpredis-* /usr/src/php/ext/redis

RUN docker-php-ext-configure gd --with-freetype --with-jpeg
RUN docker-php-ext-install gd redis
# Install composer (php package manager)
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer


# Copy existing application directory contents to the working directory
COPY . /var/www/digiattar/api/

RUN chown -R www-data:www-data /var/www/digiattar
RUN chmod -R 775 /var/www/digiattar/api/storage /var/www/digiattar/api/bootstrap/cache

COPY ./workers/conf.d /etc/supervisor/conf.d/
COPY /workers/supervisord.conf /etc/supervisord.conf
#RUN supervisord -n -c /etc/supervisord.conf

# Expose port 9000 and start php-fpm server (for FastCGI Process Manager)
EXPOSE 9000

CMD ["php-fpm"]
