<?php

namespace Modules\Transaction\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Gateway extends Model
{
	# New update, use `rebuild` api to change columns from deleted to deleted_at
	use SoftDeletes;
    use HasFactory;

    protected $primaryKey = 'id';

    protected $table = 'gateway';

    protected $keyType = 'string';

    protected $guarded = [];

    protected static function newFactory()
    {
        return \Modules\Transaction\Database\factories\GatewayFactory::new();
    }
}
