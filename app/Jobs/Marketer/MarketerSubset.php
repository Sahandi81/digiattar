<?php

namespace App\Jobs\Marketer;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;

class MarketerSubset implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    private $marketer_id;
    private $type;

    /**
     * MarketerSubset constructor.
     * @param $marketer_id
     * @param $type
     */
    public function __construct($marketer_id, $type)
    {
        $this->marketer_id = $marketer_id;
        $this->type = $type;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        DB::select('call sp_marketer_update_subset(?)', [$this->marketer_id]);
    }
}
