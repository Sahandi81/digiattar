<?php

namespace Modules\Marketer\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\FormRequest\Entities\FormRequest;
use Modules\Marketer\Entities\Marketer;
use Modules\User\Entities\Log;
use Modules\User\Entities\Role;
use Modules\User\Entities\User;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;


class MarketerController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $entities = Marketer::with(['info', 'user', 'parent'])
            ->where(function ($q) use($request) {

                if ($request->has('filter')) {

                    $filter = json_decode($request->get('filter'), true);

                    if (isset($filter['name'])) {
                        $q->whereHas('user', function ($user) use($request, $filter) {
                            $user->where('name', 'like', '%' . $filter['name'] . '%');
                        });
                    }

                    if (isset($filter['family'])) {
                        $q->whereHas('user', function ($user) use($request, $filter) {
                            $user->where('family', 'like', '%' . $filter['family'] . '%');
                        });

                    }

                    if (isset($filter['mobile'])) {
                        $q->whereHas('user', function ($user) use($request, $filter) {
                            $user->where('mobile', 'like', '%' . $filter['mobile'] . '%');
                        });

                    }


                    if (isset($filter['email'])) {
                        $q->whereHas('user', function ($user) use($request, $filter) {
                            $user->where('email', 'like', '%' . $filter['email'] . '%');
                        });
                    }

                    if (isset($filter['username'])) {
                        $q->whereHas('user', function ($user) use($request, $filter) {
                            $user->where('username', 'like', '%' . $filter['username'] . '%');
                        });
                    }

                    if (isset($filter['status']) && $filter['status'] != -1) {
                        $q->whereHas('user', function ($user) use($request, $filter) {
                            $user->where('status', $filter['status']);
                        });
                    } else {
                        $q->whereHas('user', function ($user) use($request, $filter) {
                            $user->where('status', 1);
                        });
                    }



                    if (isset($filter['national_code']) && $filter['national_code'] != '') {
                        $q->whereHas('info', function ($info) use($request, $filter) {
                            $info->where('national_code', 'like', '%' . $filter['national_code'] . '%');
                        });

                    }

                    if (isset($filter['parent_id']) && $filter['parent_id'] != '') {
                        $q->where('parent_id',  $filter['parent_id'] );
                    }

                    if (isset($filter['marketer_code']) && $filter['marketer_code'] != '') {
                        $q->where('marketer_code',  $filter['marketer_code'] );
                    }


                    if (isset($filter['hr'] ) && $filter['hr'] != '') {
                        $q->where('ancestry', 'like', DB::table('marketer')->where('id', $filter['hr'])->value('ancestry') . '%');
                    }


                    if (isset($filter['is_code']) && $filter['is_code'] != '-1') {
                        $q->whereHas('info', function ($info) use($request, $filter) {
                            if ($filter['is_code']) {
                                $info->whereNotNull('gov_code');
                            } else {
                                $info->whereNull('gov_code');
                            }
                        });

                    }



                    if (isset($filter['from_date']) && isset($filter['to_date']) && $filter['from_date'] != "" && $filter['to_date'] != "") {
                        $q->whereHas('user', function ($user) use($request, $filter) {
                            $user->where('created_at', '>=', $filter['from_date']);
                            $user->where('created_at', '<=', $filter['to_date']);
                        });
                    } elseif (isset($filter['from_date']) && $filter['from_date']) {
                        $q->whereHas('user', function ($user) use($request, $filter) {
                            $user->where('created_at', '>=', $filter['from_date']);
                        });


                    } elseif (isset($filter['to_date']) && $filter['to_date']) {
                        $q->whereHas('user', function ($user) use($request, $filter) {
                            $user->where('created_at', '<=', $filter['to_date']);
                        });

                    }

                }

            })
            ->orderBy($request->get('sort_field') ?? 'id', $request->get('sort_type') ?? 'desc');


        if ($request->has('excel_export') && $request->get('excel_export') == true) {
            $entities = $entities->get();
        } else {
            $entities = $entities->paginate($request->get('limit') ?? 100);
        }

        return response($entities);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'name' => 'required',
            'family' => 'required',
            'mobile' => 'required|unique:user|min:11|max:11',
            'username' => 'required|unique:user',
            'email' => 'unique:user|email',
            'password' => 'required|min:6',
        ]);

        if ($validator->fails()) {

            return Response()->json(['status' => false, 'msg' => $validator->errors()->first()]);
        }


        $request->merge(['password' => bcrypt($request->get('password'))]);

        $model = User::create($request->all());

        if ($model) {

            return Response()->json(['status' => true, 'msg' => 'عملیات موفقیت آمیز بود.']);
        }
        return Response()->json(['status' => false, 'msg' => 'خطایی رخ داده است.']);
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function show($id) {
        $entities = User::select(['id', 'name', 'mobile', 'email', 'created_at', 'role_id', 'status', 'username', 'family'])->with(['role' => function($q) {
            $q->select('id', 'title');
        }])->where('id', $id)->first();

        return response($entities);
    }

    /***
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id, Request $request) {
        $validator = \Validator::make($request->all(), [
            'name' 			=> 'required',
            'family' 		=> 'required',
            'role_id' 		=> 'required',
            'mobile' 		=> 'required',
            'username' 		=> 'required',
            'status' 		=> 'required'
        ]);

        if ($validator->fails()) {

            return Response()->json(['status' => false, 'msg' => $validator->errors()->first()]);
        }


        if ($request->get('mobile') && $request->get('mobile') != "") {
            $mobile = User::where('mobile', $request->get('mobile'))->where('id', '<>', $id)->count();
            if ($mobile > 0) {
                return Response()->json(['status' => false, 'msg' => 'موبایل قبلا تکرار شده است']);
            }
        }

        if ($request->get('email') && $request->get('email') != "") {
            $email = User::where('email', $request->get('email'))->where('id', '<>', $id)->count();
            if ($email > 0) {
                return Response()->json(['status' => false, 'msg' => 'ایمیل قبلا ثبت شده است.']);
            }
        }

        if ($request->get('username') && $request->get('username') != "") {
            $username = User::where('username', $request->get('username'))->where('id', '<>', $id)->count();
            if ($username > 0) {
                return Response()->json(['status' => false, 'msg' => 'نام کاربری قبلا تکرار شده است']);
            }
        }

        $model = User::where('id', $id)->update($request->all());

        if ($model) {

            return Response()->json(['status' => true, 'msg' => 'عملیات موفقیت آمیز بود.']);
        }
        return Response()->json(['status' => false, 'msg' => 'خطایی رخ داده است.']);
    }


    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword($id, Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return Response()->json(['status' => false, 'msg' => $validator->errors()->first()]);
        }

        $model = User::where('id', $id)->update([
            'password' => bcrypt($request->get('password'))
        ]);

        if ($model) {

            return Response()->json(['status' => true, 'msg' => 'عملیات موفقیت آمیز بود.']);
        }
        return Response()->json(['status' => false, 'msg' => 'خطایی رخ داده است.']);
    }


    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeStatus($id, Request $request)
    {

        $model = User::find($id);
        $model = $model->update([
            'status' => $model->status ? 0 : 1
        ]);

        DB::table('oauth_access_tokens')->where('user_id', $id)->delete();

        if ($model) {

            return Response()->json(['status' => true, 'msg' => 'عملیات موفقیت آمیز بود.']);
        }
        return Response()->json(['status' => false, 'msg' => 'خطایی رخ داده است.']);
    }

    public function getMarketerCode($id)
    {
        $result = Marketer::getMarketerCode($id);
        return response($result);
    }

//    public function marketerSalesDailyReport(Request $request)
//    {
//        try {
//            $response = \Illuminate\Support\Facades\DB::select('call sp_marketer_daily_report_with_period(?, ?, ?)', [
//                $request->get('marketer_id') != '' ? $request->get('marketer_id') :  Auth::id(),
//                $request->has('month_id') && $request->get('month_id') ? $request->get('month_id')  : 0,
//                $request->has('period_id') && $request->get('period_id') ? $request->get('period_id') : 0,
//            ]);
//            return response(['status' => true, 'data' => $response]);
//        } catch (\Exception $exception) {
//            if ($exception instanceof \Illuminate\Database\QueryException) {
//                return response()->json(['status' => false, 'msg' => $exception->getPrevious()->errorInfo[2]]);
//            } else {
//                return response()->json(['status' => false, 'msg' => $exception->getMessage()]);
//            }
//        }
//    }

    public function marketerMapReports(Request $request)
    {
        try {
            $response = \Illuminate\Support\Facades\DB::select('call sp_marketer_map_reports(?, ?, ?)', [
                $request->get('marketer_id') != '' ? $request->get('marketer_id') :  0,
                $request->has('month_id') && $request->get('month_id') ? $request->get('month_id')  : 0,
                $request->has('period_id') && $request->get('period_id') ? $request->get('period_id') : 0,
            ]);
            return response(['status' => true, 'data' => $response]);
        } catch (\Exception $exception) {
            if ($exception instanceof \Illuminate\Database\QueryException) {
                return response()->json(['status' => false, 'msg' => $exception->getPrevious()->errorInfo[2]]);
            } else {
                return response()->json(['status' => false, 'msg' => $exception->getMessage()]);
            }
        }
    }


    /*
     * post request
     * marketer revoke request
     *
     * this action call with admin panel
     */
    public function revoke($id, Request $request)
    {

        if ($request->get('content') == '' || !$request->get('content')) {
            return response(['status' => false, 'msg' => 'متن درخواست و یا بازگشت به مجموعه را وارد کنید']);
        }

        try {
            // change user status with is_login parameter
            $user = User::find($id);
            $user->status = $request->get('status') ?? 0;
            $user->save();

            if ( ! $request->get('status')) {
                DB::table('oauth_access_tokens')->where('user_id', $id)->delete();
            }


            // save request marketer from admin operator
            $revoke_request = FormRequest::updateOrCreate(['created_by' => $id, 'category_id' => 1, 'status' => 0], [
                'operator_id' 	=> Auth::id(),
                'created_by' 	=> $id,
                'category_id' 	=> 1, // revoke id in category table
                'content'	 	=> $request->get('content'),
            ]);

            if ($request->get('sending_data')) {
                $result = Marketer::revoke($id);

                if (is_array($result) && isset($result['status']) && !$result['status']) {
                    return response(['status' => false, 'msg' => $result['msg']]);
                }

                $revoke_request->status = 1;
                $revoke_request->message = is_array($result) && isset($result['msg']) ? $result['msg'] : 'نامشخص.عدم پاسخ وزارت';
                $revoke_request->save();

            } else {
                $revoke_request->status = 1;
                if ($request->get('status')) {
                    $revoke_request->message = 'جایگاه بدون ارسال اطلاعات به وزارت باز شد.';
                } else {
                    $revoke_request->message = 'جایگاه بدون ارسال اطلاعات به وزارت بسته شد.';
                }

                $revoke_request->save();
            }

            return response(['status' => true, 'msg' => 'عملیات موفقیت آمیز بود.']);

        } catch (\Exception $exception) {
            return response(['status' => false, 'msg' => $exception->getMessage()]);
        }
    }


    public function address($id)
    {
        $address = DB::select('call sp_address(?)', [$id]);
        return response()->json($address);
    }

    public function addAddress($id, Request $request)
    {

        try {

            $validator = \Validator::make($request->all(), [
                'reciver_name' => 'required',
                'region_id' => 'required',
                'postal_code' => 'required|min:10|max:10',
                'reciver_mobile' => 'required|min:11|max:11',
                'reciver_national_code' => 'required|min:10|max:10',
            ]);

            if ( $validator->fails() ) {
                return Response()->json(['status' => false, 'message' => $validator->errors()->first()]);
            }

            if (!$request->get('main')) {
                return Response()->json(['status' => false, 'message' => 'آدرس را وارد کنید']);
            }

            $address = DB::select('call sp_address_add(?, ?, ?, ?, ?, ?, ?, ?, ?)', [
                $id,
                last($request->get('region_id')),
                $request->has('lat') ? $request->get('lat') : null,
                $request->has('lng') ? $request->get('lng') : null,
                $request->get('main'),
                $request->get('postal_code'),
                $request->get('reciver_name'),
                $request->get('reciver_mobile'),
                $request->get('reciver_national_code'),
            ]);
            return response()->json(['status' => true, 'address' => $address]);
        } catch (\Exception $exception) {
            if ($exception instanceof QueryException) {
                return response()->json(['status' => false, 'msg' => $exception->getPrevious()->errorInfo[2]]);
            } else {
                return response()->json(['status' => false, 'msg' => $exception->getMessage()]);
            }

        }
    }

}
