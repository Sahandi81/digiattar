<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;

class FAQ extends Model
{
    use HasFactory, SoftDeletes;

	protected $table = 'faqs';

    protected $fillable = [
		'category_id',
		'question',
		'answer',
	];

	protected $hidden = [
		'category_id',
	];

}
