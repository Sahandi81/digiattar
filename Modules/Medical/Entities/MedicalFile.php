<?php

namespace Modules\Medical\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class MedicalFile extends Model
{
	# New update, use `rebuild` api to change columns from deleted to deleted_at
	use SoftDeletes;
    use HasFactory;

    protected $fillable = [];
    
}
