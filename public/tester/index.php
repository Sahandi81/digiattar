<?php

/**
 * @author www.softiran.org
 * @copyright 2020
 */

session_start();
if (isset($_POST['action']) && $_POST['action'] == "test3") 
{

    if (!isset($_POST['terminal']) ||  strlen($_POST['terminal']) < 3) 
	{
        echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>تست درگاه پرداخت الکترونیک سپهر </title>
    <style>
    .textbox{
        font:9pt Tahoma;
    }
    table tr td{
        font:9pt Tahoma;
    }
    </style>
</head>
<body style="background-color: #efefef;">
<center>    
    <form action="" method="POST">        
        <input type="hidden" class="textbox" name="action" id="action" value="test3"/>
        <div style="background-color: #bbe1ff;text-align: center;padding:10px;margin-top:10%;">
          <font color="red" style="font-family: Tahoma;font-size: 13px;">فرم زیر را تکمیل کنید </font>
        <table style="background-color: #fff;" align="center" dir="rtl">

            <tr>
			<td>شماره سفارش</td>
                <td style="text-align: right;">
					<input type="text" class="textbox" name="PayOrderId" id="PayOrderId" value="' . time() . '" readonly="readonly"/>
				</td>
                
            </tr>
            
			<tr>
			<td>Terminal</td>
                <td style="direction: rtl;text-align: right;">
					<input type="text" class="textbox" name="terminal" id="terminal" value="" style="text-align:left;" />
                </td>
            </tr>
            
            <tr>
                <td style="text-align: center;direction:rtl;" colspan="2">
                    <input type="submit" class="textbox" value="انجام" />
					
                </td>
               
            </tr>
        </table>
        </div>
    </form>
	<a href="http://www.softiran.org" target="_blank" title="softiran">www.softiran.org</a>
</center>


</body>
</html>';
        die;
    }

	$_SESSION['terminal'] = $terminal = trim($_POST['terminal']);
	$_SESSION['PayOrderId'] = $invoiceNumber = trim($_POST['PayOrderId']);
	$amount = 1000;
	
	if (isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1) ||isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] =='https') 
	{
		$protocol = 'https://';
	}
	else 
	{
		$protocol = 'http://';
	}
	
	$redirectAddress = $protocol . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/' . basename(__FILE__) . '?pay=ok';
	
	
	$params ='terminalID='.$terminal.'&Amount='.$amount.'&callbackURL='.urlencode($redirectAddress).'&invoiceID='.$invoiceNumber;
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, 'https://mabna.shaparak.ir:8081/V1/PeymentApi/GetToken');
	curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$res = curl_exec($ch);
	curl_close($ch);
	if($res)
	{
		$res = json_decode($res,true);
		if($res['Status'] == '0')
		{	  
			echo '<form id="paymentUTLfrm" action="https://mabna.shaparak.ir:8080" method="POST">
				<input type="hidden" id="TerminalID" name="TerminalID" value="'.$terminal.'">
				<input type="hidden" id="token" name="token" value="'.$res['Accesstoken'].'">
				<input type="hidden" id="getMethod" name="getMethod" value="1">
				</form><script>
				function submitmabna() {
				document.getElementById("paymentUTLfrm").submit();
				}
				window.onload=submitmabna; </script>';
		}else
		{
			echo getRes('خطا در ساخت توکن <br/> کد خطا :'.$res['Status']);
			die;
		}
	}
	else
	{
		echo getRes('پورت 8081 در هاست شما بسته است !');
		die;
	}
	
} 
elseif (!isset($_POST['action']) && !isset($_GET['pay'])) {
    $ip = '';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'http://ip-api.com/json/');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $ip = curl_exec($ch);
    curl_close($ch);
    $myIp = json_decode($ip, true);
    $ip = $myIp['query'];
    echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>تست درگاه پرداخت الکترونیک سپهر </title>
    <style>
    .textbox{
        font:9pt Tahoma;
    }
    table tr td{
        font:9pt Tahoma;
    }
    </style>
</head>
<body style="background-color: #efefef;">
<center>    
    <form action="" method="POST">        
        <input type="hidden" class="textbox" name="action" id="action" value="test3"/>
        <div style="background-color: #bbe1ff;text-align: center;padding:10px;margin-top:10%;">
        <font color="red" style="font-family: Tahoma;font-size: 13px;"></font>
        <table style="background-color: #fff;" align="center" dir="rtl">
            <tr>
			<td> آی پی هاست شما :</td>
			<td>' . $ip . '</td>
            </tr>
			<tr>
			<td><br/></td>
            </tr>
           
            
            <tr>
                <td style="text-align: center;direction:rtl;" colspan="2">
                    <input type="submit" class="textbox" value="رفتن به مرحله 1" />
					
                </td>
               
            </tr>
        </table>
        </div>
    </form>
	<a href="http://www.softiran.org" target="_blank" title="softiran">www.softiran.org</a>
</center>


</body>
</html>';
} elseif (!isset($_POST['action']) && isset($_GET['pay']) && $_GET['pay'] = 'ok') {
    $msg = '';
if ($_SERVER["REQUEST_METHOD"] != "POST") 
{
	$_POST +=$_GET;
}
    if( isset($_POST['respcode']) && $_POST['respcode'] == '0' )
	{
        
        $amount = 1000;
		
		$params ='digitalreceipt='.$_POST['digitalreceipt'].'&Tid='.$terminal;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://mabna.shaparak.ir:8081/V1/PeymentApi/Advice');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$res = curl_exec($ch);
		curl_close($ch);
		$result = json_decode($res,true);

		if (strtoupper($result['Status']) == 'OK') 
		 {
			if($result['ReturnId'] == $amount)
			{
				$msg = '<font color="green" ><b>پرداخت شما کامل شده است</b></font><br/>';
				$msg .= 'کد پیگیری : ' . $_POST['rrn'];
				$msg .= '<br/>اطلاعات تکمیلی : <br/>' . $ret;
				$msg .= '<br/><br/><a href="index.php" >برگشت</a>';
			}else
			{
				$resultCode = 'مبلغ واریز به قیمت محصول برابر نیست ، مبلغ واریزی :'.$result['ReturnId'];
				$msg = '<font color="red" ><b>پرداخت ناموفق</b></font><p style="direction:rtl"><br/>خطا در وریفای اطلاعات  <br/> علت خطا :<br/><b> '.$resultCode.'</b><br/>'.$ret;
				$msg .= '<br/><br/><p><a href="index.php" >برگشت</a>';
			}
			
		}else 
		{
			switch($result['ReturnId'])
			{
				case '-1' : $err = 'تراکنش پیدا نشد';break;
				case '-2' : $err = 'تراکنش قبلا Reverse شده است';break;
				case '-3' : $err = 'خطا عمومی';break;
				case '-4' : $err = 'امکان انجام درخواست برای این تراکنش وجود ندارد';break;
				case '-5' : $err = 'آدرس IP پذیرنده نامعتبر است';break;
				default  : $err = 'خطای ناشناس : '.$result['ReturnId'];break;
				
			}
					
			$msg = '<font color="red" ><b>پرداخت ناموفق</b></font><p style="direction:rtl"><br/>خطا در وریفای اطلاعات  <br/> علت خطا :<br/><b> '.$err.'</b><br/>'.$ret;
			$msg .= '<br/><br/><p><a href="index.php" >برگشت</a>';
		}
    } else 
	{
		
        $msg = "پرداخت موفق آمیز نبود ، خطای 200";
		$msg .= '<br/>اطلاعات تکمیلی : <br/>' . $ret;
		$msg .= '<br/><br/><a href="index.php" >برگشت</a>';
    }
    echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>نتیجه تراکنش</title>
    <style>
    .textbox{
        font:9pt Tahoma;
    }
    table tr td{
        font:9pt Tahoma;
    }
    </style>
</head>
<body style="background-color: #efefef;">
<center>    

        <div style="background-color: #bbe1ff;text-align: center;padding:10px;margin-top:10%;">
        <font color="red" style="font-family: Tahoma;font-size: 13px;"></font>
        <table style="background-color: #fff;" align="center">

            <tr>
                <td style="text-align: center;" colspan="2">
                   <?php echo $res ; ?>
                </td>
            </tr> 
			<tr>
                <td style="text-align: center;" colspan="2">
                   ' . $msg . '
                </td>
            </tr> 
           
        </table>
        </div>
<a href="http://www.softiran.org" target="_blank" title="softiran">www.softiran.org</a>
</center>

</body>
</html>';
}

function getRes($msg)
{
	return '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
			<head>
				<meta http-equiv="content-type" content="text/html; charset=utf-8" />
				<title>نتیجه</title>
				<style>
				.textbox{
					font:9pt Tahoma;
				}
				table tr td{
					font:9pt Tahoma;
				}
				</style>
			</head>
			<body style="background-color: #efefef;">
			<center>    

					<div style="background-color: #bbe1ff;text-align: center;padding:10px;margin-top:10%;">
					<font color="red" style="font-family: Tahoma;font-size: 13px;"></font>
					<table style="background-color: #fff;" align="center">
						<tr>
							<td style="text-align: center;" colspan="2">
							'.$msg.'
							</td>
						</tr> 
					   
					</table>
					</div>
			<a href="http://www.softiran.org" target="_blank" title="softiran">www.softiran.org</a>
			</center>

			</body>
			</html>';
}
?>