<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSpStoreReminderSms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::select("
        	CREATE DEFINER=CURRENT_USER PROCEDURE `sp_store_reminder_sms`(IN p_id INT(11), IN p_title VARCHAR(50))
			BEGIN
				
			  DECLARE done INT DEFAULT FALSE;
			  DECLARE u_id CHAR(16);
			  DECLARE users CURSOR FOR select user_id FROM notices where product_id = p_id;
			  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
			
				
			  limited_loop: LOOP
				FETCH users INTO u_id;
				IF done THEN
				  LEAVE limited_loop;
				END IF;
				INSERT INTO notification(notification.user_id,notification.template,notification.token4)
							VALUES (u_id,'DGInventoryReminder',p_title);
			  END LOOP;
			
				
			  CLOSE users;
			
			END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('', function (Blueprint $table) {

        });
    }
}
