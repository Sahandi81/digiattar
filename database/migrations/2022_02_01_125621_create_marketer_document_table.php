<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarketerDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketer_document', function (Blueprint $table) {
            $table->integer('marketer_id');
            $table->enum('type', ['pic', 'national_card', 'birth_certificate'])->default('pic');
            $table->string('old_file', 50)->nullable();
            $table->string('new_file', 50)->nullable();
            $table->tinyInteger('status')->default(0);
            $table->dateTime('created_at')->nullable();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->softDeletes();

            $table->primary(['marketer_id', 'type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketer_document');
    }
}
