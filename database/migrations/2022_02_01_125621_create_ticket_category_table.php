<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->unsignedInteger('_lft')->default('0')->index('_lft');
            $table->unsignedInteger('_rgt')->default('0')->index('_rgt');
            $table->unsignedInteger('parent_id')->nullable()->index('parent_id');
            $table->boolean('status')->default(true)->index('status');
            $table->text('content')->nullable();
            $table->mediumText('faq')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_category');
    }
}
