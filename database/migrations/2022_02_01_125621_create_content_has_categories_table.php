<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentHasCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_has_categories', function (Blueprint $table) {
            $table->bigInteger('content_id');
            $table->bigInteger('category_id');
            $table->tinyInteger('is_main')->default(0);
            $table->softDeletes();

            $table->primary(['content_id', 'category_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_has_categories');
    }
}
