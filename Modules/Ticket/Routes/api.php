<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('backend')->group(function () {

    Route::prefix('tickets')->middleware(['auth:api', 'admin_access'])->group(function () {

        Route::prefix('categories')->group(function () {
            Route::get('/', 'CategoryController@index')->middleware('access');
            Route::post('/', 'CategoryController@store')->middleware('access');
            Route::get('/{id}', 'CategoryController@show')->middleware('access');
            Route::put('/{id}', 'CategoryController@update')->middleware('access');
            Route::get('/{id}/findMapState', 'CategoryController@findMapState')->middleware('access');
            Route::put('/{id}/dispatchMove', 'CategoryController@dispachMove')->middleware('access');
            Route::post('/{id}/document', 'CategoryController@document');
        });


        Route::get('/', 'TicketController@index')->name('ticket_index');
        Route::get('/operator/report', 'TicketController@operatorReport');
        Route::post('/', 'TicketController@store')->middleware('access');
        Route::get('/{id}/conversations', 'TicketController@conversations');
        Route::post('/{id}/conversations', 'TicketController@storeConversations');
        Route::delete('/{ticket}/conversations/{id}', 'TicketController@deleteConversation');
        Route::put('/{id}', 'TicketController@update');

    });
});
