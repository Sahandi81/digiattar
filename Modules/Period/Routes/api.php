<?php

use Illuminate\Support\Facades\Route;
use Modules\Period\Entities\Month;
use Modules\Period\Http\Controllers\PeriodController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('backend')->group(function () {

    Route::prefix('period')->middleware(['auth:api', 'admin_access'])->group(function () {

        Route::get('/', function () {
            return response(
				Month::with(['period'])
				->where('hidden', 0)
				->orderBy('id', 'desc')
				->get()
			);
        });

        Route::get('/list', 					[PeriodController::class, 'index']);
        Route::get('/{period}/export', 			[PeriodController::class,'export']);
        Route::get('/{period}/qualified', 		[PeriodController::class,'qualified']);
        Route::get('/sales-daily-report', 		[PeriodController::class, 'salesDailyReport']);
        Route::get('/mapReport', 				[PeriodController::class,'mapReport']);
    });
});
