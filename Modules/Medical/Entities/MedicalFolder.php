<?php

namespace Modules\Medical\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class MedicalFolder extends Model
{
	# New update, use `rebuild` api to change columns from deleted to deleted_at
	use SoftDeletes;
    use HasFactory;

    protected $fillable = [];

	protected $hidden = [
		'deleted_at',
		'user_id',
		'record_id',
	];

	public function forms(): \Illuminate\Database\Eloquent\Relations\HasMany
	{
		return $this->hasMany(VisitForm::class, 'folder_id', 'id');
	}

}
