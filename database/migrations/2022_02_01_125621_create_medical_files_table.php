<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicalFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_files', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('file_src', 500)->default('');
            $table->dateTime('created_at');
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->integer('user_id')->default(0);
            $table->integer('record_id')->default(0);
            $table->integer('folder_id')->default(0);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medical_files');
    }
}
