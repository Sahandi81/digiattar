<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSpLikeSystemProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
			DB::select(
				"CREATE DEFINER= CURRENT_USER PROCEDURE `sp_like_system`(IN u_id INT ,IN product_id INT(11) )
				BEGIN
				 DECLARE msg VARCHAR(50);
				 
				IF  ((SELECT COUNT(*) FROM likes WHERE user_id = u_id AND parent_id = product_id) > 0) THEN
						SET msg = 'DUPLICATE_LIKE';
				ELSE
				INSERT INTO `likes` (user_id, parent_id) VALUES (u_id, product_id);
				SET msg = 'SUCCESS';
				END IF;
				
				SELECT msg;
			END"
			);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('', function (Blueprint $table) {

        });
    }
}
