<?php

namespace Modules\Temperament\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class TemperamentAnswers extends Model
{
	# New update, use `rebuild` api to change columns from deleted to deleted_at
	use SoftDeletes;
    use HasFactory;

    protected $fillable = [];
    protected $table = "determine_temperament_answer";

    protected static function newFactory()
    {
        return \Modules\Temperament\Database\factories\TemperamentAnswersFactory::new();
    }
}
