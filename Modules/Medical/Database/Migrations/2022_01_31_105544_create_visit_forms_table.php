<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisitFormsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('visit_forms', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->integer('user_id');
			$table->string('form_question_id')->index();
			$table->integer('record_id');
			$table->string('answer');
			$table->timestamp('created_at')->nullable();
			$table->timestamp('updated_at')->useCurrentOnUpdate()->nullable();

			$table->unique(['form_question_id', 'user_id', 'record_id'], 'unique_index');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('visit_forms');
	}
}
