<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSpReminder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	\Illuminate\Support\Facades\DB::select("
    	CREATE DEFINER=CURRENT_USER PROCEDURE `sp_reminder`(IN u_id INT ,IN p_id INT(11) )
			BEGIN
				 DECLARE msg VARCHAR(50);
				 
				IF  ((SELECT COUNT(*) FROM notices WHERE user_id = u_id AND product_id = p_id) > 0) THEN
						SET msg = 'DUPLICATE_LIKE';
				ELSE
				INSERT INTO notices (user_id, product_id) VALUES (u_id, p_id);
				SET msg = 'SUCCESS';
				END IF;
				
				SELECT msg;
			END
    	");
	}

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('', function (Blueprint $table) {

        });
    }
}
