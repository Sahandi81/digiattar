<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToRoleHasPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('role_has_permissions', function (Blueprint $table) {
            $table->foreign(['role_id'], 'FK_role_has_permission_role')->references(['id'])->on('role')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign(['permission_id'], 'FK_role_has_permissions_permission')->references(['id'])->on('permission')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('role_has_permissions', function (Blueprint $table) {
            $table->dropForeign('FK_role_has_permission_role');
            $table->dropForeign('FK_role_has_permissions_permission');
        });
    }
}
