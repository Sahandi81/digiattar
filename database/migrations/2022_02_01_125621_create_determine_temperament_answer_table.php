<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetermineTemperamentAnswerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('determine_temperament_answer', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('question_id')->default(0)->index('question_id');
            $table->string('answer_text', 500)->default('0');
            $table->integer('temperament_attribute_id')->nullable()->index('temperament_attribute_id');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('determine_temperament_answer');
    }
}
