<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Marketer\Entities\Marketer;

class MarketerSubset extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'MarketerSubset:command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $all = DB::table('marketer_fire_subset')->get();

        foreach ($all as $entity) {

            $ancestry = Marketer::where('id', $entity->marketer_id)->value('ancestry');
            $marketer_queue = array_reverse(explode('/', trim($ancestry, '/')));

            foreach ($marketer_queue as $queue) {

                dispatch(new \App\Jobs\Marketer\MarketerSubset($queue, $entity->type))->onQueue('subset');
            }

        }

        DB::table('marketer_fire_subset')->delete();
    }
}
