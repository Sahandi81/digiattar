<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductPinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_pins', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('product_types_id')->index('product_types_id');
            $table->integer('operator_id')->nullable()->index('operator_id');
            $table->integer('order_id')->nullable()->index('order_id');
            $table->unsignedInteger('inc_count')->default('0');
            $table->unsignedInteger('dec_count')->default('0');
            $table->integer('total')->nullable();
            $table->dateTime('depot_date')->nullable();
            $table->string('document_number', 100)->nullable();
            $table->dateTime('created_at')->nullable();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->softDeletes();

            $table->unique(['product_types_id', 'order_id', 'inc_count', 'dec_count'], 'product_types_id_order_id_inc_count_dec_count');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_pins');
    }
}
