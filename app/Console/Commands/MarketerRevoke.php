<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Marketer\Entities\Marketer;
use Modules\User\Entities\User;

class MarketerRevoke extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'MarketerRevoke:command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = DB::table('marketer as m')
            ->leftJoin('user as u', 'u.id', '=', 'm.id')
            ->where('u.status', 0)
            ->where('m.subset', '<',  1)
            ->get();

        foreach ($users as $user)
        {
            dispatch(new \App\Jobs\Marketer\MarketerRevoke($user->id))->onQueue('revoke');
        }
    }
}
