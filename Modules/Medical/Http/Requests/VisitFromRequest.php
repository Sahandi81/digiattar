<?php

namespace Modules\Medical\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VisitFromRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
	{
        return [
            'folder_id'					=> 'required|numeric|exists:medical_records,id',
            'answers.*.form_question_id'=> 'required|numeric|exists:form_questions,id',
            'answers.*.answer'			=> 'required|string',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
