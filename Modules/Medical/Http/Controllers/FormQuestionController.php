<?php

namespace Modules\Medical\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Response;
use Modules\Medical\Entities\FormQuestion;

class FormQuestionController extends Controller
{

	public function index(): JsonResponse
	{
		$questions = FormQuestion::with(['category'])
			->get(['question_text', 'id', 'category_id'])
			->groupBy('category.name')
		;
		return Response::json($questions);

	}

}
