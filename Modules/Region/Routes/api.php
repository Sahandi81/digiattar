<?php

use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('region')->middleware(['auth:api', 'admin_access'])->group(function () {

    Route::get('/', function () {

        if (!\Cache::tags(['region'])->has("region")) {

            $regions = \Modules\Region\Entities\Region::get()->toTree();

            \Cache::tags(['region'])->put("region", $regions, 24 * 60);
        }

        return response(\Cache::tags(['region'])->get('region'));
    });
});

Route::prefix('backend')->group(function () {

    Route::prefix('region')->middleware(['auth:api', 'admin_access'])->group(function () {

        Route::group(['prefix' => '/'], function () {
            Route::get('/', 'RegionController@index');
            Route::post('/', 'RegionController@store');
            Route::get('/{id}', 'RegionController@show');
            Route::put('/{id}', 'RegionController@update');
            Route::get('/{id}/findMapState', 'RegionController@findMapState');
            Route::put('/{id}/dispatchMove', 'RegionController@dispachMove');
            Route::put('/{id}/movement', function ($id, Request $request) {

                switch ($request->get('type')) {
                    case 'up':
                        foreach (explode(',', $id) as $item) {
                            \Modules\Region\Entities\Region::find($item)->up();
                        }
                        break;
                    case 'down':
                        foreach (explode(',', $id) as $item) {
                            \Modules\Region\Entities\Region::find($item)->down();
                        }
                        break;
                }

                return response(['status' => true]);
            });
        });
    });
});
