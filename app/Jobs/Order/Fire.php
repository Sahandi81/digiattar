<?php

namespace App\Jobs\Order;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Modules\Order\Entities\OrderQueueJob;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class Fire implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;



    private $orderQueueJob;

    /**
     * Fire constructor.
     * @param OrderQueueJob $orderQueueJob
     */
    public function __construct(OrderQueueJob $orderQueueJob)
    {
        $this->orderQueueJob = $orderQueueJob;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            DB::select('call sp_order_queue_job(?)', [$this->orderQueueJob->id]);
        } catch (\Exception $exception) {
            $logger = new Logger('orderQueuedJob');
            $logger->pushHandler(new StreamHandler(storage_path().'/logs/orderQueuedJob.log'));
            $logger->error($exception->getMessage() . ': with OrderQueueJob ' . $this->orderQueueJob->id);
        }
    }
}
