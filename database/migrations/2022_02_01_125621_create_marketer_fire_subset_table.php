<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarketerFireSubsetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketer_fire_subset', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('marketer_id')->index('marketer_id');
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->boolean('type')->default(true);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketer_fire_subset');
    }
}
