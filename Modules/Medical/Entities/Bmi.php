<?php

namespace Modules\Medical\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class bmi extends Model
{
	# New update, use `rebuild` api to change columns from deleted to deleted_at
	use SoftDeletes;
    use HasFactory;

    protected $table = 'bmi';

    protected $fillable = [
        'user_id',
        'weight',
        'height',
        'bmi',
        'record_id',
    ];


}
