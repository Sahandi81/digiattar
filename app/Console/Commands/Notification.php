<?php

namespace App\Console\Commands;

use App\Jobs\Notification\Fire;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\User\Entities\User;

class Notification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notification:command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send notification to queue';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $entities = \Modules\Notification\Entities\Notification::where('status', 0)->whereNull('error_log')->orderBy('level')->get();
        foreach ($entities as $entity) {
//            dispatch(new Fire($entity))->onQueue($entity->level == 1 ? 'nh' : ($entity->level == 2 ? 'nm' : 'nl'));
            $user = User::find($entity->user_id);

            if (!$user->mobile || strlen($user->mobile) > 11 || $user->mobile == '11111111111' || $user->mobile == '00000000000' || $user->mobile == '99999999999') {
                $entity->update([
                    'status' => 2,
                    'error_log' => 'موبایل نامعتبر است'
                ]);
            } else {
                try {
                    if (env('KAVENEGAR_API_KEY')) {
                        $api_key = env('KAVENEGAR_API_KEY');
                    } else {
                        $api_key = '655444654B5A624B4458774C3773714C783875357133555348654C3442353176';
                    }
                    $url = "https://api.kavenegar.com/v1/".$api_key."/verify/lookup.json?receptor=".$user->mobile."&token=".urlencode($entity->token1)."&template=".$entity->template."";
                    if($entity->token2){$url = $url."&token2=".urlencode($entity->token2);}
                    if($entity->token3){$url = $url."&token3=".urlencode($entity->token3);}
                    if($entity->token4){$url = $url."&token10=".urlencode($entity->token4);}
                    if($entity->token5){$url = $url."&token20=".urlencode($entity->token5);}
                    $result = file_get_contents($url);
//                    $api = new \Kavenegar\KavenegarApi($api_key);
//                    $sender = "100080040";
//                    $receptor = $user->mobile;
//                    $sender = "1000596446";
//                    $message = $entity->text;
//                    $result = $api->Send($sender, $receptor, $message);
                    if ($result) {
                        $entity->update([
                                'status' => 4
                            ]);
                        var_dump($result);
                    }
                } catch (\Kavenegar\Exceptions\ApiException $e) {
                    echo $e->errorMessage();
                    $entity->update([
                            'status' => 2,
                            'error_log' => $e->errorMessage()
                        ]);
                } catch (\Kavenegar\Exceptions\HttpException $e) {
                    echo $e->errorMessage();
                    $entity->update([
                            'status' => 2,
                            'error_log' => $e->errorMessage()
                        ]);
                }
            }
        }
    }
}
