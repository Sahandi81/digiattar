<?php

namespace Modules\Medical\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\User\Entities\User;

class VisitForm extends Model
{
    use HasFactory;

    protected $fillable = [
		'user_id',
		'form_question_id',
		'record_id',
		'answer',
	];


	public function user(): \Illuminate\Database\Eloquent\Relations\HasOne
	{
		return $this->hasOne(User::class, 'id', 'user_id');
	}

	public function question(): \Illuminate\Database\Eloquent\Relations\HasOne
	{
		return $this->hasOne(FormQuestion::class);
	}
    
}
