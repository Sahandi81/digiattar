<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('user_id');
            $table->integer('period_id');
            $table->integer('status')->default(0)->comment('1 => درحال پیگیری, 2 => حمل و نقل, 3 => تحویل, 4 => مرجوعی');
            $table->integer('address_id')->index('address_id');
            $table->integer('price')->default(0);
            $table->integer('off_price')->default(0);
            $table->integer('retail_reward')->default(0);
            $table->integer('post_cost')->default(0);
            $table->integer('discount')->default(0)->comment('discount from code');
            $table->integer('tax')->default(0);
            $table->integer('total_pay')->default(0)->comment('pure_price + post_cost - discount + tax');
            $table->float('weight', 10, 0)->default(0);
            $table->boolean('transport')->default(false)->index('transport');
            $table->boolean('delivery')->default(false)->index('delivery');
            $table->boolean('returned')->default(false);
            $table->boolean('private')->default(false);
            $table->boolean('paid')->default(false)->index('paid');
            $table->boolean('is_voucher')->default(false)->index('is_voucher');
            $table->boolean('is_wallet')->default(false)->index('is_wallet');
            $table->dateTime('created_at')->nullable();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->string('token', 50);
            $table->softDeletes();

            $table->index(['user_id', 'period_id', 'status', 'returned', 'private'], 'user_id_period_id_status_returned_private');
            $table->index(['user_id', 'period_id', 'status'], 'user_id_period_id_status');
            $table->index(['period_id', 'status'], 'period_id_status');
            $table->index(['user_id', 'status'], 'user_id_status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
