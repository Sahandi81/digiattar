<?php

namespace Modules\Transaction\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Order\Entities\Order;
use Modules\Region\Entities\Region;
use Modules\Transaction\Entities\Transaction;

class TransactionController extends Controller
{


    public function index(Request $request)
    {


        $entities = Transaction::with(['user'])->where(function ($q) use($request) {

            if ($request->get('gateway') && $request->get('gateway') != "") {
                $q->where('gateway', $request->get('gateway'));
            }

            if ($request->get('id') && $request->get('id') != "") {
                $q->where('id', $request->get('id'));
            }

            if ($request->get('order_id') && $request->get('order_id') != "") {
                $q->where('order_id', $request->get('order_id'));
            }

            if ($request->get('user_id') && $request->get('user_id') != "") {
                $q->where('user_id', $request->get('user_id'));
            }

            if ($request->get('status') && $request->get('status') != -1) {
                $q->where('status', $request->get('status'));
            }

            if ($request->get('mobile') && $request->get('mobile') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where('mobile', 'like', '%' . $request->get('mobile') . '%');
                });
            }

            if ($request->get('email') && $request->get('email') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where('email', 'like', '%' . $request->get('email') . '%');
                });
            }

            if ($request->get('username') && $request->get('username') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where('username', 'like', '%' . $request->get('username') . '%');
                });
            }

            if ($request->get('name') && $request->get('name') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where(DB::raw('concat(name, " ", family)'), 'like', '%' . $request->get('name') . '%');
                });
            }

            if ($request->get('national_code') && $request->get('national_code') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->whereHas('info', function ($info) use($request) {
                        $info->where('national_code', 'like', '%' . $request->get('national_code') . '%');
                    });
                });
            }

            if ($request->get('parent_id') && $request->get('parent_id') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->whereHas('marketer', function ($marketer) use($request) {
                        $marketer->where('ancestry', 'like', DB::table('marketer')->where('id', $request->get('parent_id'))->value('ancestry') . '%');
                    });
                });
            }

            if ($request->get('from_date') && $request->get('to_date') && $request->get('from_date') != "" && $request->get('to_date') != "") {
                $q->where('created_at', '>=', $request->get('from_date'));
                $q->where('created_at', '<=', $request->get('to_date'));
            } elseif ($request->get('from_date')) {
                $q->where('created_at', '>=', $request->get('from_date'));
            } elseif ($request->get('to_date')) {
                $q->where('created_at', '<=', $request->get('to_date'));
            }

        })->orderBy($request->get('sort_field') ?? 'id', $request->get('sort_type') ?? 'desc');

        if (($request->has('excel_export') && $request->get('excel_export') == true) || ($request->has('print') && $request->get('print') == true) || ($request->has('cumulative') && $request->get('cumulative') == true)) {
            $entities = $entities->get();
        } else {
            $entities = $entities->paginate($request->get('limit') ?? 100);
        }

        return response($entities);

    }


    public function show($id)
    {

        $transaction = Transaction::with(['user'])->find($id);

        return response($transaction);
    }
}
