<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDomainConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domain_config', function (Blueprint $table) {
            $table->string('domain_key', 50)->primary();
            $table->boolean('android')->default(false);
            $table->boolean('ios')->default(false);
            $table->boolean('maintenance_mode')->default(false);
            $table->boolean('register')->default(true);
            $table->boolean('basket')->default(true);
            $table->boolean('user_dashboard')->default(true);
            $table->boolean('admin_panel')->default(true);
            $table->boolean('notify_order')->default(true);
            $table->boolean('notify_ticket')->default(true);
            $table->boolean('notify_register')->default(true);
            $table->boolean('chat')->default(true);
            $table->dateTime('created_at')->nullable();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domain_config');
    }
}
