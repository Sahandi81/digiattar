<?php

namespace Modules\Contact\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Contact\Entities\Contact;

class ContactController extends Controller
{

    public function index(Request $request)
    {

        $entities = Contact::where(function ($q) use($request) {

            if ($request->get('status') != -1) {
                $q->where('status', $request->get('status'));
            }

            if ($request->get('name') && $request->get('name') != '') {
                $q->where('name', 'like', '%' . $request->get('name') . '%');
            }

            if ($request->get('mobile') && $request->get('mobile') != '') {
                $q->where('mobile', 'like', '%' . $request->get('mobile') . '%');
            }

            if ($request->get('email') && $request->get('email') != '') {
                $q->where('email', 'like', '%' . $request->get('email') . '%');
            }

            if ($request->get('from_date') && $request->get('to_date') && $request->get('from_date') != "" && $request->get('to_date') != "") {
                $q->where('created_at', '>=', $request->get('from_date'));
                $q->where('created_at', '<=', $request->get('to_date'));
            } elseif ($request->get('from_date')) {
                $q->where('created_at', '>=', $request->get('from_date'));
            } elseif ($request->get('to_date')) {
                $q->where('created_at', '<=', $request->get('to_date'));
            }

        })->orderBy($request->get('sort_field') ?? 'id', $request->get('sort_type') ?? 'desc');

        if (($request->has('excel_export') && $request->get('excel_export') == true)) {
            $entities = $entities->get();
        } else {
            $entities = $entities->paginate($request->get('limit') ?? 100);
        }

        return response($entities);
    }


    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeStatus($id, Request $request)
    {

        $model = Contact::find($id);

        if ($request->get('field') == 'status') {

            $model = $model->update([
                'status' => $model->status ? 0 : 1
            ]);
        }

        if ($model) {

            return Response()->json(['status' => true, 'msg' => 'عملیات موفقیت آمیز بود.']);
        }
        return Response()->json(['status' => false, 'msg' => 'خطایی رخ داده است.']);
    }
}
