<?php

namespace Modules\Temperament\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class DetermineTemperament extends Model
{
	# New update, use `rebuild` api to change columns from deleted to deleted_at
	use SoftDeletes;
    use HasFactory;

    protected $fillable = [
        'temperament_id_answer_1',
        'temperament_id_answer_2',
        'temperament_id_answer_3',
        'temperament_id_answer_4',
        'temperament_id_answer_5',
        'temperament_id_answer_6',
        'temperament_id_answer_7',
        'temperament_id_answer_8',
        'temperament_id_answer_9',
        'temperament_id_answer_10',
        'temperament_id_answer_11',
        'temperament_id_answer_12',
        'temperament_id_answer_13',
        'temperament_id_answer_14',
        'temperament_id_answer_15',
        'temperament_id_answer_16',
        'user_id',
        'percent_cold',
        'percent_hot',
        'percent_dry',
        'percent_more',
        'record_id'
    ];
    protected $table ='determine_temperaments';

}
