<?php

function rewardReason($key)
{
    $list = [
        "1" => "عدم خرید ماهانه",
        "2" => "نداشتن یا عدم درج شماره شبا",
        "3" => "نداشتن کد بازاریابی",
        "4" => "عدم ثبت قرارداد پشتیبانی",
    ];

    if ($key) {
        return $list[$key];
    }

    return $list;
}

function _custom_check_national_code($code)
{
    if(!preg_match('/^[0-9]{10}$/',$code))
        return false;
    for($i=0;$i<10;$i++)
        if(preg_match('/^'.$i.'{10}$/',$code))
            return false;
    for($i=0,$sum=0;$i<9;$i++)
        $sum+=((10-$i)*intval(substr($code, $i,1)));
    $ret=$sum%11;
    $parity=intval(substr($code, 9,1));
    if(($ret<2 && $ret==$parity) || ($ret>=2 && $ret==11-$parity))
        return true;
    return false;
}

function convert2english($string) {
    $newNumbers = range(0, 9);
    // 1. Persian HTML decimal
    $persianDecimal = array('&#1776;', '&#1777;', '&#1778;', '&#1779;', '&#1780;', '&#1781;', '&#1782;', '&#1783;', '&#1784;', '&#1785;');
    // 2. Arabic HTML decimal
    $arabicDecimal = array('&#1632;', '&#1633;', '&#1634;', '&#1635;', '&#1636;', '&#1637;', '&#1638;', '&#1639;', '&#1640;', '&#1641;');
    // 3. Arabic Numeric
    $arabic = array('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩');
    // 4. Persian Numeric
    $persian = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹');

    $string =  str_replace($persianDecimal, $newNumbers, $string);
    $string =  str_replace($arabicDecimal, $newNumbers, $string);
    $string =  str_replace($arabic, $newNumbers, $string);
    return str_replace($persian, $newNumbers, $string);
}

function blogSortItems($key = null) {
    $list = [
        ['title' => 'جدیدترین', 'field' => 'id', 'type' => 'desc'],
        ['title' => 'قدیمی', 'field' => 'id', 'type' => 'asc'],
        ['title' => 'محبوبترین', 'field' => 'visitor', 'type' => 'asc'],
        ['title' => 'عنوان', 'field' => 'title', 'type' => 'asc'],
    ];

    if ($key) {
        return $list[$key];
    }

    return $list;
}

function productSortItems($key = null) {
    $list = [
        ['title' => 'جدیدترین',         'field' => 'id',                'type' => 'desc'],
        ['title' => 'گرانترین',         'field' => 'price',             'type' => 'desc'],
        ['title' => 'ارزانترین',        'field' => 'price',             'type' => 'asc'],
        ['title' => 'بیشترین تخفیف',    'field' => 'discount',          'type' => 'desc'],
        ['title' => 'پرفروش ترین',      'field' => 'sales_number',      'type' => 'desc'],
        ['title' => 'حروف الفبا',       'field' => 'title',             'type' => 'asc'],
//        ['title' => 'محبوبترین',      'field' => 'visitor', 'type' => 'asc'],
    ];

    if ($key) {
        return $list[$key];
    }

    return $list;
}

function payloadRecoverySMS($mobile) {

    $rand = random_code();
    try {
        $verify = new \MahdiMajidzadeh\Kavenegar\KavenegarVerify();
        $res = $verify->lookup($mobile, 'PasswordRecovery', $rand, null, null, 'sms');
        if (@$res[0]->status) {
            return $rand;
        } else {
            return false;
        }
    } catch (Exception $exception) {
        return false;
    }

}


function quickRandom($length = 16)
{
    $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    return substr(str_shuffle(str_repeat($pool, 5)), 0, $length);
}


function imploadValue($types){
    $strTypes = implode(",", $types);
    return $strTypes;
}

function explodeValue($types){
    $strTypes = explode(",", $types);
    return $strTypes;
}

function random_code() {

    return rand(11111, 99999);
}

function remove_special_char($text) {

    $t = $text;

    $specChars = array(
        ' ' => '-',    '!' => '',    '"' => '',
        '#' => '',    '$' => '',    '%' => '',
        '&amp;' => '',    '\'' => '',   '(' => '',
        ')' => '',    '*' => '',    '+' => '',
        ',' => '',    '₹' => '',    '.' => '',
        '/-' => '',    ':' => '',    ';' => '',
        '<' => '',    '=' => '',    '>' => '',
        '?' => '',    '@' => '',    '[' => '',
        '\\' => '',   ']' => '',    '^' => '',
        '_' => '',    '`' => '',    '{' => '',
        '|' => '',    '}' => '',    '~' => '',
        '-----' => '-',    '----' => '-',    '---' => '-',
        '/' => '',    '--' => '-',   '/_' => '-',

    );

    foreach ($specChars as $k => $v) {
        $t = str_replace($k, $v, $t);
    }

    return mb_strtolower($t, 'UTF-8');
}


?>
