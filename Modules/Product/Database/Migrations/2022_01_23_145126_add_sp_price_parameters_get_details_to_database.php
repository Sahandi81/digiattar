<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSpPriceParametersGetDetailsToDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		\Illuminate\Support\Facades\DB::select(
			'CREATE DEFINER = CURRENT_USER PROCEDURE `sp_price_parameters_get_details`(IN `product_id` INT)
			BEGIN
				select distinct(pp.id) as id,json_arrayagg(pppp.price_parameter_id) as price_parameters,
				 `pp`.`price`,`pp`.`discount`, `pp`.`count`, `pp`.`off_price`, `pp`.`package_count` from `product_types` as `pp`
				  left join `product_types_has_price_parameter` as `pppp` on `pppp`.`product_types_id` = `pp`.`id` left join `product` as `p` on `p`.`id` = `pp`.`product_id`
				 where `pp`.`status` = 1 and `p`.`id` = `product_id` and `pp`.`count` > 0 group by `pp`.`id` order by `price` asc;
			END'
		);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('', function (Blueprint $table) {
			# if u want to drop procedure, drop it manually :)
        });
    }
}
