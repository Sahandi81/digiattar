<?php

namespace Modules\Finanical\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Finanical\Entities\Finance;

class FinanceController extends Controller
{

    public function index(Request $request)
    {

        $entities = Finance::with(['user' => function($user) {
            $user->with(['info', 'marketer']);
        }, 'period' => function($p) {
            $p->with(['month']);
        }])->where(function ($q) use($request) {

            if ($request->get('month_id') && $request->get('month_id') != "") {
                $q->whereHas('period', function ($period) use($request) {
                    $period->where('parent_id', $request->get('month_id'));
                });
            }

            if ($request->get('period_id') && $request->get('period_id') != "") {
                $q->where('period_id', $request->get('period_id'));
            }

            if ($request->get('user_id') && $request->get('user_id') != "") {
                $q->where('user_id', $request->get('user_id'));
            }

            if ($request->get('mobile') && $request->get('mobile') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where('mobile', 'like', '%' . $request->get('mobile') . '%');
                });
            }

            if ($request->get('email') && $request->get('email') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where('email', 'like', '%' . $request->get('email') . '%');
                });
            }

            if ($request->get('username') && $request->get('username') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where('username', 'like', '%' . $request->get('username') . '%');
                });
            }

            if ($request->get('name') && $request->get('name') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where(DB::raw('concat(name, " ", family)'), 'like', '%' . $request->get('name') . '%');
                });
            }

            if ($request->get('national_code') && $request->get('national_code') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->whereHas('info', function ($info) use($request) {
                        $info->where('national_code', 'like', '%' . $request->get('national_code') . '%');
                    });
                });
            }

            if ($request->get('parent_id') && $request->get('parent_id') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->whereHas('marketer', function ($marketer) use($request) {
                        $marketer->where('ancestry', 'like', DB::table('marketer')->where('id', $request->get('parent_id'))->value('ancestry') . '%');
                    });
                });
            }

            if ($request->get('from_date') && $request->get('to_date') && $request->get('from_date') != "" && $request->get('to_date') != "") {
                $q->where('created_at', '>=', $request->get('from_date'));
                $q->where('created_at', '<=', $request->get('to_date'));
            } elseif ($request->get('from_date')) {
                $q->where('created_at', '>=', $request->get('from_date'));
            } elseif ($request->get('to_date')) {
                $q->where('created_at', '<=', $request->get('to_date'));
            }

            if ($request->get('type') && $request->get('type') != "") {
                $q->where('type', $request->get('type'));
            }


            if ($request->get('value') && $request->get('value') != "") {
                $q->where($request->get('value'), '>', 0);
            }

        })->orderBy($request->get('sort_field') ?? 'id', $request->get('sort_type') ?? 'desc');

        if (($request->has('excel_export') && $request->get('excel_export') == true)) {
            $entities = $entities->get();
        } else {
            $entities = $entities->paginate($request->get('limit') ?? 100);
        }

        return response($entities);

    }

    public function group(Request $request)
    {

        $entities = Finance::select('user_id', DB::raw('sum(creditor) as creditor'), DB::raw('sum(debtor) as debtor'), DB::raw('sum(creditor) - sum(debtor) as total'))
            ->with(['user' => function($user) {
                $user->with(['info', 'marketer']);
            }, 'period' => function($p) {
                $p->with(['month']);
            }])->where(function ($q) use($request) {

                if ($request->get('month_id') && $request->get('month_id') != "") {
                    $q->whereHas('period', function ($period) use($request) {
                        $period->where('parent_id', $request->get('month_id'));
                    });
                }

                if ($request->get('period_id') && $request->get('period_id') != "") {
                    $q->where('period_id', $request->get('period_id'));
                }

                if ($request->get('user_id') && $request->get('user_id') != "") {
                    $q->where('user_id', $request->get('user_id'));
                }

                if ($request->get('mobile') && $request->get('mobile') != '') {
                    $q->whereHas('user', function ($user) use($request) {
                        $user->where('mobile', 'like', '%' . $request->get('mobile') . '%');
                    });
                }

                if ($request->get('email') && $request->get('email') != '') {
                    $q->whereHas('user', function ($user) use($request) {
                        $user->where('email', 'like', '%' . $request->get('email') . '%');
                    });
                }

                if ($request->get('username') && $request->get('username') != '') {
                    $q->whereHas('user', function ($user) use($request) {
                        $user->where('username', 'like', '%' . $request->get('username') . '%');
                    });
                }

                if ($request->get('name') && $request->get('name') != '') {
                    $q->whereHas('user', function ($user) use($request) {
                        $user->where(DB::raw('concat(name, " ", family)'), 'like', '%' . $request->get('name') . '%');
                    });
                }

                if ($request->get('national_code') && $request->get('national_code') != '') {
                    $q->whereHas('user', function ($user) use($request) {
                        $user->whereHas('info', function ($info) use($request) {
                            $info->where('national_code', 'like', '%' . $request->get('national_code') . '%');
                        });
                    });
                }

                if ($request->get('parent_id') && $request->get('parent_id') != '') {
                    $q->whereHas('user', function ($user) use($request) {
                        $user->whereHas('marketer', function ($marketer) use($request) {
                            $marketer->where('ancestry', 'like', DB::table('marketer')->where('id', $request->get('parent_id'))->value('ancestry') . '%');
                        });
                    });
                }

                if ($request->get('from_date') && $request->get('to_date') && $request->get('from_date') != "" && $request->get('to_date') != "") {
                    $q->where('created_at', '>=', $request->get('from_date'));
                    $q->where('created_at', '<=', $request->get('to_date'));
                } elseif ($request->get('from_date')) {
                    $q->where('created_at', '>=', $request->get('from_date'));
                } elseif ($request->get('to_date')) {
                    $q->where('created_at', '<=', $request->get('to_date'));
                }

                if ($request->get('type') && $request->get('type') != "") {
                    $q->where('type', $request->get('type'));
                }


                if ($request->get('value') && $request->get('value') != "") {
                    $q->where($request->get('value'), '>', 0);
                }


            })
            ->groupBy('user_id')
            ->orderByRaw(DB::raw('sum(creditor) - sum(debtor) desc'))
            ->having(DB::raw('sum(creditor) - sum(debtor)'), '>', 0)
        ;

        if (($request->has('excel_export') && $request->get('excel_export') == true)) {
            $entities = $entities->get();
        } else {
            $entities = $entities->paginate($request->get('limit') ?? 100);
        }

        return response($entities);

    }

}
