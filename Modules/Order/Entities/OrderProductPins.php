<?php

namespace Modules\Order\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Product\Entities\PackageType;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\Types;

class OrderProductPins extends Model
{
	# New update, use `rebuild` api to change columns from deleted to deleted_at
	use SoftDeletes;
    use HasFactory;

    protected $table = 'order_product_pins';

//    protected $primaryKey = ['order_id', 'period_id'];

    protected $fillable = [];

    protected static function newFactory()
    {
        return \Modules\Order\Database\factories\OrderProductPinsFactory::new();
    }

    public function product()
    {
        return $this->belongsTo(Types::class, 'product_types_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

}
