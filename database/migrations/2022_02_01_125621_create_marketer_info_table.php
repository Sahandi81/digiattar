<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarketerInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketer_info', function (Blueprint $table) {
            $table->integer('marketer_id')->primary();
            $table->string('national_code', 50)->nullable();
            $table->boolean('nationality')->nullable();
            $table->string('card_number', 50)->nullable();
            $table->string('shaba', 50)->nullable();
            $table->string('account_number', 50)->nullable();
            $table->enum('bank', ['mellat', 'saderat', 'parsian'])->nullable()->default('mellat');
            $table->string('father_name', 50)->nullable();
            $table->string('heir_name', 50)->nullable();
            $table->string('heir_last_name', 50)->nullable();
            $table->string('heir_national_code', 50)->nullable();
            $table->tinyInteger('heir_relation')->nullable();
            $table->tinyInteger('education')->nullable();
            $table->string('gov_code', 50)->nullable();
            $table->boolean('gender')->nullable();
            $table->date('birth_date')->nullable();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketer_info');
    }
}
