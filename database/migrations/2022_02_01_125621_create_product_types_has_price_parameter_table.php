<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTypesHasPriceParameterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_types_has_price_parameter', function (Blueprint $table) {
            $table->bigInteger('product_types_id');
            $table->integer('price_parameter_id');
            $table->softDeletes();

            $table->primary(['product_types_id', 'price_parameter_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_types_has_price_parameter');
    }
}
