<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRewardAsRewardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reward_as_reward', function (Blueprint $table) {
            $table->integer('marketer_id');
            $table->integer('period_id');
            $table->bigInteger('reward')->default(0);
            $table->integer('channels')->default(0);
            $table->integer('level_1')->nullable();
            $table->bigInteger('level_1_value')->nullable();
            $table->integer('level_2')->nullable();
            $table->bigInteger('level_2_value')->nullable();
            $table->integer('level_3')->nullable();
            $table->bigInteger('level_3_value')->nullable();
            $table->bigInteger('amount')->default(0);
            $table->softDeletes();

            $table->primary(['marketer_id', 'period_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reward_as_reward');
    }
}
