<?php

namespace Modules\Like\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Like extends Model
{
	# New update, use `rebuild` api to change columns from deleted to deleted_at
	use SoftDeletes;
    use HasFactory, SoftDeletes;

	public const UPDATED_AT = null; # CUZ really we don't need updating time

    protected $fillable = [
		'parent_id',
		'user_id',
	];
    
}
