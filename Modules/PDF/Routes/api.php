<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Modules\PDF\Http\Controllers\PDFController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => '/pdf'], function () {

	Route::get('/', 				[PDFController::class, 'create'])					->name('form.pdf.generator');

});
