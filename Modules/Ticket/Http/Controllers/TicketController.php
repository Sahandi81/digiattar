<?php

namespace Modules\Ticket\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Modules\Notification\Entities\Notification;
use Modules\Ticket\Entities\Category;
use Modules\Ticket\Entities\Conversation;
use Modules\Ticket\Entities\Ticket;
use Modules\User\Entities\Role;
use Modules\User\Entities\User;

class TicketController extends Controller
{
    public function index(Request $request)
    {


        $entities = Ticket::with(['createdBy' => function($q) {
            $q->select('id','name', 'family', 'mobile', 'pic')->with('marketer');
        }, 'category' => function($q) {
            $q->select('id', 'title');
        }])->where( function ($q) use ($request) {

            if ($request->has('filter')) {

                $filter = json_decode($request->get('filter'));

                if ($filter->title) {
                    $q->where('title', $filter->title);
                }

                if ($filter->id) {
                    $q->where('id', $filter->id);
                }
                if (isset($filter->created_by) && $filter->created_by) {
                    $q->where('created_by', $filter->created_by);
                }
                if ($filter->status != -1) {
                    $q->where('status', $filter->status);
                }

                if (isset($filter->category_id) &&  $filter->category_id != -1 and  $filter->category_id != "") {
                    $q->whereIn('category_id', Category::descendantsAndSelf($filter->category_id)->pluck('id'));
                }


                if ($filter->mobile && $filter->mobile != '') {
                    $q->whereHas('createdBy', function ($user) use($filter) {
                        $user->where('mobile', 'like', '%' . $filter->mobile . '%');
                    });
                }

                if ($filter->username && $filter->username != '') {
                    $q->whereHas('createdBy', function ($user) use($filter) {
                        $user->where('username', 'like', '%' . $filter->username . '%');
                    });
                }

                if ($filter->name && $filter->name != '') {
                    $q->whereHas('createdBy', function ($user) use($filter) {
                        $user->where(DB::raw('concat(name, " ", family)'), 'like', '%' . $filter->name . '%');
                    });
                }

                if ($filter->national_code && $filter->national_code != '') {
                    $q->whereHas('createdBy', function ($user) use($filter) {
                        $user->whereHas('info', function ($info) use($filter) {
                            $info->where('national_code', 'like', '%' . $filter->national_code . '%');
                        });
                    });
                }

                if ($filter->parent_id && $filter->parent_id != '') {
                    $q->whereHas('createdBy', function ($user) use($filter) {
                        $user->whereHas('marketer', function ($marketer) use($filter) {
                            $marketer->where('ancestry', 'like', DB::table('marketer')->where('id', $filter->parent_id)->value('ancestry') .'%');
                        });
                    });
                }

                if ($filter->marketer_code && $filter->marketer_code != '') {
                    $q->whereHas('createdBy', function ($user) use($filter) {
                        $user->whereHas('marketer', function ($marketer) use($filter) {
                            $marketer->where('marketer_code', 'like', '%' . $filter->marketer_code . '%');
                        });
                    });
                }

                if ($filter->from_date && $filter->to_date) {
                    $q->where('created_at', '>=', $filter->from_date);
                    $q->where('created_at', '<=', $filter->to_date);
                } elseif ($filter->from_date) {
                    $q->where('created_at', '>=', $filter->from_date);
                } elseif ($filter->to_date) {
                    $q->where('created_at', '<=', $filter->to_date);
                }

                if ($filter->content && $filter->content != '') {
                    $q->whereHas('conversations', function ($c) use($filter) {
                        $c->where('content', 'like', '%' . $filter->content . '%');
                    });
                }



            }

        })->orderBy($request->get('sort_field') ?? 'id', $request->get('sort_type') ?? 'desc')
            ->paginate($request->get('limit') ?? 10);

        return response($entities);

    }


    public function store(Request $request)
    {

        $result = Ticket::create([
            'title' => $request->get('title'),
            'category_id' => $request->get('category_id'),
            'created_by' => $request->get('created_by')
        ]);

        Notification::create([
            'user_id' =>  $request->get('created_by'),
            'template' => 'AdminTicketCreated',
            'token' => $result->id,
            'token20' => User::find($request->get('created_by'))->name . ' ' . User::find($request->get('created_by'))->family
        ]);

        if ($result) {
            return response()->json(['status' => true, 'msg' => 'با موفقیت انجام شد.']);
        }

        return response()->json(['status' => false, 'msg' => 'خطایی رخ داده است.']);
    }



    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     *
     * ticket_id
     */
    public function conversations($id)
    {
        $ticket = Ticket::with(['createdBy', 'conversations' => function($q) {
            $q->with(['createdBy']);
        }, 'category' => function($q) {
            $q->select('id', 'title')->with(['tags']);
        }])->find($id);


        $list = [
            'id' => $ticket->id,
            'title' => $ticket->title,
            'created_by' => [
                'id' => $ticket->createdBy->id,
                'name' => $ticket->createdBy->name,
                'family' => $ticket->createdBy->family,
                'mobile' => $ticket->createdBy->mobile,
                'pic' => $ticket->createdBy->pic
            ],
            'created_at' => $ticket->created_at,
            'category' => $ticket->category,
            'conversations' => [],
        ];

        foreach ($ticket->conversations as $key => $conversation) {
            $list['conversations'][$key] = [
                'files' => [],
                'created_by' => [
                    'id' => $conversation->createdBy->id,
                    'name' => $conversation->createdBy->name,
                    'family' => $conversation->createdBy->family,
                    'mobile' => $conversation->createdBy->mobile,
                    'pic' => $conversation->createdBy->pic
                ],
                'id' => $conversation->id,
                'content' => $conversation->content,
                'is_customer' => $ticket->created_by == $conversation->created_by ? true: false,
                'created_at' => $conversation->created_at,
            ];

            foreach ($conversation->files as $file) {
                $list['conversations'][$key]['files'][] = "ticket/" . $conversation->id . "/" . $file->file;
            }


        }


        return response($list);
    }


    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeConversations($id, Request $request)
    {

        $ticket = Ticket::find($id);

        $result = $ticket->conversations()->create([
            'created_by' => Auth::id(),
            'content' => $request->get('content'),
        ]);

        if ($request->has('file') && $request->get('file') != "") {

            $old = 'attachment/' . $request->get('file');

            $new = 'ticket/' . $result->id . '/' . $request->get('file');

            $move = Storage::disk('public')->move($old, $new); // Move Main Image

            if ($move) {

                $result->files()->create([
                    'created_by' => Auth::id(),
                    'file' => $request->get('file'),
                    'collection' => 0,
                    'directory' => 'ticket',
                ]);
            }
        }



        if ($result) {
            return response()->json(['status' => true, 'msg' => 'با موفقیت انجام شد.']);
        }

        return response()->json(['status' => false, 'msg' => 'خطایی رخ داده است.']);
    }

    /**
     * @param $ticket_id
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteConversation($ticket_id, $id)
    {
        $status = false;

        $cnv = Conversation::find($id);

        if ( ! in_array(Auth::user()->role_id, Role::where('crud', 1)->pluck('id')->toArray())) {
            if (Auth::id() != $cnv->created_by) {
                return response()->json(['status' => false, 'msg' => 'دسترسی لازم برای حذف این سند را ندارید.']);
            }
        }

        if ($cnv->ticket_id == $ticket_id) {

//            $directory_delete_status = Storage::deleteDirectory("ticket/$cnv->id");
//
//            if ($directory_delete_status) {
//                $cnv->files()->delete();
//            }



            $status = $cnv->delete();
        }

        if ($status) {
            return response()->json(['status' => true, 'msg' => 'با موفقیت انجام شد.']);
        }

        return response()->json(['status' => false, 'msg' => 'خطایی رخ داده است.']);

    }




    public function update($ticket_id, Request $request)
    {

        $res = Ticket::find($ticket_id);

        if ($request->has('category_id')) {

            $res->update([
                'category_id' => $request->get('category_id')
            ]);
        } elseif ($request->has('status')) {
            $res->update([
                'status' => $request->get('status')
            ]);
        }

        if ($res) {
            return response()->json(['status' => true, 'msg' => 'عملیات موفقیا آمیز']);
        }

        return response()->json(['status' => true, 'msg' => 'عملیات موفقیا آمیز']);
    }

    public function operatorReport(Request $request)
    {

        try {

            $report = \Illuminate\Support\Facades\DB::select('call sp_ticket_report(?, ?)', [
                $request->get('from_date') ?? null,
                $request->get('to_date') ?? null,
            ]);

            return response($report);

        } catch (\Exception $exception) {
            if ($exception instanceof \Illuminate\Database\QueryException) {
                return response()->json(['status' => false, 'msg' => $exception->getPrevious()->errorInfo[2]]);
            } else {
                return response()->json(['status' => false, 'msg' => $exception->getMessage()]);
            }
        }
    }
}
