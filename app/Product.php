<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Product\Entities\Brand;
use ScoutElastic\Searchable;


class Product extends Model
{
	# New update, use `rebuild` api to change columns from deleted to deleted_at
	use SoftDeletes;
    use Searchable;

    protected $table = 'product';

    protected $indexConfigurator = ProductIndexConfigurator::class;

    // one to one relations

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function toSearchableArray()
    {

        $array = $this->toArray();



        $array['author_name'] = $this->brand ? $this->brand->title : '-';

        return $array;
    }



    protected $mapping = [
        'properties' => [
            'title' => [
                'type' => 'text',
                // Also you can configure multi-fields, more details you can find here https://www.elastic.co/guide/en/elasticsearch/reference/current/multi-fields.html
                'fields' => [
                    'raw' => [
                        'type' => 'keyword',
                    ]
                ]
            ],
        ]
    ];


}
