<?php

namespace Modules\PDF\Http\Controllers;

use Illuminate\Routing\Controller;
use niklasravnsborg\LaravelPdf\Facades\Pdf;

class PDFController extends Controller
{

	public static function create(array $details)
	{
		$pdf = PDF::loadView('pdf::visitForm', compact('details'));
		return $pdf->download('document.pdf');
	}

}
