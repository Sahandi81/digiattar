<?php

namespace Modules\Blog\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Modules\Blog\Entities\Content;

class BlogController extends Controller
{
# Maybe I developed this, but this sh*t isn't mine ..::Sahandi81::..

    public function index(Request $request, $id)
    {

		if (!Cache::tags(['blog', 'content_id'])->has("content[$id]")) {

			$content = Content::with(['relatedBlogs', 'files' => function($q) {
				$q->select('fileable_id', 'fileable_type', 'mime_type', DB::raw('fetch_file_address(id) as prefix'), 'file', 'size')
					->where('collection', 1)
					->orderBy('order', 'asc');
			}, 'products' => function($q) use($request) {
				$q->select('id', 'title', 'slug','brand_id', 'img')
					->with(['brand' => function($q) {
						$q->select('id', 'title', 'id');
					}])
					->where('status', 1) // change to true
					->orderBy($request->get('sort') ?? 'id', $request->get('type') ?? 'desc')
					->get();
			}, 'createdBy' => function($q) {
				$q->select('id', 'name');
			}, 'tags', 'categories' => function($q) use($id) {
				$q->select('id', 'title', 'slug')
					->where('status', 1)
					->with(['contents' => function($q) use($id) {
						$q->select('id', 'slug', 'title', 'heading', 'created_at', 'img')
							->where('status', 1)
							->where(is_numeric($id) ? 'id' : 'slug', '<>', $id)
							->take(5);
					}]);
			}])
				->where( 'id', $id)
				->where('status', 1)
				->first();
			if (!$content){
				return Response::json(['status' => false, 'msg' => 'بلاگ مورد نظر موجود ندارد.']);
			}

			Cache::tags(['blog', 'content_id'])->put("content[$id]", $content , 24 * 60);
		}


		return response(Cache::tags(['blog', 'content_id'])->get("content[$id]"));
    }

    public function update($id, Request $request)
	{

		$fields = $request->validate([
			'content'					=> 'required|string',
			'short_content'				=> 'required|string',
			'meta_title'				=> 'required|string',
			'meta_description'			=> 'required|string',
			'h2_titles'					=> 'required|array|min:1',
			'slug'						=> 'string',
		]);


		if ($request->get('slug') && $request->get('slug') != "") {
			$slug = Content::where('slug', remove_special_char($request->get('slug')))->where('id', '<>', $id)->count();
			if ($slug > 0) {
				return Response()->json(['status' => false, 'msg' => 'اسلاگ قبلا ثبت شده است.']);
			}
		}
		$result = Content::updateOrCreate(['id' => $id], [
			'created_by'			=> Auth::id(),
			'content' 				=> $request->get('content'),
			'short_content' 		=> $request->get('short_content'),
			'slug' 					=> $request->get('slug') == '' ? null : remove_special_char($request->get('slug')),
			'meta_title' 			=> $request->get('meta_title'),
			'h2_titles' 			=> json_encode($fields['h2_titles']),
			'meta_description' 		=> $request->get('meta_description'),
		]);

		$result->tags()->detach();

		if ($request->has('tags')) {
			foreach ($request->get('tags') as $tag) {

				if (!is_numeric($tag)) {
					$try_check = \Modules\Tag\Entities\Tag::where('name', trim($tag))->first();
					if ($try_check) {
						$tag = $try_check->id;
					} else {
						$tag = \Modules\Tag\Entities\Tag::create(['name' => $tag])->id;
					}
				}


				$result->tags()->attach($tag);
			}
		}

		if ($request->has('products')) {
			$result->products()->sync($request->get('products'));
		}


		if ($result) {
			return response()->json(['status' => true, 'msg' => 'عملیات موفقیت امیز بود.', 'details' => $result->refresh()]);
		}

		return response()->json(['status' => false, 'msg' => 'un success'], 200);

	}

    public function destroy($id)
    {
        //
    }
}
