<?php

namespace Developer\Payment\Gateway\Saman;

use Developer\Payment\GatewayAbstract;
use Developer\Payment\GatewayInterface;
use GuzzleHttp\Client;

class Saman extends GatewayAbstract implements GatewayInterface
{


    private $MerchantCode;


    /**
     * Saman constructor.
     */
    public function __construct()
    {
        $this->MerchantCode = 12510092;
        $this->gateway = 'saman';
    }



    public function ready()
    {
        return parent::ready();
    }


    /**
     * @param mixed $parameter
     */
    public function setParameter($parameter): void
    {
        $this->parameter = $parameter;
    }


    public function setInvoice($invoice)
    {
        $this->invoice = $invoice;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    public function setCallback($url)
    {
        $this->callback = $url;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }


    public function setRefId($refId)
    {
        $this->refId = $refId;
    }


    public function setType($type)
    {
        $this->type = $type;
    }


    public function redirect()
    {

        $url = "https://sep.shaparak.ir/MobilePG/MobilePayment";

        $client = new Client();
        $response = $client->request('post', $url, [
            'form_params' => [
                'action' => 'token',
                'Amount' => $this->amount,
                'TerminalId' => $this->MerchantCode,
                'RedirectUrl' => $this->callback,
                'ResNum' => $this->transaction->id
            ]
        ]);


        if ($response->getStatusCode() == 200) {
            $result = json_decode($response->getBody()->getContents());
            if ($result->status > -1) {
                return response([
                    'status' => true,
                    'msg' => 'در حال اتصال به درگاه بانک',
                    'payload' => [
                        'action' => "https://sep.shaparak.ir/OnlinePG/OnlinePG",
                        'method' => 'POST',
                        'fields' => [
                            ['name' => 'token', 'value' =>  $result->token],
                        ]
                    ]
                ]);
            } else {
                return response([
                    'status' => false,
                    'msg' => 'خطا در مرحله ایجاد توکن بانک',
                ]);
            }
        }

    }

    public function verify($transaction)
    {

        $url = 'https://sep.shaparak.ir/verifyTxnRandomSessionkey/ipg/VerifyTranscation';

        if(isset($this->parameter['State']) && $this->parameter['State'] == "OK") {

            // verify
          $client = new Client();
            $response = $client->request('post', $url, [
                'form_params' => [
                    'RefNum' => $this->parameter['RefNum'],
                    'TerminalNumber' => $this->MerchantCode,
                    'TxnRandomSessionkey' => 0,
                    'Nationalcode' => null,
                    'CellNumber' => null,
                    'IgnoreNationalcode' => "True"
                ]
            ]);

            if ($response->getStatusCode() == 200) {
                $result = json_decode($response->getBody()->getContents());
                if ($result->ResultCode == 0) {
                    $this->parameter['verify'] = $result;
                    $this->transactionSucceed($transaction);
                    return ['status'=>true];
                }
            }

        }



        $this->transactionFailed($transaction);
        return ['status'=>false, 'msg' => ''];

    }


}
?>
