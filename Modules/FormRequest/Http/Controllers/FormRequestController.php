<?php

namespace Modules\FormRequest\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\FormRequest\Entities\FormRequest;
use Modules\MLM\Entities\Period;
use Modules\Order\Entities\Order;

class FormRequestController extends Controller
{
    public function index(Request $request)
    {
        $entities = FormRequest::with(['category', 'createdBy', 'operator'])->where(function ($q) use($request) {

            if ($request->has('status') && $request->get('status') != -1) {
                $q->where('status', $request->get('status') ?? 0);
            }


            if ($request->has('category_id') && $request->get('category_id') != -1) {
                $q->where('category_id', $request->get('category_id') ?? 0);
            }


            if ($request->get('marketer_id') && $request->get('marketer_id') != "") {
                $q->where('marketer_id', $request->get('marketer_id'));
            }

            if ($request->get('mobile') && $request->get('mobile') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where('mobile', 'like', '%' . $request->get('mobile') . '%');
                });
            }

            if ($request->get('username') && $request->get('username') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where('username', 'like', '%' . $request->get('username') . '%');
                });
            }

            if ($request->get('name') && $request->get('name') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where(DB::raw('concat(name, " ", family)'), 'like', '%' . $request->get('name') . '%');
                });
            }

            if ($request->get('national_code') && $request->get('national_code') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->whereHas('info', function ($info) use($request) {
                        $info->where('national_code', 'like', '%' . $request->get('national_code') . '%');
                    });
                });
            }


        })
            ->orderBy('id','desc');

        if (($request->has('excel_export') && $request->get('excel_export') == true)) {
            $entities = $entities->get();
        } else {
            $entities = $entities->paginate($request->get('limit') ?? 100);
        }

        return response($entities);
    }

    public function confirmation($id, Request $request)
    {

        $form_request = FormRequest::find($id);

        if ($request->has('status') && $request->get('status') == 'cancel') {

            if (!$request->get('message')) {
                return response(['status' => false, 'msg' => 'دلیل لغو درخواست را وارد کنید.']);
            }

            $form_request->operator_id = Auth::user()->id;
            $form_request->status = 2;
            $form_request->message = $request->get('message');
            $form_request->save();

            return response(['status' => true, 'msg' => 'لغو درخواست ثبت گردید.']);
        }

        switch ($form_request->category->type) {

            case 'revoke':

                $user = \Illuminate\Support\Facades\DB::table('marketer_info as i')
                    ->leftJoin('user as u', 'u.id', '=', 'i.marketer_id')
                    ->where('marketer_id', $form_request->created_by)
                    ->first();



                $arr = [
                    'username' => 'keyhankala',
                    'password' => '12345',
                    'nationalCode' => $user ? $user->national_code : '',
                    'IsCompanyRequest' => true
                ];

                try {

                    $client = new \SoapClient('http://unicode.mimt.gov.ir/nmir.asmx?wsdl');

                    $res = $client->Revoke($arr);

                    if ($res) {


                        $form_request->status = 1;
                        $form_request->message = 'لغو جایگاه با موفقیت انجام شد.';
                        $form_request->operator_id = Auth::user()->id;
                        $form_request->save();

                        return response(['status' => true, 'msg' => 'درخواست با موفقیت انجام شد.']);
                    }

                } catch (\Exception $exception) {

                    $form_request->message = $exception->getMessage();
                    $form_request->operator_id = Auth::user()->id;
                    $form_request->save();

                    return response(['status' => false, 'msg' => $exception->getMessage()]);
                }
                break;
            case 'unrevoke':
                $user = \Illuminate\Support\Facades\DB::table('marketer_info as i')
                    ->leftJoin('user as u', 'u.id', '=', 'i.marketer_id')
                    ->where('marketer_id', $form_request->created_by)
                    ->first();

                if (!$user) return response(['status' => false, 'msg' => 'پروفایل کاربر خالی است.']);

                if (!$user->national_code) return response(['status' => false, 'msg' => 'کد ملی کاربر ثبت نشده است.']);

                if (!$user->gov_code && $user->gov_code === 'NULL') return response(['status' => false, 'msg' => 'کد بازاریابی کاربر خالی است و نیاز به ارسال اطلاعات به وزارت ندارد.']);

                // persian code or invalid national code
                if (!preg_match("/[0-9]/", $user->national_code) || strlen($user->national_code) != 10 || !is_numeric($user->national_code)) return response(['status'=> false, 'msg' => 'کد ملی شما نامعتبر است.']);


                $arr = [
                    'username' => 'keyhankala',
                    'password' => '12345',
                    'nationalCode' => $user->national_code,
                    'IsCompanyRequest' => true
                ];

                try {

                    $client = new \SoapClient('http://unicode.mimt.gov.ir/nmir.asmx?wsdl');

                    $res = $client->Reactive($arr);

                    if ($res) {

                        $form_request->status = 1;
                        $form_request->message = 'بازگشت به مجموعه با موفقیت انجام شد';
                        $form_request->operator_id = Auth::user()->id;
                        $form_request->save();

                        return response(['status' => true, 'msg' => 'درخواست با موفقیت انجام شد.']);
                    }

                } catch (\Exception $exception) {

                    $form_request->message = $exception->getMessage();
                    $form_request->save();

                    return response(['status' => false, 'msg' => $exception->getMessage()]);
                }

                break;
            case 'extradition':

                if (!$request->has('factor') || !$request->get('factor')) {
                    return response(['status' => false, 'msg' => 'شماره فاکتور را وارد نکرده اید']);
                }

                $order = Order::find($request->get('factor'));

                if (!$order) {
                    return response(['status' => false, 'msg' => 'این فاکتور وجود ندارد.']);
                }

                if ($order->status !== 1) {
                    return response(['status' => false, 'msg' => 'فاکتور تایید نشده است.']);
                }

                if ($form_request->created_by !== $order->user_id) {
                    return response(['status' => false, 'msg' => 'فاکتور متعلق به درخواست کننده نیست.']);
                }

                $period = Period::find($order->period_id);
                if ($period->calculated == 1) {
                    return response(['status' => false, 'msg' => 'این فاکتور در محاسبات پورسانت لحاظ شده است و امکان استرداد وجود ندارد.درخواست بای بک ارسال گردد.']);
                }

                $order->returned = 1;
                $form_request->status = 1;
                $form_request->message = 'درخواست استرداد با موفقیت انجام شد.';
                $form_request->operator_id = Auth::user()->id;

                if ($form_request->save() && $order->save()) {

                    dispatch(new \App\Jobs\Order\Queue($order))->onQueue('order');

                    return response(['status' => true, 'msg' => 'درخواست استرداد انجام شد.']);
                }


                return response(['status' => false, 'msg' => 'خطایی رخ داده است.']);
                break;
            case 'byback':

                if (!$request->has('factor') || !$request->get('factor')) {
                    return response(['status' => false, 'msg' => 'شماره فاکتور را وارد نکرده اید']);
                }

                $order = Order::find($request->get('factor'));

                if (!$order) {
                    return response(['status' => false, 'msg' => 'این فاکتور وجود ندارد.']);
                }

                if ($order->status !== 1) {
                    return response(['status' => false, 'msg' => 'فاکتور تایید نشده است.']);
                }

                if ($form_request->created_by !== $order->user_id) {
                    return response(['status' => false, 'msg' => 'فاکتور متعلق به درخواست کننده نیست.']);
                }

                $period = Period::find($order->period_id);
                if ($period->calculated == 0) {
                    return response(['status' => false, 'msg' => 'این فاکتور در محاسبات پورسانت لحاظ نشده است. درخواست استرداد ثبت شود.']);
                }

                $amount = 0;

                if ($order->off_price >= 200000) { // pay from company
                    $post_cost = 20000;
                    $amount += $post_cost;
                }
                $amount += $order->off_price * 46 / 100;
                $amount += $order->tax;

                $final = intval($order->total_pay - $amount);

                $order->returned = 2;
                $form_request->status = 1;
                $msg = " درخواست بای بک با موفقیت ثبت گردید و مبلغ " . number_format($final) . " تومان به عنوان مبلغ بازگشتی به کاربر محاسبه گردید. ";
                $form_request->message = $msg;
                $form_request->operator_id = Auth::user()->id;

                if ($form_request->save() && $order->save()) {

                    dispatch(new \App\Jobs\Order\Queue($order))->onQueue('order');

                    return response(['status' => true, 'msg' => 'درخواست با موفقیت انجام شد.']);
                }


                return response(['status' => false, 'msg' => 'خطایی رخ داده است.']);
                break;
            default:
                break;
        }



        return response(['status' => true, 'msg' => 'بدون عملکرد']);

    }
}
