<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductHasAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_has_attributes', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('product_id');
            $table->bigInteger('attribute_id');
            $table->bigInteger('tag_id')->nullable();
            $table->longText('value')->nullable();
            $table->tinyInteger('sort')->default(0);
            $table->softDeletes();

            $table->unique(['product_id', 'attribute_id', 'tag_id'], 'product_id_attribute_id_tag_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_has_attributes');
    }
}
