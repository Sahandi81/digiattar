<?php

namespace Modules\Period\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Month extends Model
{
	# New update, use `rebuild` api to change columns from deleted to deleted_at
	use SoftDeletes;
    use HasFactory;

    protected $guarded = [];

    protected $table = 'period_month';

    protected $primaryKey = 'id';

    public function period(): HasMany
	{
        return $this->hasMany(Period::class, 'parent_id');
    }
}
