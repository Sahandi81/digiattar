<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDomainTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domain', function (Blueprint $table) {
            $table->string('key', 50)->primary();
            $table->string('backend', 50);
            $table->string('frontend', 50);
            $table->string('name', 100);
            $table->string('meta_title');
            $table->string('meta_description');
            $table->mediumText('introduce')->nullable();
            $table->string('copy_right')->nullable();
            $table->string('blog_title')->nullable();
            $table->string('blog_description')->nullable();
            $table->string('shop_title')->nullable();
            $table->string('shop_description')->nullable();
            $table->boolean('status')->default(true);
            $table->decimal('min_purchase', 10, 0)->default(0);
            $table->decimal('free_postage', 10, 0)->default(100000);
            $table->decimal('default_post_cost', 10, 0)->default(5000);
            $table->dateTime('created_at')->nullable();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domain');
    }
}
