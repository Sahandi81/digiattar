<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarketerCutPasteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketer_cut_paste', function (Blueprint $table) {
            $table->integer('marketer_id');
            $table->integer('new_parent_id')->default(0);
            $table->boolean('new_position');
            $table->integer('old_parent_id')->default(0);
            $table->boolean('old_position');
            $table->boolean('status')->default(false);
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->softDeletes();

            $table->primary(['marketer_id', 'new_parent_id', 'new_position']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketer_cut_paste');
    }
}
