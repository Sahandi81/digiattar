<?php

namespace Modules\User\Entities;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Modules\Marketer\Entities\Info;
use Modules\Marketer\Entities\Marketer;
use Modules\Medical\Entities\MedicalRecords;
use Modules\Medical\Entities\VisitForm;

class User extends Authenticatable
{


    use HasApiTokens, Notifiable;


    const USER_TYPE_GUEST = 'guest';

    protected $table = 'user';

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function role(): \Illuminate\Database\Eloquent\Relations\BelongsTo
	{
        return $this->belongsTo(Role::class, 'role_id');
    }

	public function form(): \Illuminate\Database\Eloquent\Relations\HasOne
	{
		return $this->hasOne(VisitForm::class);
	}

    public function marketer(): \Illuminate\Database\Eloquent\Relations\HasOne
	{
        return $this->hasOne(Marketer::class, 'id');
    }

    public function info(): \Illuminate\Database\Eloquent\Relations\HasOne
	{
        return $this->hasOne(Info::class, 'marketer_id');
    }

	# Medical
	public function records(): \Illuminate\Database\Eloquent\Relations\HasMany
	{
		return $this->hasMany(MedicalRecords::class);
	}
}
