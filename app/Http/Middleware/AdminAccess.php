<?php

namespace App\Http\Middleware;

use App\Role;
use App\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\User\Entities\Permission;

class AdminAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $access = true;

        if (Auth::check()) {

            $role = Auth::user()->role_id;

            if (Auth::user()->role->admin_access) {

                $permission = Permission::where('method', strtoupper($request->getMethod()))->where('url', 'like',  $request->getPathInfo())->first();

                if (!in_array($role, \Modules\User\Entities\Role::where('full_access', 1)->pluck('id')->toArray()) ) {
                    if ($permission) {
                        $has_permission = DB::table('role_has_permissions')->where('role_id', $role)->where('permission_id', $permission->id)->count();
                        if ($has_permission == 0) {
                            $access = false;
                        }
                    }
                }

                if ($permission) {
                    DB::table('user_log')->insert([
                        'user_id' => Auth::id(),
                        'permission_id'=> $permission->id,
                        'ip' => $request->ip(),
                        'url' => $request->getPathInfo(),
                        'referer' => request()->headers->get('referer'),
                        'parameter' => json_encode($request->all())
                    ]);
                }
            }
        }

        if (! $access) {
            return response()->json(['status' => false, 'msg' => 'no access'], 401);
        }


        return $next($request);
    }
}
