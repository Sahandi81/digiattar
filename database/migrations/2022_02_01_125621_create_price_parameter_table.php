<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePriceParameterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('price_parameter', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->unsignedInteger('_lft')->default('0')->index('_lft');
            $table->unsignedInteger('_rgt')->default('0')->index('_rgt');
            $table->unsignedInteger('parent_id')->nullable()->index('parent_id');
            $table->boolean('status')->default(true)->index('status');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('price_parameter');
    }
}
