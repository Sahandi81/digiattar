<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderProductPinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_product_pins', function (Blueprint $table) {
            $table->integer('order_id');
            $table->integer('product_types_id');
            $table->integer('count');
            $table->integer('price');
            $table->integer('discount');
            $table->integer('off_price');
            $table->integer('tax')->default(0);
            $table->integer('weight');
            $table->mediumText('parameter')->nullable();
            $table->dateTime('created_at')->nullable();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->softDeletes();

            $table->primary(['order_id', 'product_types_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_product_pins');
    }
}
