<?php

namespace Modules\Marketer\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\User\Entities\User;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class Marketer extends Model
{
	# New update, use `rebuild` api to change columns from deleted to deleted_at
	use SoftDeletes;

    protected $table = 'marketer';

    protected $primaryKey = 'id';

    public $incrementing = false;

    public $timestamps = false;

    protected $guarded = [];

    public function parent()
    {
        return $this->belongsTo(User::class, 'parent_id');

    }

    public function user()
    {
        return $this->belongsTo(User::class, 'id');
    }

    public function info()
    {
        return $this->hasOne(Info::class, 'marketer_id');
    }

    /*
     * revoke marketer
     * get marketer id parameter
     */
    public static function revoke($marketer_id)
    {

        $user = \Illuminate\Support\Facades\DB::table('marketer_info as i')
            ->leftJoin('user as u', 'u.id', '=', 'i.marketer_id')
            ->where('marketer_id', $marketer_id)
            ->first();

        if (!$user) return response(['status' => true, 'msg' => 'کد ملی کاربر خالی است.']);


        $arr = [
            'username' => 'keyhankala',
            'password' => '12345',
            'nationalCode' => $user ? $user->national_code : '',
            'IsCompanyRequest' => true
        ];

        try {

            $client = new \SoapClient('http://unicode.mimt.gov.ir/nmir.asmx?wsdl');

            $res = $client->Revoke($arr);

            if ($res) {
                return response(['status' => true, 'msg' => 'درخواست با موفقیت انجام شد.']);
            }

            return response(['status' => true, 'msg' => 'خطای وب سرویس']);

        } catch (\Exception $exception) {

            return response(['status' => false, 'msg' => $exception->getMessage()]);
        }
    }


    public static function 			getMarketerCode($marketer_id)
    {
        $user = DB::table('marketer_info as i')
            ->leftJoin('user as u', 'u.id', '=', 'i.marketer_id')
            ->where('marketer_id', $marketer_id)
            ->first();

        if (!$user) return ['status' => false, 'msg' => 'اطلاعات پروفایل تکمیل نیست'];

        if ($user->gov_code && $user->gov_code !== 'NULL') return ['status' => false, 'msg' => 'کد وزارت  قبلا صادر گردیده است.'];

        // persian code or invalid national code
        if (!preg_match("/[0-9]/", $user->national_code) || strlen($user->national_code) != 10 || !is_numeric($user->national_code)) return ['status'=> false, 'msg' => 'کد ملی  نامعتبر است.'];


        if (!$user->name || !$user->family || !$user->birth_date)  return ['status' => false, 'msg' => 'اطلاعات پروفایل ناقص میباشد. شامل تاریخ تولد, نام پدر و ...'];

        $arr = [
            'username' 			=> 'keyhankala',
            'password' 			=> '12345',
            'firstName' 		=> $user->name,
            'lastName' 			=> $user->family,
            'birthDate' 		=> $user->birth_date,
            'fatherName' 		=> $user->father_name,
            'nationalCode' 		=> $user->national_code,
            'phone1' 			=> $user->mobile,
            'phone2' 			=> $user->mobile,
            'email' 			=> $user->email,
            'education' 		=> $user->education ? $user->education : '',
            'idNo' 				=> $user->national_code ? $user->national_code : $user->national_code,
            'address' 			=> 'وارد نشده',
            'postalCode' 		=> 'وارد نشده',
            'parentNationalCode'=> $user->heir_national_code ? $user->heir_national_code : $user->heir_national_code,
        ];

        try {

            $client = new \SoapClient('http://unicode.mimt.gov.ir/nmir.asmx?wsdl');

            $res = $client->Register($arr);

            if ($res->RegisterResult) {

                if ($res->RegisterResult === 'error:EmployeeIsActive') {
                    return ['status' => false, 'msg' => 'کد ملی در شرکت دیگری درگیر است.'];
                } elseif ($res->RegisterResult === 'error:InvalidNationalCode') {
                    return ['status' => false, 'msg' => 'کد ملی از دید وزارت صنعت معدن تجارت نامعتبر است.'];
                } else {

                    DB::table('marketer_info')->where('marketer_id', $marketer_id)->update([
                        'gov_code' => $res->RegisterResult,
                    ]);

                    return ['status' => true, 'msg' => 'کد وزارت با موفقیت دریافت گردید'];
                }
            }

        } catch (\Exception $exception) {

            return ['status' => false, 'msg' => 'عدم دریافت کد از سمت وزارت.پروفایل را تکمیل و مجددا امتحان کنید', 'exception' => $exception->getMessage()];
        }
    }


    public function contract()
    {
        return $this->hasMany(Contract::class)->where('status', 1);
    }




}
