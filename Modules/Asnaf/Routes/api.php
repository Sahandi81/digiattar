<?php

use Illuminate\Http\Request;
use Modules\Product\Entities\Product;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('backend')->group(function () {

    Route::prefix('asnaf')->middleware('auth:api')->group(function () {

        Route::get('/branch', 'AsnafController@branchList');

        Route::post('/branch', 'AsnafController@branchStore')->name('asnaf_branch_post');

        Route::get('/product', 'AsnafController@productList');

        Route::post('/product', 'AsnafController@productStore')->name('asnaf_product_post');

        Route::get('/cities', function () {
            return response(\Modules\Asnaf\Entities\AsnafCity::all());
        });

        Route::get('/fetchProvinceAndCity', function () {

            $arr = [
                "password" => 'Aa@3392@Aa@3023',
            ];

            $client = new \SoapClient('http://easnaf.mimt.sis-eg.com/fa/index.php?module=cdk&func=loadmodule&system=cdk&sismodule=user/call_function.php&ctp_id=62&func_name=wsrvTypeWSDL&type_name=mim_branch_issuance_change&wsServiceTyps=soap', ['encoding' => 'UTF-8']);
            $result = $client->__soapCall('getProvinceCityList', $arr);

            foreach (json_decode($result, true)['response'] as $item) {
                \Illuminate\Support\Facades\DB::table('asnaf_province')->insertGetId([
                    'id' => $item['provinceId'],
                    'title' => $item['provinceTitle']
                ]);

                foreach ($item['provinceCity'] as $value) {
                    \Illuminate\Support\Facades\DB::table('asnaf_city')->insertGetId([
                        'id' => $value['cityId'],
                        'province_id' => $item['provinceId'],
                        'title' => $value['cityTitle'],
                        'center' => $value['cityCenter']
                    ]);
                }
            }

        });
    });
});
