<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Marketer\Entities\Info;
use Modules\Marketer\Entities\Marketer;
use Modules\User\Entities\User;

class MarketerCode extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'MarketerCode:command';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'get marketer code';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $entities = Info::whereNull('gov_code')
            ->whereNotNull('national_code')
            ->whereNotNull('father_name')
            ->whereNotNull('heir_name')
            ->whereNotNull('heir_last_name')
            ->whereNotNull('heir_national_code')
            ->get()->random(100);

        foreach ($entities as $entity) {
            dispatch(new \App\Jobs\Marketer\MarketerCode($entity))->onQueue('mc');
        }
    }
}
