<?php

namespace Modules\Temperament\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Temperament extends Model
{
	# New update, use `rebuild` api to change columns from deleted to deleted_at
	use SoftDeletes;
    use HasFactory;

    protected $primaryKey = 'temperament_id';
    protected $fillable = [];

    protected static function newFactory()
    {
        return \Modules\Temperament\Database\factories\TemperamentFactory::new();
    }
}
