<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->primary();
            $table->integer('user_id')->index('user_id');
            $table->integer('transactionable_id')->nullable();
            $table->string('transactionable_type')->nullable()->default('order');
            $table->integer('period_id')->nullable()->index('period_id');
            $table->enum('gateway', ['parsian', 'pasargad', 'zarinpal', 'saderat', 'saman'])->nullable();
            $table->integer('amount');
            $table->string('description');
            $table->enum('status', ['pending', 'failed', 'success'])->default('pending');
            $table->dateTime('deadline')->nullable();
            $table->dateTime('payment_date')->nullable();
            $table->string('ref_id', 50)->nullable();
            $table->enum('type', ['none', 'creditor', 'debtor'])->default('none');
            $table->enum('finance', ['none', 'voucher', 'wallet'])->default('none');
            $table->string('remember_token')->nullable();
            $table->dateTime('created_at');
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->longText('data')->nullable();
            $table->softDeletes();

            $table->index(['transactionable_id', 'transactionable_type'], 'transactionable_id_transactionable_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction');
    }
}
