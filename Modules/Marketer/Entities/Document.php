<?php

namespace Modules\Marketer\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Modules\User\Entities\User;

class Document extends Model
{
	# New update, use `rebuild` api to change columns from deleted to deleted_at
	use SoftDeletes;

    protected $table = 'marketer_document';

//    protected $primaryKey = ['marketer_id', 'type'];

    public $incrementing = false;


    protected $guarded = [];

    public function marketer()
    {
        return $this->belongsTo(Marketer::class, 'marketer_id');

    }

    public function user()
    {
        return $this->belongsTo(User::class, 'marketer_id');

    }



}
