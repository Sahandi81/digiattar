<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('heading')->nullable();
            $table->unsignedInteger('_lft')->default('0')->index('_lft');
            $table->unsignedInteger('_rgt')->default('0')->index('_rgt');
            $table->unsignedInteger('parent_id')->nullable()->index('parent_id');
            $table->boolean('status')->default(true)->index('status');
            $table->string('slug')->nullable();
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->text('content')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_category');
    }
}
