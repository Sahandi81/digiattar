<?php

namespace Modules\Order\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Address\Entities\Address;
use Modules\Period\Entities\Period;
use Modules\Product\Entities\Product;
use Modules\Transaction\Entities\Transaction;
use Modules\User\Entities\User;


class Order extends Model
{
	# New update, use `rebuild` api to change columns from deleted to deleted_at
	use SoftDeletes;
    protected $primaryKey = 'id';
    protected $table = 'order';
    protected $guarded = [];


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function period()
    {
        return $this->belongsTo(Period::class, 'period_id');
    }

    public function address()
    {
        return $this->belongsTo(Address::class, 'address_id');
    }

    public function transaction()
    {
        return $this->hasOne(Transaction::class, 'transactionable_id')->where('status', 'success');
    }

    public function notes()
    {
        return $this->hasMany(Note::class);
    }

    public function productPins()
    {
        return $this->hasMany(OrderProductPins::class);
    }
}

