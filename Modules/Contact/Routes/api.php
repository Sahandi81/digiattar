<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('backend')->group(function () {

    Route::prefix('contact')->middleware(['auth:api', 'admin_access'])->group(function () {

        Route::get('/', 'ContactController@index');
        Route::put('/{id}/status', 'ContactController@changeStatus');

    });
});
