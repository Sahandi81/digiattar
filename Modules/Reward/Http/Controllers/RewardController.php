<?php

namespace Modules\Reward\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Marketer\Entities\Point;
use Modules\Order\Entities\Order;
use Modules\Reward\Entities\Commission;
use Modules\Reward\Entities\Reagent;
use Modules\Reward\Entities\Reward;
use Modules\Reward\Entities\RewardAsReward;
use Modules\Reward\Entities\Wallet;

class RewardController extends Controller
{

    public function index(Request $request)
    {
        $entities = Commission::select('id', 'marketer_id', 'period_id', DB::raw('amount as total'),
            'status', 'payment_ref', 'payment_date', 'payment_text', 'reason', DB::raw('fn_marketer_maintain(marketer_id, period_id, 0) as maintain'))
			->with(['user' => function($user) {
            $user->with(['info', 'marketer']);
        }, 'marketer' => function($q2) {
                $q2->with('contract');
        }, 'period' => function($p) {
            return $p->with(['month']);
        }])->where(function ($q) use($request) {

            if ($request->get('month_id') && $request->get('month_id') != "") {
                $q->whereHas('period', function ($period) use($request) {
                    $period->where('parent_id', $request->get('month_id'));
                });
            }

            if ($request->get('period_id') && $request->get('period_id') != "") {
                $q->where('period_id', $request->get('period_id'));
            }

            if ($request->get('status') != -1) {
                $q->where('status', $request->get('status'));
            }


            if ($request->get('reason') != -1) {
                $q->where('reason', $request->get('reason'));
            }

            if ($request->get('marketer_id') && $request->get('marketer_id') != "") {
                $q->where('marketer_id', $request->get('marketer_id'));
            }

            if ($request->get('mobile') && $request->get('mobile') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where('mobile', 'like', '%' . $request->get('mobile') . '%');
                });
            }

            if ($request->get('username') && $request->get('username') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where('username', 'like', '%' . $request->get('username') . '%');
                });
            }

            if ($request->get('name') && $request->get('name') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where(DB::raw('concat(name, " ", family)'), 'like', '%' . $request->get('name') . '%');
                });
            }

            if ($request->get('national_code') && $request->get('national_code') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->whereHas('info', function ($info) use($request) {
                        $info->where('national_code', 'like', '%' . $request->get('national_code') . '%');
                    });
                });
            }

            if ($request->get('parent_id') && $request->get('parent_id') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->whereHas('marketer', function ($marketer) use($request) {
                        $marketer->where('ancestry', 'like', DB::table('marketer')->where('id', $request->get('parent_id'))->value('ancestry') . '%');
                    });
                });
            }


        })
            ->orderByRaw('period_id desc, total desc');

        if ($request->has('excel_export') && $request->get('excel_export') == true) {
            $entities = $entities->get();
        } else {
            $entities = $entities->paginate($request->get('limit') ?? 100);
        }

        return response($entities);
    }

    public function detail($marketer_id, $period_id)
    {
        $entities = Reward::select('marketer_id', 'period_id', 'type', DB::raw('sum(amount) as total'))
            ->groupby('marketer_id', 'period_id', 'type')
            ->where('period_id', $period_id)
            ->where('marketer_id', $marketer_id)
            ->get();

        return response($entities);
    }

    public function reagent(Request $request)
    {
        $entities = Reagent::with(['user', 'marketer', 'period' => function($p) {
            return $p->with(['month']);
        }])->where(function ($q) use($request) {

            if ($request->get('month_id') && $request->get('month_id') != "") {
                $q->whereHas('period', function ($period) use($request) {
                    $period->where('parent_id', $request->get('month_id'));
                });
            }

            if ($request->get('period_id') && $request->get('period_id') != "") {
                $q->where('period_id', $request->get('period_id'));
            }


            if ($request->get('node') && $request->get('node') != "") {
                $q->where('node', $request->get('node'));
            }

            if ($request->get('marketer_id') && $request->get('marketer_id') != "") {
                $q->where('marketer_id', $request->get('marketer_id'));
            }

            if ($request->get('mobile') && $request->get('mobile') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where('mobile', 'like', '%' . $request->get('mobile') . '%');
                });
            }

            if ($request->get('username') && $request->get('username') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where('username', 'like', '%' . $request->get('username') . '%');
                });
            }

            if ($request->get('name') && $request->get('name') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where(DB::raw('concat(name, " ", family)'), 'like', '%' . $request->get('name') . '%');
                });
            }

            if ($request->get('national_code') && $request->get('national_code') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->whereHas('info', function ($info) use($request) {
                        $info->where('national_code', 'like', '%' . $request->get('national_code') . '%');
                    });
                });
            }

            if ($request->get('parent_id') && $request->get('parent_id') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->whereHas('marketer', function ($marketer) use($request) {
                        $marketer->where('ancestry', 'like', DB::table('marketer')->where('id', $request->get('parent_id'))->value('ancestry') . '%');
                    });
                });
            }


        })->orderByRaw('period_id desc, value desc');
        if ($request->has('excel_export') && $request->get('excel_export') == true) {
            $entities = $entities->get();
        } else {
            $entities = $entities->paginate($request->get('limit') ?? 100);
        }

        return response($entities);
    }

    public function retail(Request $request)
    {
        $entities = Reward::select('marketer_id', 'period_id', DB::raw('sum(amount) as total'), 'type')->with(['user', 'marketer', 'period' => function($p) {
            return $p->with(['month']);
        }])->where(function ($q) use($request) {

            if ($request->get('month_id') && $request->get('month_id') != "") {
                $q->whereHas('period', function ($period) use($request) {
                    $period->where('parent_id', $request->get('month_id'));
                });
            }

            if ($request->get('period_id') && $request->get('period_id') != "") {
                $q->where('period_id', $request->get('period_id'));
            }

            if ($request->get('status') && $request->get('status') != -1) {
                $q->where('status', $request->get('status'));
            }


            if ($request->get('marketer_id') && $request->get('marketer_id') != "") {
                $q->where('marketer_id', $request->get('marketer_id'));
            }

            if ($request->get('mobile') && $request->get('mobile') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where('mobile', 'like', '%' . $request->get('mobile') . '%');
                });
            }

            if ($request->get('username') && $request->get('username') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where('username', 'like', '%' . $request->get('username') . '%');
                });
            }

            if ($request->get('name') && $request->get('name') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where(DB::raw('concat(name, " ", family)'), 'like', '%' . $request->get('name') . '%');
                });
            }

            if ($request->get('national_code') && $request->get('national_code') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->whereHas('info', function ($info) use($request) {
                        $info->where('national_code', 'like', '%' . $request->get('national_code') . '%');
                    });
                });
            }

            if ($request->get('parent_id') && $request->get('parent_id') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->whereHas('marketer', function ($marketer) use($request) {
                        $marketer->where('ancestry', 'like',  DB::table('marketer')->where('id', $request->get('parent_id'))->value('ancestry') . '%');
                    });
                });
            }


        })
            ->where('type', 'retail')
            ->groupBy('marketer_id', 'period_id', 'type')
            ->orderByRaw('period_id desc, total desc');


        if ($request->has('excel_export') && $request->get('excel_export') == true) {
            $entities = $entities->get();
        } else {
            $entities = $entities->paginate($request->get('limit') ?? 100);
        }

        return response($entities);
    }


    public function rewardAsReward(Request $request)
    {
        $entities = RewardAsReward::with(['user', 'marketer', 'period' => function($p) {
            return $p->with(['month']);
        }])->where(function ($q) use($request) {

            if ($request->get('month_id') && $request->get('month_id') != "") {
                $q->whereHas('period', function ($period) use($request) {
                    $period->where('parent_id', $request->get('month_id'));
                });
            }

            if ($request->get('period_id') && $request->get('period_id') != "") {
                $q->where('period_id', $request->get('period_id'));
            }


            if ($request->get('channels') && $request->get('channels') != "-1") {
                $q->where('channels', $request->get('channels'));
            }

            if ($request->get('marketer_id') && $request->get('marketer_id') != "") {
                $q->where('marketer_id', $request->get('marketer_id'));
            }

            if ($request->get('mobile') && $request->get('mobile') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where('mobile', 'like', '%' . $request->get('mobile') . '%');
                });
            }

            if ($request->get('username') && $request->get('username') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where('username', 'like', '%' . $request->get('username') . '%');
                });
            }

            if ($request->get('name') && $request->get('name') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where(DB::raw('concat(name, " ", family)'), 'like', '%' . $request->get('name') . '%');
                });
            }

            if ($request->get('national_code') && $request->get('national_code') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->whereHas('info', function ($info) use($request) {
                        $info->where('national_code', 'like', '%' . $request->get('national_code') . '%');
                    });
                });
            }

            if ($request->get('parent_id') && $request->get('parent_id') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->whereHas('marketer', function ($marketer) use($request) {
                        $marketer->where('ancestry', 'like',  DB::table('marketer')->where('id', $request->get('parent_id'))->value('ancestry') .'%');
                    });
                });
            }


        })
            ->orderByRaw('period_id desc, amount desc');

        if ($request->has('excel_export') && $request->get('excel_export') == true) {
            $entities = $entities->get();
        } else {
            $entities = $entities->paginate($request->get('limit') ?? 100);
        }

        return response($entities);
    }

    public function saving(Request $request)
    {
        $entities = Wallet::with(['user', 'marketer', 'period' => function($p) {
            return $p->with(['month']);
        }])->where(function ($q) use($request) {

            if ($request->get('month_id') && $request->get('month_id') != "") {
                $q->whereHas('period', function ($period) use($request) {
                    $period->where('parent_id', $request->get('month_id'));
                });
            }

            if ($request->get('period_id') && $request->get('period_id') != "") {
                $q->where('period_id', $request->get('period_id'));
            }

            if ($request->get('status') && $request->get('status') != -1) {
                $q->where('status', $request->get('status'));
            }


            if ($request->get('type') && $request->get('type') != "") {
                $q->where('type', $request->get('type'));
            }

            if ($request->get('marketer_id') && $request->get('marketer_id') != "") {
                $q->where('marketer_id', $request->get('marketer_id'));
            }

            if ($request->get('mobile') && $request->get('mobile') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where('mobile', 'like', '%' . $request->get('mobile') . '%');
                });
            }

            if ($request->get('username') && $request->get('username') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where('username', 'like', '%' . $request->get('username') . '%');
                });
            }

            if ($request->get('name') && $request->get('name') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where(DB::raw('concat(name, " ", family)'), 'like', '%' . $request->get('name') . '%');
                });
            }

            if ($request->get('national_code') && $request->get('national_code') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->whereHas('info', function ($info) use($request) {
                        $info->where('national_code', 'like', '%' . $request->get('national_code') . '%');
                    });
                });
            }

            if ($request->get('parent_id') && $request->get('parent_id') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->whereHas('marketer', function ($marketer) use($request) {
                        $marketer->where('ancestry', 'like',  DB::table('marketer')->where('id', $request->get('parent_id'))->value('ancestry') . '%');
                    });
                });
            }


        })
            ->orderByRaw('period_id desc, amount desc');

        if ($request->has('excel_export') && $request->get('excel_export') == true) {
            $entities = $entities->get();
        } else {
            $entities = $entities->paginate($request->get('limit') ?? 100);
        }

        return response($entities);
    }


    public function point(Request $request)
    {

        $entities = Point::with(['user', 'marketer', 'period' => function($p) {
            return $p->with(['month']);
        }])->where(function ($q) use($request) {

            if ($request->get('month_id') && $request->get('month_id') != "") {
                $q->whereHas('period', function ($period) use($request) {
                    $period->where('parent_id', $request->get('month_id'));
                });
            }

            if ($request->get('period_id') && $request->get('period_id') != "") {
                $q->where('period_id', $request->get('period_id'));
            }

            if ($request->get('marketer_id') && $request->get('marketer_id') != "") {
                $q->where('marketer_id', $request->get('marketer_id'));
            }

            if ($request->get('mobile') && $request->get('mobile') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where('mobile', 'like', '%' . $request->get('mobile') . '%');
                });
            }

            if ($request->get('username') && $request->get('username') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where('username', 'like', '%' . $request->get('username') . '%');
                });
            }

            if ($request->get('name') && $request->get('name') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where(DB::raw('concat(name, " ", family)'), 'like', '%' . $request->get('name') . '%');
                });
            }

            if ($request->get('national_code') && $request->get('national_code') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->whereHas('info', function ($info) use($request) {
                        $info->where('national_code', 'like', '%' . $request->get('national_code') . '%');
                    });
                });
            }

            if ($request->get('parent_id') && $request->get('parent_id') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->whereHas('marketer', function ($marketer) use($request) {
                        $marketer->where('ancestry', 'like', DB::table('marketer')->where('id', $request->get('parent_id'))->value('ancestry') . '%');
                    });
                });
            }


        })
            ->orderByRaw('period_id desc, group_sales desc');
        if ($request->has('excel_export') && $request->get('excel_export') == true) {
            $entities = $entities->get();
        } else {
            $entities = $entities->paginate($request->get('limit') ?? 100);
        }

        return response($entities);

    }

    public function deposit(Request $request)
    {

        if ($request->get('deposit_status') == -1) {
            return response(['status' => false, 'msg' => 'وضعیت پرداخت را مشخص نمایید.']);
        }

        $entities = Commission::where(function ($q) use($request) {

            if ($request->get('month_id') && $request->get('month_id') != "") {
                $q->whereHas('period', function ($period) use($request) {
                    $period->where('parent_id', $request->get('month_id'));
                });
            }

            if ($request->get('period_id') && $request->get('period_id') != "") {
                $q->where('period_id', $request->get('period_id'));
            }

            if ($request->get('status') != -1) {
                $q->where('status', $request->get('status'));
            }



            if ($request->get('reason') != -1) {
                $q->where('reason', $request->get('reason'));
            }

            if ($request->get('marketer_id') && $request->get('marketer_id') != "") {
                $q->where('marketer_id', $request->get('marketer_id'));
            }

            if ($request->get('mobile') && $request->get('mobile') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where('mobile', 'like', '%' . $request->get('mobile') . '%');
                });
            }

            if ($request->get('username') && $request->get('username') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where('username', 'like', '%' . $request->get('username') . '%');
                });
            }

            if ($request->get('name') && $request->get('name') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where(DB::raw('concat(name, " ", family)'), 'like', '%' . $request->get('name') . '%');
                });
            }

            if ($request->get('national_code') && $request->get('national_code') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->whereHas('info', function ($info) use($request) {
                        $info->where('national_code', 'like', '%' . $request->get('national_code') . '%');
                    });
                });
            }

            if ($request->get('parent_id') && $request->get('parent_id') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->whereHas('marketer', function ($marketer) use($request) {
                        $marketer->where('ancestry', 'like', DB::table('marketer')->where('id', $request->get('parent_id'))->value('ancestry') . '%');
                    });
                });
            }

            if ($request->get('ids')) { // for excel print or cumulative
                $q->whereIn('id', explode(',', $request->get('ids')));
            }


        });

        $result = $entities->update([
            'status' => $request->get('deposit_status') ?? 0,
            'reason' => $request->get('deposit_reason') == -1 ? 0 : $request->get('deposit_reason'),
            'payment_date' => $request->get('payment_date') ? $request->get('payment_date') : null,
            'payment_ref' => $request->get('payment_ref') ? $request->get('payment_ref') : null,
            'payment_text' => $request->get('payment_text') ? $request->get('payment_text') : null,
            'payment_amount' => $request->get('payment_amount') ? $request->get('payment_amount') : null,
        ]);

        if ($result) {
            return response(['status' => true, 'msg' => 'عملیات موفقیت آمیز بود.']);
        }

        return response(['status' => false, 'msg' => 'ورودی معتبر وارد کند']);


    }

}
