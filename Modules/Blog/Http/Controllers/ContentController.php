<?php

namespace Modules\Blog\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Modules\Blog\Entities\Category;
use Modules\Blog\Entities\Content;
use Modules\Blog\Entities\RelatedContent;
use phpDocumentor\Reflection\DocBlock\Tag;

class ContentController extends Controller
{
    public function index(Request $request)
    {
        $entities = Content::with(['categories', 'relatedBlogs', 'createdBy' => function($q) {
            $q->select('id','name', 'family');
        }])->where( function ($q) use ($request) {

            if ($request->has('filter')) {

                $filter = json_decode($request->get('filter'), true);

                if (isset($filter['id'])) {
                    $q->where('id', '=', $filter['id']);
                }

                if (isset($filter['title'])) {
                    $q->where('title', 'like', '%' . $filter['title'] . '%');
                }

                if (isset($filter['created_by']) && $filter['created_by'] != -1 and  $filter['created_by'] != "") {
                    $q->where('created_by', '=', $filter['created_by']);
                }

                if (isset($filter['category_id']) &&  $filter['category_id'] != -1 and  $filter['category_id'] != "") {
                    $q->whereHas('categories' , function($q) use($filter){
                        $q->where('id',$filter['category_id']);
                    });
                }

                if (isset($filter['status']) && $filter['status'] != -1) {
                    $q->where('status', $filter['status']);
                }

                if (isset($filter['is_blog']) && $filter['is_blog'] != -1) {
                    $q->where('is_blog', $filter['is_blog']);
                }

                if (isset($filter['is_popup']) && $filter['is_popup'] != -1) {
                    $q->where('is_popup', $filter['is_popup']);
                }

                if (isset($filter['is_private']) && $filter['is_private'] != -1) {
                    $q->where('is_private', $filter['is_private']);
                }

            }
        })->orderBy($request->get('sort_field') ?? 'id', $request->get('sort_type') ?? 'desc')
            ->paginate($request->get('limit') ?? 10);

        return response($entities);

    }


    public function initShow($id, Request $request)
    {
        $result = Content::find($id);

        if ($result) {

            return response([
                'title' 		=>  $result->title,
                'heading' 		=>  $result->heading ?? '',
                'is_blog' 		=>  $result->is_blog ?? 0,
                'is_private' 	=>  $result->is_private ?? 0,
                'is_popup' 		=>  $result->is_popup ?? 0,
            ]);
        }

        return response()->json(['status' => false, 'msg' => 'request is invalid'], 200);
    }


    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function initUpdate($id, Request $request)
    {

        if ($request->get('is_blog') && $request->get('is_popup')) {
            return response()->json(['status' => false, 'msg' => 'مطالب وبلاگ نمیتواند به صورت پاپ آپ باشد']);
        }


        $result = Content::updateOrCreate(['id' => $id] ,[
            'title' =>  $request->get('title'),
            'heading' =>  $request->get('heading') ?? '',
            'is_blog' =>  $request->get('is_blog') ?? 0,
            'is_private' =>  $request->get('is_private') ?? 0,
            'is_popup' =>  $request->get('is_popup') ?? 0,
        ]);


        if ($result) {
            return response()->json(['status' => true, 'msg' => 'عملیات موفقیت امیز بود.', 'model' => $result], 200);
        }

        return response()->json(['status' => false, 'msg' => 'un success'], 200);


    }

    public function initStore(Request $request)
    {

		$validator = Validator::make($request->all(), [
			'title' 						=> 'required',
			'related'						=> 'array|min:1',
			'h2_titles'						=> 'array|min:1',
			'related.*'						=> 'array|min:1',
			'related.*.blog_id'				=> 'numeric|required_with:related.*|exists:content,id',
		]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'msg'  =>  $validator->errors()->first()]);
        }

		$fields = $validator->validated();


        $slug = Content::where('slug', remove_special_char($request->get('title')))->count();
        if ($slug > 0) {
            return Response()->json(['status' => false, 'msg' => 'اسلاگ قبلا ثبت شده است.']);
        }

        $model = Content::firstOrCreate([
            'created_by' 		=> Auth::id(),
            'title' 			=> $request->get('title'),
            'heading' 			=> $request->get('heading'),
            'slug' 				=> $request->get('slug') == '' ? remove_special_char($request->get('title')) : remove_special_char($request->get('slug')),
            'meta_title' 		=> $request->get('meta_title') ?? $request->get('title'),
            'meta_description' 	=> $request->get('meta_description') ?? $request->get('title'),
            'is_blog' 			=> $request->get('is_blog') ?? 0,
            'h2_titles' 		=> json_encode($fields['h2_titles'] ?? []),
            'is_private' 		=> $request->get('is_private') ?? 0,
            'is_popup' 			=> $request->get('is_popup') ?? 0,
        ]);

		if (isset($fields['related'])){
			$related = [];
			foreach ($fields['related'] as $item){
				$related[] = [
					'blog_id' 			=> $model->id,
					'related_blog_id' 	=> $item['blog_id'],
				];
			}
			RelatedContent::insert($related);
		}


        if ($model) {
            return response(['status' => true, 'model' => $model]);
        }

    }




    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function status($id, Request $request)
    {

        $model = Content::find($id);

        if ($request->get('field') == 'status') {

            $model = $model->update([
                'status' => $model->status ? 0 : 1
            ]);
        }

        if ($model) {

            return Response()->json(['status' => true, 'msg' => 'عملیات موفقیت آمیز بود.']);
        }
        return Response()->json(['status' => false, 'msg' => 'خطایی رخ داده است.']);
    }
}
