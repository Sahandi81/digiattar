<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWalletTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallet', function (Blueprint $table) {
            $table->integer('marketer_id');
            $table->integer('period_id');
            $table->enum('type', ['1', '2', '3', '4', '5', '6', 'B', 'C', 'D', 'E'])->comment('توی دبلیو ها پول هست و توی بقیه فروش');
            $table->integer('period_usage')->nullable()->index('period_usage');
            $table->bigInteger('amount')->default(0);
            $table->boolean('status')->default(true);
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->softDeletes();

            $table->primary(['marketer_id', 'period_id', 'type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wallet');
    }
}
