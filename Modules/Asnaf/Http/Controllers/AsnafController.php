<?php

namespace Modules\Asnaf\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Log;
use Modules\Asnaf\Entities\AsnafBranch;
use Modules\Asnaf\Entities\AsnafCity;
use Modules\Asnaf\Entities\AsnafConference;
use Modules\Asnaf\Entities\AsnafProduct;
use Modules\Asnaf\Entities\AsnafProvince;

class AsnafController extends Controller
{


    public function branchList(Request $request)
    {
        $entities = AsnafBranch::with(['cityAsnaf'])->orderBy($request->get('sort_field') ?? 'id', $request->get('sort_type') ?? 'desc')
            ->paginate($request->get('limit') ?? 100);

        return response($entities);
    }


    public function productList(Request $request)
    {
        $entities = AsnafProduct::with(['product'])->where(function ($q) use($request) {

            if ($request->get('product_id') && $request->get('product_id') != "") {
                $q->where('product_id', $request->get('product_id'));
            }

        })->orderBy($request->get('sort_field') ?? 'id', $request->get('sort_type') ?? 'desc')
            ->paginate($request->get('limit') ?? 100);

        return response($entities);
    }



    public function branchStore(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            "city" => 'required',
            "branch_code" => 'required',
            "manager_name" => 'required',
            "manager_mobile" => 'required',
            "manager_nationalcode" => 'required',
            "postal_code" => 'required',
            "address" => 'required',
        ]);

        if ( $validator->fails() ) {
            return Response()->json(['status' => false, 'msg' => $validator->errors()->first()]);
        }

        $arr = [
            "password" => 'Aa@3392@Aa@3023',
            "companyId" => 24,
            "branchCode" => $request->get('branch_code'),
            "province" => AsnafCity::where('id', $request->get('city'))->value('province_id'),
            "city" => $request->get('city'),
            "managerName" => $request->get('manager_name'),
            "mobile" => $request->get('manager_mobile'),
            "nationalId" => $request->get('manager_nationalcode'),
            "postalCode" => $request->get('postal_code'),
            "address" => $request->get('address'),
            "nationalCardCopyFileName" => "",
            "identificationCopyFileName" => "",
            "obligationFormFileName" => "",
            "establishFormFileName" => "",
            "rentalContractFileName" => "",
            "officialNewspaperFileName" => "",
            "nationalCardCopy" => "",
            "identificationCopy" => "",
            "obligationForm" => "",
            "establishForm" => "",
            "rentalContract" => "",
            "officialNewspaper" => "",

        ];

        foreach ($request->get('files') as $key=>$file) {
            foreach ($file as $k=>$value) {
                $arr[$key . 'FileName'] .= $value['file_name'] . ((count($file) - 1 != $k) ? '|' : '');
                preg_match('/data:image\/jpeg;base64,(.*)/', $value['uploaded_file'], $matches);
                $arr[$key] .= $matches[1] . ((count($file) - 1 != $k) ? '|' : '');
            }

        }

            $client = new \SoapClient('http://easnaf.mimt.sis-eg.com/fa/index.php?module=cdk&func=loadmodule&system=cdk&sismodule=user/call_function.php&ctp_id=62&func_name=wsrvTypeWSDL&type_name=mim_branch_issuance_change&wsServiceTyps=soap', ['encoding' => 'UTF-8']);
            $result = $client->__soapCall('branchIssuance', $arr);

            $result = json_decode($result);

            if ($result->status) {

            AsnafBranch::create([
                "city" => $request->get('city'),
                "branch_code" => $request->get('branch_code'),
                "manager_name" => $request->get('manager_name'),
                "manager_mobile" => $request->get('manager_mobile'),
                "manager_nationalcode" => $request->get('manager_nationalcode'),
                "postal_code" => $request->get('postal_code'),
                "address" => $request->get('address'),
                'request_id' => $result->requestId,
                'message' => $result->message,
                'status' => $result->status
            ]);

                return response(['status' => true, 'msg' => $result->message]);
            } else {
                return response(['status' => false, 'msg' => $result->message]);
            }

    }


    public function productStore(Request $request)
    {

        ini_set('upload_max_filesize', '60M');
        ini_set('max_execution_time', '999');
        ini_set('memory_limit', '2048M');
        ini_set('post_max_size', '60M');

        $validator = \Validator::make($request->all(), [
            'product_id' => 'required',
        ]);

        if ( $validator->fails() ) {
            return Response()->json(['status' => false, 'msg' => $validator->errors()->first()]);
        }


        $product = \Modules\Product\Entities\Product::find($request->get('product_id'));


        $arr = [
            "password" => 'Aa@3392@Aa@3023',
            "companyId" => 24,
            'title' => $product->title,
            "iranCode" => '6260667803176',
            "userName" => '466989742',
            "userEmail" => 'info@bee.ir',
            "registrationDate" => date('Y-m-d'),
            "brandOwnerdocFileName" => '',
            "businessLicenseFileName" => '',
            "healthLicensingFileName" => '',
            "costEstimationFileName" => '',
            "commitmentFileName" => '',
            "catalogueFileName" => '',
            "factorFileName" => '',
            "otherDocumentFileName" => '',
            "brandOwnerdoc" => '',
            "businessLicense" => '',
            "healthLicensing" => '',
            "costEstimation" => '',
            "commitment" => '',
            "catalogue" => '',
            "factor" => '',
            "otherDocument" => '',

        ];



        foreach ($request->get('files') as $key=>$file) {
            foreach ($file as $k=>$value) {
                $arr[$key . 'FileName'] .= $value['file_name'] . ((count($file) - 1 != $k) ? '|' : '');
                preg_match('/data:image\/jpeg;base64,(.*)/', $value['uploaded_file'], $matches);
                $arr[$key] .= $matches[1] . ((count($file) - 1 != $k) ? '|' : '');
            }
        }


        $client = new \SoapClient('http://easnaf.mimt.sis-eg.com/fa/index.php?module=cdk&func=loadmodule&system=cdk&sismodule=user/call_function.php&ctp_id=62&func_name=wsrvTypeWSDL&type_name=mim_product&wsServiceTyps=soap', ['encoding' => 'UTF-8']);
        $result = $client->__soapCall('product', $arr);

        $result = json_decode($result);

        \Modules\Asnaf\Entities\AsnafProduct::create([
            'product_id' => $request->get('product_id'),
            'request_id' => $result->requestId,
            'message' => $result->message,
            'status' => $result->status
        ]);

        if ($result->status == 1) {
            return response(['status' => true, 'msg' => $result->message]);
        } else {
            return response(['status' => false, 'msg' => $result->message]);
        }
    }
}
