<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Modules\Setting\Entities\Config;
use Modules\User\Entities\Role;
use Lcobucci\JWT\Parser;

class LoginController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
            'captcha' => 'required'
        ]);

        if ( $validator->fails() ) {
            return Response()->json(['status' => false, 'msg' => $validator->errors()->first()]);
        }


        // fetch user with domain
        if(Auth::attempt(['mobile' => $request->get('username'), 'password' => $request->get('password')]) || Auth::attempt(['email' => $request->get('username'), 'password' => $request->get('password')])) {

            $user = $request->user();

            if ( ! $user->role->admin_access ) {
                return response()->json([
                    'status' => false,
                    'msg' => 'به این بخش دسترسی ندارید'
                ], 403);
            }

            if ( ! $user->status ) {
                return response()->json([
                    'status' => false,
                    'msg' => 'کاربر غیرفعال است.'
                ], 200);
            }

            if ( ! $user->role->full_access ) {
                $status = Config::where('id', 'admin')->value('status');
                if ( ! $status ) {
                    return response()->json([
                        'status' => false,
                        'msg' => 'دسترسی به پنل مدیریت موقتا بسته شده است.'
                    ], 200);
                }
            }


            // create token
            $token = $user->createToken('Token Name')->accessToken;


            return response()->json([
                'status' => true,
                'token' => $token,
                'user' => [
                    'name' => $user->name . ' ' . $user->family,
                    'mobile' => $user->mobile,
                    'email' => $user->email,
                    'role' => Role::find($user->role_id),
                    'permissions' => Role::roleHasPermissions($user->role_id, false)
                ],
            ]);

        } else {
            return response()->json([
                'status' => false,
                'msg' => 'نام کاربری و کلمه عبور اشتباه است.'
            ], 200);
        }



    }

    public function logout(Request $request)
    {

        $request->user()->token()->revoke();

        return response(['status' => true]);
    }

}
