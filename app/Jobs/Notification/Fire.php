<?php

namespace App\Jobs\Notification;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Modules\Notification\Entities\Notification;
use Modules\User\Entities\User;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class Fire implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $notification;

    public function __construct(Notification $notification)
    {
//        \Modules\Notification\Entities\Notification::find($this->notification->id)
//            ->update([
//                'status' => 2
//            ]);
        $this->notification = $notification;
    }


    /**
     *
     */
    public function handle()
    {
        $user = User::find($this->notification->user_id);

        if (!$user->mobile || strlen($user->mobile) > 11 || $user->mobile == '11111111111' || $user->mobile == '00000000000' || $user->mobile == '99999999999') {
            $this->notification->update([
                'status' => 2,
                'error_log' => 'موبایل نامعتبر است'
            ]);
        } else {

            try {

                $client = new Client();
                $message_text = \Illuminate\Support\Facades\DB::select('select fn_make_sms_text(?)', [
                    $this->notification['template'],
                    json_encode(array(
                        isset($this->notification['token']) ? $this->notification['token'] : "",
                        isset($this->notification['token2']) ? $this->notification['token2'] : "",
                        isset($this->notification['token3']) ? $this->notification['token3'] : "",
                        isset($this->notification['token10']) ? $this->notification['token10'] : "",
                        isset($this->notification['token20']) ? $this->notification['token20'] : "",
                    ))
                ]);
                $response = $client->get("https://api.kavenegar.com/v1/".env('KAVENEGAR_API_KEY')."/sms/send.json", [
                    "query" => [
                        "receptor" => convert2english($user->mobile),
                        "message" => $message_text,
                        "sender" => 1000596446,
                    ]
                ]);
                if ($response->getStatusCode() == 200) {

                    $body = json_decode($response->getBody()->getContents());

                    if ($body->return->status == 200) {

                        $this->notification->update([
                            'status' => 1,
                            'text' => $body->entries[0]->message
                        ]);

                    } else {
                        $this->notification->update([
                            'status' => 2,
                            'err' => $body->return->message
                        ]);
                    }
                } else {
                    $this->notification->update([
                        'status' => 2,
                        'error_log' => "client status code is :" . $response->getStatusCode()
                    ]);
                }
            } catch (\Exception $exception) {

                $logger = new Logger('notification');
                $logger->pushHandler(new StreamHandler(storage_path().'/logs/notification.log'));
                $logger->error('notificationId error' . $this->notification->id . ' with message ' . $exception->getMessage());

                $this->notification->update([
                    'status' => 2,
                    'error_log' => $exception->getMessage()
                ]);
            }
        }
    }
}
