<?php

namespace App\Jobs\Marketer;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Modules\Marketer\Entities\Info;
use Modules\Marketer\Entities\Marketer;
use Modules\User\Entities\Log;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class MarketerCode implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    private $marketer;

    /**
     * MarketerCode constructor.
     * @param Info $marketer
     */
    public function __construct(Info $marketer)
    {
        $this->marketer = $marketer;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $result = Marketer::getMarketerCode($this->marketer->marketer_id);

        if (!$result['status']) {

            $logger = new Logger('marketer');
            $logger->pushHandler(new StreamHandler(storage_path().'/logs/marketer.log'));
            $logger->error('marketer error' . $this->marketer->marketer_id . ' with message ' . $result['msg']);
        }

    }
}
