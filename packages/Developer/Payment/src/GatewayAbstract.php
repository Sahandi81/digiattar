<?php

namespace Developer\Payment;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

abstract class GatewayAbstract
{

    protected $amount;

    protected $callback;

    protected $description;

    protected $gateway;

    protected $transaction;

    protected $refId;

    protected $parameter;

    protected $invoice;

    function getTimeId()
    {
        $genuid = function() {
            return substr(str_pad(str_replace('.','', microtime(true)),12,0),0,12);
        };

        $uid = $genuid();

        while (DB::table('transaction')->find($uid)) {
            $uid = $genuid();
        }

        return $uid;
    }

    protected function ready()
    {

        $id = $this->getTimeId();

        DB::table('transaction')->insert([
            'id' => $id,
            'user_id' => Auth::id(),
            'transactionable_id' => $this->invoice,
            'amount' => $this->gateway == 'zarinpal' ? $this->amount : $this->amount / 10,
            'gateway' => $this->gateway,
            'description' => $this->description,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);


        $this->transaction = DB::table('transaction')->find($id);

        return $this->transaction;

    }

    protected function transactionSucceed($transaction)
    {
        $transaction->payment_date = Carbon::now();
        $transaction->ref_id = $this->refId;
        $transaction->status = 'success';
        $transaction->data = json_encode($this->parameter); // store bank response
        $transaction->save();

        return $transaction;
    }

    protected function transactionFailed($transaction)
    {
        $transaction->status = 'failed';
        $transaction->data = json_encode($this->parameter); // store bank response
        $transaction->save();

        return $transaction;
    }

}
?>
