<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('region', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('title', 50);
            $table->integer('parent_id')->nullable()->index('parent_id');
            $table->integer('_lft')->index('lft');
            $table->integer('_rgt')->index('rgt');
            $table->boolean('status')->default(true);
            $table->boolean('is_posting')->default(true);
            $table->boolean('is_bearing')->default(false);
            $table->boolean('is_delivery')->default(false);
            $table->boolean('in_person')->default(false);
            $table->dateTime('created_at')->nullable();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->softDeletes();

            $table->index(['_lft', '_rgt'], 'idx_left_right_together');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('region');
    }
}
