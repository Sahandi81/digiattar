<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_types', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->string('product_id', 45)->index('product_id');
            $table->string('title')->nullable();
            $table->unsignedInteger('count')->default('0');
            $table->integer('per_count')->default(1)->comment('bulk or counterable unit');
            $table->integer('price')->default(0);
            $table->integer('off_price')->default(0);
            $table->integer('discount')->default(0);
            $table->integer('tax')->default(0);
            $table->integer('weight')->default(0);
            $table->integer('order_point')->default(0);
            $table->float('point', 10, 0)->default(0);
            $table->boolean('status')->default(true);
            $table->dateTime('created_at')->nullable();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->integer('sales_number')->default(0);
            $table->string('product_typescol', 45)->nullable();
            $table->integer('package_count')->nullable()->default(0);
            $table->string('product_typescol1', 45)->nullable();
            $table->integer('max_purchase')->nullable()->default(5);
            $table->string('product_typescol2', 45)->nullable();
            $table->timestamp('deleted-at')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_types');
    }
}
