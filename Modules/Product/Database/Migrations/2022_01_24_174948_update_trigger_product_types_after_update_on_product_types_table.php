<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTriggerProductTypesAfterUpdateOnProductTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

		DB::select("DROP TRIGGER IF EXISTS `trigger_product_types_after_update`");
		DB::select("
		CREATE DEFINER = CURRENT_USER
 		TRIGGER trigger_product_types_after_update	
		AFTER UPDATE	
		ON product_types 
		FOR EACH ROW
		BEGIN

			SET @number = digiattar.fn_product_type_count_number(NEW.id);
		
		
			if old.count <> new.count THEN
			
					if @number <= 3 THEN
						INSERT INTO notification(notification.user_id,notification.template,notification.token1,notification.token4)
						VALUES (12,'DGInventoryAlert',@number,NEW.title);
					end if;
					
					IF OLD.count < 1 THEN
						CALL sp_store_reminder_sms(NEW.product_id, NEW.title);
					END IF;
			END if;

		END
		");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('', function (Blueprint $table) {

        });
    }
}
