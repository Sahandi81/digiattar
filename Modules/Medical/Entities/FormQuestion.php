<?php

namespace Modules\Medical\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class FormQuestion extends Model
{
    use HasFactory;

    protected $fillable = [
		'question_text'
	];

	protected $hidden = [
		'category_id',
		'category',  # relation
	];

	protected $guarded = [

	];


	public function category(): \Illuminate\Database\Eloquent\Relations\HasOne
	{
		return $this->hasOne(QuestionCategory::class, 'id', 'category_id');
	}

}
