<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePeriodTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('period', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('parent_id')->nullable();
            $table->string('title', 100)->default('');
            $table->dateTime('from_date');
            $table->dateTime('to_date');
            $table->boolean('status')->default(false)->index('status');
            $table->boolean('hidden')->default(false)->index('hidden');
            $table->enum('type', ['continuous', 'conclusion'])->default('continuous');
            $table->boolean('calculated')->default(true);
            $table->dateTime('created_at')->nullable();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->softDeletes();

            $table->index(['from_date', 'to_date'], 'from_date_to_date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('period');
    }
}
