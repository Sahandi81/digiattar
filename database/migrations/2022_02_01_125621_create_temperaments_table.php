<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTemperamentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temperaments', function (Blueprint $table) {
            $table->integer('temperament_id', true);
            $table->string('temperament_title', 50);
            $table->string('temperament_message', 5000);
            $table->string('temperament_description', 5000);
            $table->integer('temperament_cold_or_hot')->nullable();
            $table->integer('temperament_dry_or_more')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temperaments');
    }
}
