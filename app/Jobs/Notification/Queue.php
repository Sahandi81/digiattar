<?php

namespace App\Jobs\Notification;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Modules\Notification\Entities\Notification;
use Modules\User\Entities\User;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class Queue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    private $user;

    private $params;


    public function __construct(User $user, $params = [])
    {
        $this->user = $user;
        $this->params = $params;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $model = Notification::create([
                'user_id' => $this->user->id,
                "template" => isset($this->params['template']) ? $this->params['template'] : "",
                "token" => isset($this->params['token']) ? $this->params['token'] : "",
                "token2" => isset($this->params['token2']) ? $this->params['token2'] : "",
                "token3" => isset($this->params['token3']) ? $this->params['token3'] : "",
                "token10" => isset($this->params['token10']) ? $this->params['token10'] : "",
                "token20" => isset($this->params['token20']) ? $this->params['token20'] : "",
            ]);

            if ($model) {
                dispatch(new Fire($model))->onQueue('notification');
            }

        } catch (\Exception $exception) {

            $logger = new Logger('notification');
            $logger->pushHandler(new StreamHandler(storage_path().'/logs/notification.log'));
            $logger->error('notification error in file queue.php with message: '. $exception->getMessage());

        }


    }
}

