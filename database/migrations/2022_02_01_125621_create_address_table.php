<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->integer('user_id')->index('user_id');
            $table->integer('region_id')->index('region_id');
            $table->decimal('lat', 9, 6)->nullable();
            $table->decimal('lng', 9, 6)->nullable();
            $table->string('postal_code', 50)->nullable();
            $table->string('main');
            $table->string('reciver_name', 50);
            $table->string('reciver_mobile', 20)->nullable();
            $table->string('reciver_national_code', 50)->nullable();
            $table->boolean('status')->default(true);
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->string('token', 50)->unique('token');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('address');
    }
}
