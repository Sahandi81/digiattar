<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('fileable_id')->index('fileable_id');
            $table->string('fileable_type', 50)->index('fileable_type');
            $table->unsignedBigInteger('created_by')->index('FK_file_users');
            $table->enum('mime_type', ['image', 'video'])->default('image');
            $table->string('directory');
            $table->string('file', 100);
            $table->boolean('collection')->default(true);
            $table->tinyInteger('order')->default(1);
            $table->string('size', 100)->nullable();
            $table->string('link')->nullable();
            $table->string('caption')->nullable();
            $table->dateTime('created_at');
            $table->timestamp('updated_at')->useCurrentOnUpdate()->useCurrent();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file');
    }
}
