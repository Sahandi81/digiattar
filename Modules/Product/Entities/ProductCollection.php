<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ProductCollection extends Model
{
	# New update, use `rebuild` api to change columns from deleted to deleted_at
	use SoftDeletes;
    use HasFactory;

    protected $table = 'product_collection';

    protected $fillable = [];

    protected static function newFactory()
    {
        return \Modules\Product\Database\factories\ProductCollectionFactory::new();
    }

    public function types(){
        return $this->hasMany(Types::class,'product_collection_id');
    }

    public function product(){
        return $this->hasOne(Product::class,'id','product_id');
    }
}
