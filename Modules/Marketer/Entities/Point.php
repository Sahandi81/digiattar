<?php

namespace Modules\Marketer\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Period\Entities\Period;
use Modules\User\Entities\User;

class Point extends Model
{
	# New update, use `rebuild` api to change columns from deleted to deleted_at
	use SoftDeletes;
    use HasFactory;

    protected $table = 'marketer_point';

    protected static function newFactory()
    {
        return \Modules\Marketer\Database\factories\PointFactory::new();
    }


    public function marketer()
    {
        return $this->belongsTo(Marketer::class, 'marketer_id');

    }

    public function user()
    {
        return $this->belongsTo(User::class, 'marketer_id');
    }

    public function period()
    {
        return $this->belongsTo(Period::class, 'period_id');
    }
}
