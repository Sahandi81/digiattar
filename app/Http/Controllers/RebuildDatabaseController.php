<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

# it's not important, So I will not develop this properly
class RebuildDatabaseController extends Controller
{

	protected $db = [];
	protected $rmColumn;
	protected $rpColumn;
	protected $emEqualTo = 1;
	public $ignore	= [
		'product_view',
	];

	public function __construct(Request $request)
	{
		$fields = $request->validate([
			'rm_column'			=> 'string',
			'rp_column'			=> 'string',
			'rm_equal_to'		=> 'string',
		]);
		$this->db 		= $this->getTables();
		$this->rmColumn = $fields['rm_column'] ?? null;
		$this->rpColumn = $fields['rp_column'] ?? null;
		isset($fields['rm_equal_to']) ? $this->emEqualTo = $fields['rm_equal_to'] : null;
	}

	public function getTables(): array
	{
		return [
			# DB Name
			'database' =>DB::connection()->getDatabaseName(),
			# Tables
			'tables' =>DB::select('SHOW TABLES'),
		];
	}


	public function convertArabicCharToPersian()
	{

		try {
			# u can remove it. just for show tables
			echo '<pre>';
			foreach($this->db['tables'] as $table)
			{
				$tableName 	= ($table->{'Tables_in_' . $this->db['database']});
				$columns 	= Schema::getColumnListing($tableName);
				echo $tableName .' '. '[' . "\n";
				foreach ($columns as $column) {
					echo "\t" . $column . "\n";
					DB::select("update $tableName set $tableName.$column=REPLACE($tableName.$column,'ي',N'ی')");
					DB::select("update $tableName set $tableName.$column=REPLACE($tableName.$column,'ك',N'ک')");
					DB::select("update $tableName set $tableName.$column=REPLACE($tableName.$column,'ة',N'ه')");
				}
				echo ']' . "\n";
			}
			return response(['status' => true]);

		} catch (\PDOException $exception){

			return response([
				'status' 	=> false,
				# remove this lines after release
				'line'	 	=> $exception->getLine(),
				'file'	 	=> $exception->getFile(),
				'msg'	 	=> $exception->getMessage(),
			]);
		}
	}


	public function updateDeletedColumns()
	{
		try {
			$counter = 0;
			foreach($this->db['tables'] as $table) {

				$tableName 	= ($table->{'Tables_in_' . $this->db['database']});
				$columns 	= Schema::getColumnListing($tableName);
				if (in_array($this->rmColumn, $columns) && !in_array($this->rpColumn, $columns)){
					$endCol = end($columns);
					DB::select("ALTER TABLE `{$tableName}` ADD `{$this->rpColumn}` TIMESTAMP NULL AFTER `{$endCol}`;");
					DB::select("UPDATE `{$tableName}` SET `{$this->rpColumn}` = NOW() where `{$this->rmColumn}` = {$this->emEqualTo};");
					DB::select("ALTER TABLE `{$tableName}` DROP `{$this->rmColumn}`;");
					$counter++;
				}

			}
			return ['status' => 'OK', 'msg' => $counter .' '. 'table effected.'];

		} catch (\PDOException $exception){

			return response([
				'status' 	=> false,
				# remove this lines after release
				'line'	 	=> $exception->getLine(),
				'file'	 	=> $exception->getFile(),
				'msg'	 	=> $exception->getMessage(),
			]);
		}
	}

	public function addDeletedAt()
	{
		try {
			$counter = 0;
			foreach($this->db['tables'] as $table) {
				$tableName 	= ($table->{'Tables_in_' . $this->db['database']});
				$columns 	= Schema::getColumnListing($tableName);
				if (!in_array($this->rpColumn, $columns) && !in_array($tableName,$this->ignore)){
					$endCol = end($columns);
					DB::select("ALTER TABLE `{$tableName}` ADD `{$this->rpColumn}` TIMESTAMP NULL AFTER `{$endCol}`;");
					$counter++;
				}
			}
			return ['status' => 'OK', 'msg' => $counter .' '. 'table effected.'];

		} catch (\PDOException $exception){

			return response([
				'status' 	=> false,
				# remove this lines after release
				'line'	 	=> $exception->getLine(),
				'file'	 	=> $exception->getFile(),
				'msg'	 	=> $exception->getMessage(),
			]);
		}
	}

}

