<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductCategoryAssignableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_category_assignable', function (Blueprint $table) {
            $table->integer('category_id');
            $table->integer('assignable_id');
            $table->enum('type', ['brand', 'attribute', 'filter'])->default('brand');
            $table->tinyInteger('sort')->default(0);
            $table->softDeletes();

            $table->primary(['category_id', 'assignable_id', 'type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_category_assignable');
    }
}
