<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\User\Entities\User;

class ProductComment extends Model
{
	# New update, use `rebuild` api to change columns from deleted to deleted_at
	use SoftDeletes;
    use HasFactory;

    protected $fillable = [];

    protected static function newFactory()
    {
        return \Modules\Product\Database\factories\ProductCommentFactory::new();
    }

    public function user(){
        return $this->hasMany(User::class,'id','user_id');
    }

    public function showUser(){
        return $this->hasMany(User::class,'id','show_name');
    }
}
