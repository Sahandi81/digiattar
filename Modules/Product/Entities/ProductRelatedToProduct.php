<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ProductRelatedToProduct extends Model
{
    use HasFactory;

    public $timestamps = false;

	protected $fillable = [
		'product_id',
		'related_product_id',
		'product_type_id',
	];

    public function asignedProduct(){
        return $this->hasMany(Product::class,'id','related_product_id');
    }

}
