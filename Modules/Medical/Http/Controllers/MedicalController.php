<?php

namespace Modules\Medical\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Modules\Medical\Entities\MedicalFolder;
use Modules\Medical\Entities\MedicalRecords;
use Modules\PDF\Http\Controllers\PDFController;
use Modules\User\Entities\User;

class MedicalController extends Controller
{

	public function recordsIndex(): \Illuminate\Http\JsonResponse
	{
		$records = User::find(Auth::id())->records()->get();
		if ($records)
			return Response::json($records);
		else
			return Response::json(['status' => false, 'msg' => 'ریکوردی یافت نشد.'], 404);
	}

	public function recordShow(MedicalRecords $medicalRecords): \Illuminate\Http\JsonResponse
	{
		if ($medicalRecords->getAttribute('user_id') !== Auth::id())
			return Response::json(['status' => false, 'msg' => 'شما دسترسی به این ریکورد را ندارید.']);

		return Response::json($medicalRecords->fresh('folders'));
	}

	public function folderShow($medical_records, MedicalFolder $medicalFolder): \Illuminate\Http\JsonResponse
	{
		if ($medicalFolder->getAttribute('user_id') !== Auth::id()) # I can use validation on parent table here BUT maybe someday we want to save folders without parent.
			return Response::json(['status' => false, 'msg' => 'شما دسترسی به این پرونده را ندارید.']);

		return Response::json($medicalFolder->fresh(['forms' => function($q) {
			$q->select('answer','fq.question_text', 'folder_id')
				->leftJoin('form_questions as fq', 'fq.id', '=', 'form_question_id')
			;
		}]));
	}

	public function downloadFrom($medical_records, MedicalFolder $medicalFolder): \Illuminate\Http\Response
	{
		$form = $this->folderShow($medical_records, $medicalFolder);
		$form = json_decode($form->getContent());
		return PDFController::create($form->forms);
	}

}
