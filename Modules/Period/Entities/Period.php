<?php

namespace Modules\Period\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Period extends Model
{
	# New update, use `rebuild` api to change columns from deleted to deleted_at
	use SoftDeletes;
    use HasFactory;

    protected $guarded = [];

    protected $table = 'period';

    protected $primaryKey = 'id';

    protected static function newFactory()
    {
        return \Modules\Period\Database\factories\PeriodFactory::new();
    }


    public function month()
    {
        return $this->belongsTo(Month::class, 'parent_id');
    }
}

