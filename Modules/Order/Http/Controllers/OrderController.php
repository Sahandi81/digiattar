<?php

namespace Modules\Order\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Modules\Order\Entities\Note;
use Modules\Order\Entities\Order;
use Modules\Product\Entities\PackageType;
use Modules\Product\Entities\Pins;
use Modules\Product\Entities\PriceParameter;
use Modules\Product\Entities\Product;
use Modules\Region\Entities\Region;
use PDOException;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $entities = Order::withCount('notes')->with(['user' => function($user) {
            $user->with(['info', 'marketer']);
        }, 'period' => function($p) {
            $p->with(['month']);
        }, 'address' => function($a) {
            return $a->select('id', 'region_id', DB::raw('fn_make_region(region_id) as region_title'));
        }, 'transaction'])
			->where(function ($q) use($request) {

            if ($request->get('notes') != -1) {
                if ($request->get('notes') == 1) {
                    $q->WhereHas('notes');
                } else {
                    $q->whereDoesntHave('notes');
                }
            }

            if ($request->get('gateway') && $request->get('gateway') != "") {
                $q->whereHas('transaction', function ($transaction) use($request) {
                    $transaction->where('gateway', $request->get('gateway'));
                });
            }

            if ($request->get('region_id') && $request->get('region_id') != "") {
                $q->whereHas('address', function ($address) use($request) {
                    $output = [];
                    foreach (Region::select('id')->descendantsAndSelf($request->get('region_id'))->toArray() as $item) {
                        $output[] = $item['id'];
                    }

                    $address->whereIn('region_id', $output);
                });
            }


            if ($request->get('month_id') && $request->get('month_id') != "") {
                $q->whereHas('period', function ($period) use($request) {
                    $period->where('parent_id', $request->get('month_id'));
                });
            }

            if ($request->get('period_id') && $request->get('period_id') != "") {
                $q->where('period_id', $request->get('period_id'));
            }

            if ($request->get('id') && $request->get('id') != "") {
                $q->whereIn('id', explode(',',$request->get('id')));
            }
            if ($request->get('user_id') && $request->get('user_id') != "") {
                $q->where('user_id', $request->get('user_id'));
            }
            if ($request->get('status') != -1) {
                $q->where('status', $request->get('status'));
            }else{
				$q->where('status', '!=', 0);
			}

            if ($request->get('transport') && $request->get('transport') != -1) {
                $q->where('transport', $request->get('transport'));
            }

            if ($request->get('delivery') && $request->get('delivery') != -1) {
                $q->where('delivery', $request->get('delivery'));
            }

            if ($request->get('returned') && $request->get('returned') != -1) {
                $q->where('returned', $request->get('returned'));
            }

            if ($request->get("private") && $request->get("private") != -1) {
                $q->where("private", $request->get("private"));
            }

            if ($request->get("is_voucher") && $request->get("is_voucher") != -1) {
                $q->where("is_voucher", $request->get("is_voucher"));
            }

            if ($request->get("is_wallet") && $request->get("is_wallet") != -1) {
                $q->where("is_wallet", $request->get("is_wallet"));
            }
            if ($request->get("paid") && $request->get("paid") != -1) {
                $q->where("paid", $request->get("paid"));
            }

            if ($request->get('mobile') && $request->get('mobile') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where('mobile', 'like', '%' . $request->get('mobile') . '%');
                });
            }

            if ($request->get('email') && $request->get('email') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where('email', 'like', '%' . $request->get('email') . '%');
                });
            }

            if ($request->get('username') && $request->get('username') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where('username', 'like', '%' . $request->get('username') . '%');
                });
            }

            if ($request->get('name') && $request->get('name') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->where(DB::raw('concat(name, " ", family)'), 'like', '%' . $request->get('name') . '%');
                });
            }

            if ($request->get('national_code') && $request->get('national_code') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->whereHas('info', function ($info) use($request) {
                        $info->where('national_code', 'like', '%' . $request->get('national_code') . '%');
                    });
                });
            }

            if ($request->get('parent_id') && $request->get('parent_id') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->whereHas('marketer', function ($marketer) use($request) {
                        $marketer->where('ancestry', 'like', DB::table('marketer')->where('id', $request->get('parent_id'))->value('ancestry') . '%');
                    });
                });
            }


            if ($request->get('marketer_code') && $request->get('marketer_code') != '') {
                $q->whereHas('user', function ($user) use($request) {
                    $user->whereHas('marketer', function ($marketer) use($request) {
                        $marketer->where('marketer_code', 'like', '%' . $request->get('marketer_code') . '%');
                    });
                });
            }

            if ($request->get('from_date') && $request->get('to_date') && $request->get('from_date') != "" && $request->get('to_date') != "") {
                $q->where('created_at', '>=', $request->get('from_date'));
                $q->where('created_at', '<=', $request->get('to_date'));
            } elseif ($request->get('from_date')) {
                $q->where('created_at', '>=', $request->get('from_date'));
            } elseif ($request->get('to_date')) {
                $q->where('created_at', '<=', $request->get('to_date'));
            }


            if ($request->get('ids')) { // for excel print or cumulative
                $q->whereIn('id', explode(',', $request->get('ids')));
            }

        })->orderBy($request->get('sort_field') ?? 'id', $request->get('sort_type') ?? 'desc');

        if (($request->has('excel_export') && $request->get('excel_export') == true) || ($request->has('print') && $request->get('print') == true) || ($request->has('cumulative') && $request->get('cumulative') == true)) {
            $entities = $entities->get();
        } else {
            $entities = $entities->paginate($request->get('limit') ?? 100);
        }


        if ($request->has('print') && $request->get('print') == true) {
            $output = [];
            foreach ($entities as $order) {
                $output[] = \Illuminate\Support\Facades\DB::select('call sp_order_info(?)', [$order->id])[0];
            }
            return response($output);
        }


        if ($request->has('cumulative') && $request->get('cumulative') == true) {
            $output = [];

            foreach ($entities as $order) {
                $output[] = $order->id;
            }

            $result = DB::table('order_product_pins as opp')
                ->select('opp.product_types_id', 'p.title as product_title', 'p.code', 'b.title as brand_title', DB::raw('sum(opp.count) as counter'))
                ->leftJoin('product_types as pt', 'pt.id', '=', 'opp.product_types_id')
                ->leftJoin('product as p', 'p.id', '=', 'pt.product_id')
                ->leftJoin('brand as b', 'b.id', '=', 'p.brand_id')
                ->whereIn('opp.order_id', $output)
                ->groupBy('opp.product_types_id', 'p.title', 'p.code', 'b.title')
                ->orderBy('b.id', 'desc')
                ->get();

            return response($result);
        }




        return response($entities);

    }


    public function invoice($id)
    {
        $order = DB::select('call sp_order_info(?)', [$id]);

        return response(['status' => true, 'order' => $order[0]]);
    }

    public function printArrayInvoice($id)
    {
        foreach (explode(',', $id) as $order) {
            $output[] = \Illuminate\Support\Facades\DB::select('call sp_order_info(?)', [$order])[0];
        }

        return response($output);
    }

    public function notes($id)
    {
        $notes = Note::with(['user', 'files'])->where('order_id', $id)->get();

        return response($notes);
    }

    public function addNotes($id, Request $request)
    {

        $result = Note::create([
            'order_id' => $id,
            'user_id' => Auth::id(),
            'note' => $request->get('note')
        ]);



        if ($request->has('file') && $request->get('file') != "") {

            $old = 'attachment/' . $request->get('file');

            $new = 'notes/' . $result->id . '/' . $request->get('file');

            $move = Storage::disk('public')->move($old, $new); // Move Main Image

            if ($move) {
                $result->files()->create([
                    'created_by' => Auth::id(),
                    'file' => $request->get('file'),
                    'collection' => 0,
                    'directory' => 'notes',
                ]);
            }
        }

        if ($result) {
            return response(['status' => true]);
        }

        return response(['status' => false, 'خطا']);
    }


	public function updateStatus(Request $request,$id)
	{
		$fields = $request->validate([
			'status'	=> 'required',
			'condition'	=> 'required|numeric'
		]);

		if (!in_array($fields['status'], ['delivery', 'paid', 'returned', 'transport']))

		try {
			$order = Order::findOrFail($id)->updateOrCreate(
				['id'  => $id]
				,[$fields['status'] => (int)$fields['condition']]);
			return Response::json(['status'	=> true, 'msg' => 'سفارش با موفقیت تغییر وضعیت داده شد.', 'details' => $order]);

		} catch (PDOException $e) {
			# if order exists, check function and triggers
			return response([
				'status'=> false,
				'msg' 	=> 'مشکلی پیش آمده است.',
				'line'	=> __LINE__,
				'file'	=> __FILE__
				,
			], 404);
		}
	}

}
