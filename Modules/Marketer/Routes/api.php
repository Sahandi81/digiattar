<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Modules\User\Entities\User;


Route::prefix('backend')->group(function () {

    Route::prefix('marketers')->middleware(['auth:api', 'admin_access'])->group(function () {

        Route::prefix('contract')->middleware('auth:api')->group(function () {

            Route::get('/', function (Request $request) {

                $entities = \Modules\Marketer\Entities\Contract::with(['marketer', 'user'])
                    ->where(function ($q) use($request) {

                        if ($request->get('status') && $request->get('status') != -1) {
                            $q->where('status', $request->get('status'));
                        }


                        if ($request->get('mobile') && $request->get('mobile') != '') {
                            $q->whereHas('user', function ($user) use($request) {
                                $user->where('mobile', 'like', '%' . $request->get('mobile') . '%');
                            });
                        }

                        if ($request->get('username') && $request->get('username') != '') {
                            $q->whereHas('user', function ($user) use($request) {
                                $user->where('username', 'like', '%' . $request->get('username') . '%');
                            });
                        }

                        if ($request->get('name') && $request->get('name') != '') {
                            $q->whereHas('user', function ($user) use($request) {
                                $user->where(DB::raw('concat(name, " ", family)'), 'like', '%' . $request->get('name') . '%');
                            });
                        }

                        if ($request->get('national_code') && $request->get('national_code') != '') {
                            $q->whereHas('user', function ($user) use($request) {
                                $user->whereHas('info', function ($info) use($request) {
                                    $info->where('national_code', 'like', '%' . $request->get('national_code') . '%');
                                });
                            });
                        }

                        if ($request->get('parent_id') && $request->get('parent_id') != '') {
                            $q->whereHas('marketer', function ($marketer) use($request) {
                                $marketer->where('ancestry', 'like', DB::table('marketer')->where('id', $request->get('parent_id'))->value('ancestry') . '%');
                            });
                        }


                    })
                    ->orderBy('status', 'asc')
                    ->paginate($request->get('limit') ?? 10);

                return response($entities);
            });


            Route::get('/{id}', function ($id) {
                $result = [
                    'name' => '',
                    'national_code' => '',
                    'born' => '',
                    'birth_certificate' => '',
                    'father_name' => '',
                    'mobile' => '',
                    'gov_code' => '',
                    'up_gov_code' => '',
                    'up_name' => '',
                    'address' => '',
                    'postal_code' => '',
                    'shaba' => '',
                    'status' => ''
                ];

                $query = \Illuminate\Support\Facades\DB::table('marketer_contract')->where('marketer_id', $id)->first();

                if ($query) {
                    $result = [
                        'name' => $query->name,
                        'national_code' => $query->national_code,
                        'born' => $query->born,
                        'birth_certificate' => $query->birth_certificate,
                        'father_name' => $query->father_name,
                        'mobile' => $query->mobile,
                        'gov_code' => $query->gov_code,
                        'up_gov_code' => $query->up_gov_code,
                        'up_name' => $query->up_name,
                        'address' => $query->address,
                        'postal_code' => $query->postal_code,
                        'shaba' => $query->shaba,
                        'status' => $query->status,
                    ];
                }

                return response(['status' => true, 'data' => $result]);

            });

            Route::post('/{id}', function ($id, Request $request) {


                $contract = \Illuminate\Support\Facades\DB::table('marketer_contract')
                    ->where('marketer_id', $id)
                    ->first();

                try {
                    if($contract) {
                        \Illuminate\Support\Facades\DB::table('marketer_contract')->where('marketer_id' , $id)->update([
                            'name' => $request->get('name'),
                            'national_code' => $request->get('national_code'),
                            'born' => $request->get('born'),
                            'birth_certificate' => $request->get('birth_certificate'),
                            'father_name' => $request->get('father_name'),
                            'mobile' => $request->get('mobile'),
                            'gov_code' => $request->get('gov_code'),
                            'up_gov_code' => $request->get('up_gov_code'),
                            'up_name' => $request->get('up_name'),
                            'address' => $request->get('address'),
                            'postal_code' => $request->get('postal_code'),
                            'shaba' => $request->get('shaba'),
                        ]);
                    } else {
                        \Illuminate\Support\Facades\DB::table('marketer_contract')->insert([
                            'marketer_id' => $id,
                            'name' => $request->get('name'),
                            'national_code' => $request->get('national_code'),
                            'born' => $request->get('born'),
                            'birth_certificate' => $request->get('birth_certificate'),
                            'father_name' => $request->get('father_name'),
                            'mobile' => $request->get('mobile'),
                            'gov_code' => $request->get('gov_code'),
                            'up_gov_code' => $request->get('up_gov_code'),
                            'up_name' => $request->get('up_name'),
                            'address' => $request->get('address'),
                            'postal_code' => $request->get('postal_code'),
                            'shaba' => $request->get('shaba'),
                            'created_at' => date('Y-m-d H:i:s')
                        ]);
                    }

                    return response(['status' => true, 'msg' => 'عملیات موفقیت آمیز بود']);
                } catch (Exception $exception) {
                    return response(['status' => false, 'msg' => $exception->getMessage()]);
                }

            });

            Route::put('/{id}', function ($id, Request $request) {

                \Illuminate\Support\Facades\DB::table('marketer_contract')
                    ->where('marketer_id', $id)
                    ->update([
                        'status' => $request->get('status')
                    ]);



                return response(['status' => true]);
            });

        });

        Route::prefix('document')->middleware('auth:api')->group(function () {

            Route::get('/', function (Request $request) {

                $entities = \Modules\Marketer\Entities\Document::with(['marketer', 'user'])
                    ->where(function ($q) use($request) {

                        if ($request->get('status') && $request->get('status') != -1) {
                            $q->where('status', $request->get('status'));
                        }

                        if ($request->get('mobile') && $request->get('mobile') != '') {
                            $q->whereHas('user', function ($user) use($request) {
                                $user->where('mobile', 'like', '%' . $request->get('mobile') . '%');
                            });
                        }

                        if ($request->get('username') && $request->get('username') != '') {
                            $q->whereHas('user', function ($user) use($request) {
                                $user->where('username', 'like', '%' . $request->get('username') . '%');
                            });
                        }

                        if ($request->get('name') && $request->get('name') != '') {
                            $q->whereHas('user', function ($user) use($request) {
                                $user->where(DB::raw('concat(name, " ", family)'), 'like', '%' . $request->get('name') . '%');
                            });
                        }

                        if ($request->get('national_code') && $request->get('national_code') != '') {
                            $q->whereHas('user', function ($user) use($request) {
                                $user->whereHas('info', function ($info) use($request) {
                                    $info->where('national_code', 'like', '%' . $request->get('national_code') . '%');
                                });
                            });
                        }

                        if ($request->get('parent_id') && $request->get('parent_id') != '') {
                            $q->whereHas('marketer', function ($marketer) use($request) {
                                $marketer->where('ancestry', 'like',  DB::table('marketer')->where('id', $request->get('parent_id'))->value('ancestry') . '%');
                            });
                        }


                    })
                ->orderBy('status', 'asc')
                    ->paginate($request->get('limit') ?? 10);

                return response($entities);
            });

        });

        Route::prefix('profile')->middleware('auth:api')->group(function () {

            Route::put('/{id}/ancestry', function ($id, Request $request) {

                return response(['status' => false, 'msg' => 'این سرویس توسط برنامه نویس بسته شده است.']);

                $validator = \Validator::make($request->all(), [
                    'position' => 'required',
                    'parent_id' => 'required'
                ]);


                if ($validator->fails()) {
                    return Response()->json(['status' => false, 'msg' => $validator->errors()->first()]);
                }

                $checker = \Illuminate\Support\Facades\DB::table('marketer_refral')
                    ->where('marketer_id', $request->get('parent_id'))
                    ->where('position', $request->get('position'))
                    ->value('direct_id');

                if ($checker) {
                    return response(['status' => false, 'msg' => 'این لاین پر است.']);
                }

                $loop_checker = DB::table('marketer')
                    ->where('id', $request->get('parent_id'))
                    ->value('parent_id');

                if ($loop_checker == $id) {
                    if ($checker) {
                        return response(['status' => false, 'msg' => 'جابه جایی حلقه ای غیر ممکن است.بالاسری جدید دایرکت شخص جابه جاشونده است.']);
                    }
                }

                $marketer_with_sales = DB::table('marketer_point')
                    ->where('marketer_id', $id)
                    ->where('period_id', DB::table('period')
                        ->where('status', 1)
                        ->orderBy('id', 'desc')
                        ->take(1)
                        ->value('id'))
                    ->first();

                if ($marketer_with_sales) {
                    return response(['status' => false, 'msg' => 'امکان جابه جایی کاربری که خرید شخصی و یا گروهی دارد ممکن نیست.']);
                }



                $user = \Modules\Marketer\Entities\Marketer::find($id)->update([
                    'parent_id' => $request->get('parent_id'),
                    'position' => $request->get('position')
                ]);

                if ($user) {
                    return response(['status' => true]);
                }

                return response(['status' => false, 'msg' => 'خطایی رخ داده است']);


            });

            Route::put('/{id}/password', function ($id, Request $request) {

                $validator = \Validator::make($request->all(), [
                    'password' => 'required|min:6',
                    'password_confirmation' => 'required| min:6'
                ]);

                if ($validator->fails()) {
                    return Response()->json(['status' => false, 'msg' => $validator->errors()->first()]);
                }

                $user = User::find($id)->update([
                    'password' => \Illuminate\Support\Facades\Hash::make($request->get('password')),
                    'remember_token' => quickRandom()
                ]);

                if ($user) {
                    return response(['status' => true]);
                }

                return response(['status' => false, 'msg' => 'خطایی رخ داده است']);


            });

            Route::get('/{id}/bank', function ($id) {

                $result = [
                    'shaba' => '',
                    'card_number' => '',
                    'account_number' => ''
                ];

                $info = \Illuminate\Support\Facades\DB::table('marketer_info')
                    ->select('shaba', 'card_number', 'account_number', 'bank')
                    ->where('marketer_id', $id)
                    ->first();

                if ($info) {
                    $result = $info;
                }

                return response(['status' => true, 'data' => $result]);

            });

            Route::put('/{id}/bank', function ($id,Request $request) {

                $checker = \Illuminate\Support\Facades\DB::table('marketer_info')->where('marketer_id', $id)->count();

                if ($checker == 1) {
                    \Illuminate\Support\Facades\DB::table('marketer_info')->where('marketer_id', $id)->update([
                        'shaba' => $request->get('shaba'),
                        'bank' => $request->get('bank'),
                        'account_number' => $request->get('account_number'),
                        'card_number' => $request->get('card_number'),
                    ]);
                } else {
                    \Illuminate\Support\Facades\DB::table('marketer_info')->insert([
                        'marketer_id' => $id,
                        'shaba' => $request->get('shaba'),
                        'bank' => $request->get('bank'),
                        'account_number' => $request->get('account_number'),
                        'card_number' => $request->get('card_number'),
                    ]);
                }


                return response(['status' => true]);


            });

            Route::get('/{id}/document', function ($id) {

                $data = \Illuminate\Support\Facades\DB::table('marketer_document')
                    ->where('marketer_id', $id)
                    ->get();

                return response(['status' => true, 'data' => $data]);
            });

            Route::put('/{id}/document/{type}', function ($id, $type, Request $request) {

               \Illuminate\Support\Facades\DB::table('marketer_document')
                    ->where('marketer_id', $id)
                    ->where('type', $type)
                    ->update([
                        'status' => $request->get('status')
                    ]);



                return response(['status' => true]);
            });

            Route::get('/{id}/basic', function ($id) {

                try {

                    $data = DB::select('call sp_marketer_basic_data(?)', [
                        $id
                    ]);

                    return response(['status' => true, 'data' => $data[0]]);

                } catch (Exception $exception) {
                    if ($exception instanceof \Illuminate\Database\QueryException) {
                        return response()->json(['status' => false, 'msg' => $exception->getPrevious()->errorInfo[2]]);
                    } else {
                        return response()->json(['status' => false, 'msg' => $exception->getMessage()]);
                    }
                }

            });

            Route::put('/{id}/basic', function ($id, Request $request) {

                $validator = \Validator::make($request->all(), [
                    'gender' => 'required',
                ]);

                if ( $validator->fails() ) {
                    return Response()->json(['status' => false, 'msg' => $validator->errors()->first()]);
                }


                \Illuminate\Support\Facades\DB::table('user')->where('id', $id)->update([
                    'name' => $request->get('name'),
                    'family' => $request->get('family'),
                ]);

                $checker = \Illuminate\Support\Facades\DB::table('marketer_info')->where('marketer_id', $id)->count();

                if ($checker == 1) {
                    $model = \Illuminate\Support\Facades\DB::table('marketer_info')->where('marketer_id', $id)->update([
                        'father_name' => $request->get('father_name'),
                        'education' => $request->get('education'),
                        'gender' => $request->get('gender')  ?? null,
                        'heir_name' => $request->get('heir_name') ?? null,
                        'heir_last_name' => $request->get('heir_last_name') ?? null,
                        'heir_national_code' => $request->get('heir_national_code') ?? null,
                        'heir_relation' => $request->get('heir_relation') ?? null,
                        'birth_date' => $request->get('birth_date') ?? null,
                    ]);
                } else {
                    $model = \Illuminate\Support\Facades\DB::table('marketer_info')->insertGetId([
                        'marketer_id' => $id,
                        'father_name' => $request->get('father_name'),
                        'education' => $request->get('education'),
                        'gender' => $request->get('gender'),
                        'heir_name' => $request->get('heir_name') ?? null,
                        'heir_last_name' => $request->get('heir_last_name') ?? null,
                        'heir_national_code' => $request->get('heir_national_code') ?? null,
                        'heir_relation' => $request->get('heir_relation') ?? null,
                        'birth_date' => $request->get('birth_date') ?? null,
                    ]);
                }


                return response(['status' => true]);


            });

            Route::get('/{id}/unique', function ($id) {

                try {

                    $data = DB::select('call sp_marketer_unique_data(?)', [
                        $id
                    ]);

                    return response(['status' => true, 'data' => $data[0]]);

                } catch (Exception $exception) {
                    if ($exception instanceof \Illuminate\Database\QueryException) {
                        return response()->json(['status' => false, 'msg' => $exception->getPrevious()->errorInfo[2]]);
                    } else {
                        return response()->json(['status' => false, 'msg' => $exception->getMessage()]);
                    }
                }

            });

            Route::put('/{id}/unique', function ($id,Request $request) {

                $validator = \Validator::make($request->all(), [
                    'username' => 'required',
                    'mobile' => 'required',
                    'national_code' => 'required|min:10|max:10',
                ]);

                if ($validator->fails()) {
                    return Response()->json(['status' => false, 'msg' => $validator->errors()->first()]);
                }

                $username = \Modules\User\Entities\User::where('username', $request->get('username'))->where('id', '<>', $id)->count();

                if ($username > 0) {
                    return Response()->json(['status' => false, 'msg' => 'نام کاربری قبلا در سیستم ثبت شده است.']);
                }

                $mobile = User::where('mobile', $request->get('mobile'))->where('id', '<>', $id)->count();

                if ($mobile > 0) {
                    return Response()->json(['status' => false, 'msg' => 'موبایل قبلا در سیستم ثبت شده است.']);
                }

                if ($request->get('email')) {

                    $email = User::where('email', $request->get('email'))->where('id', '<>', $id)->count();

                    if ($email > 0) {
                        return Response()->json(['status' => false, 'msg' => 'ایمیل قبلا در سیستم ثبت شده است.']);
                    }
                }


                $national_code = \Illuminate\Support\Facades\DB::table('marketer_info')
                    ->where('national_code', $request->get('national_code'))
                    ->where('marketer_id', '<>', $id)
                    ->count();

                if ($national_code > 0) {
                    return Response()->json(['status' => false, 'msg' => 'کد ملی قبلا در سیستم ثبت شده است.']);
                }


                \Illuminate\Support\Facades\DB::table('user')->where('id', $id)->update([
                    'username' => $request->get('username'),
                    'mobile' => $request->get('mobile'),
                    'email' => $request->get('email'),
                ]);

                $checker = \Illuminate\Support\Facades\DB::table('marketer_info')->where('marketer_id', $id)->count();

                if ($checker == 1) {
                    \Illuminate\Support\Facades\DB::table('marketer_info')->where('marketer_id', $id)->update([
                        'national_code' => $request->get('national_code'),
                    ]);
                } else {
                    \Illuminate\Support\Facades\DB::table('marketer_info')->insert([
                        'marketer_id' => $id,
                        'national_code' => $request->get('national_code'),
                    ]);
                }


                return response(['status' => true]);


            });
        });

        Route::get('/autocomplete', function (Request $request) {
            $response = \Modules\User\Entities\User::select('id', \Illuminate\Support\Facades\DB::raw('concat(name, " ", family) as name'), 'mobile')
                ->where(\Illuminate\Support\Facades\DB::raw('concat(name, " ", family)'), 'like', '%' . $request->get('term') . '%')
                ->orWhere('mobile', 'like', '%' . $request->get('term') . '%')
                ->orWhere('username', 'username', '%' . $request->get('term') . '%')
                ->where("status", 1)
                ->take(10)
                ->get();
            return response($response);
        });

        Route::get('/', 'MarketerController@index');
        Route::post('/', 'MarketerController@store');
        Route::get('/{id}', 'MarketerController@show');
        Route::put('/{id}', 'MarketerController@update');
        Route::put('/{id}/password', 'MarketerController@changePassword');
        Route::put('/{id}/status', 'MarketerController@changeStatus');
        Route::put('/{id}/getMarketerCode', 'MarketerController@getMarketerCode');
        Route::get('/{id}/sales-daily-report', 'MarketerController@marketerSalesDailyReport');
        Route::get('/{id}/mapReports', 'MarketerController@marketerMapReports');
        Route::post('/{id}/revoke', 'MarketerController@revoke');
        Route::get('/{id}/address', 'MarketerController@address');
        Route::post('/{id}/address', 'MarketerController@addAddress');

        Route::get('/{id}/info', function ($id) {

            try {

                $response = \Illuminate\Support\Facades\DB::select("call sp_marketer_info($id)");

                return response(['status' => true, 'info' => is_array($response) ? $response[0] : []]);

            } catch (Exception $exception) {
                return response($exception->getMessage());
            }
        });


    });
});
