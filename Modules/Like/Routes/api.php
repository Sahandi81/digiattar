<?php

use Illuminate\Support\Facades\Route;
use Modules\Like\Http\Controllers\LikeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function (){

	Route::prefix('like')->group(function (){
		Route::post('{id}',					[LikeController::class, 'store'])		->name('like_product_by_client');
		Route::delete('{id}',				[LikeController::class, 'destroy'])		->name('unlike_product_by_client');
	});

});
