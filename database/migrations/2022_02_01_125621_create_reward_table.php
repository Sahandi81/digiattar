<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRewardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reward', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('marketer_id');
            $table->integer('period_id');
            $table->enum('type', ['retail', 'reagent', 'reward_as_reward']);
            $table->bigInteger('amount')->default(0);
            $table->boolean('status')->default(false);
            $table->timestamp('created_at')->useCurrentOnUpdate()->useCurrent();
            $table->softDeletes();

            $table->index(['marketer_id', 'period_id'], 'marketer_id_period_id');
            $table->unique(['marketer_id', 'period_id', 'type'], 'marketer_id_period_id_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reward');
    }
}
