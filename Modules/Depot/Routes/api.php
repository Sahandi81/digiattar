<?php

use Carbon\Carbon;
use Illuminate\Http\Request;
use Modules\Order\Entities\Order;
//use PDF;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('backend')->group(function () {

    Route::prefix('depot')->middleware(['auth:api', 'admin_access'])->group(function () {

        Route::get('/', 'DepotController@index');

        Route::get('/{id}', function ($id) {

            $response = \Illuminate\Support\Facades\DB::select('call sp_depot_cumulative_report(?)', [$id]);

            return response($response);

        });



        Route::get('/{id}/orders', function ($id) {

            $response = \Illuminate\Support\Facades\DB::select('call sp_depot_order_list(?)', [$id]);

            return response($response);

        });

        Route::put('/{id}/orders', function ($id, Request $request) {


            \Illuminate\Support\Facades\DB::table('depot_order')
                ->where('depot_id', $id)
                ->where('order_id', $request->get('order_id'))
                ->update([
                    'deleted_at' =>  $request->get('deleted') == 1 ? Carbon::now() : null  # :(( hey
                ]);

            $response = \Illuminate\Support\Facades\DB::select('call sp_depot_order_list(?)', [$id]);

            return response($response);

        });

        Route::get('/{id}/orders/{order}/{type}', function ($id, $order, $type) {


            $response = \Illuminate\Support\Facades\DB::select('call sp_order_info(?)', [$order]);

            if ($type && $type == 'label') {
                \Illuminate\Support\Facades\DB::table('depot_order')
                    ->where('depot_id', $id)
                    ->where('order_id', $order)
                    ->increment('label');
            }

            if ($type && $type == 'factor') {
                \Illuminate\Support\Facades\DB::table('depot_order')
                    ->where('depot_id', $id)
                    ->where('order_id', $order)
                    ->increment('factor');

            }

            return response($response);

        });

        Route::put('/{id}/accumulative', function ($id) {

            $response = \Illuminate\Support\Facades\DB::select('call sp_depot_cumulative_report(?)', [$id]);

            \Modules\Depot\Entities\Depot::find($id)->increment('accumulative');

            return response($response);

        });

        Route::get('/{id}/accumulative/orders/{type?}', function ($id, $type) {

            $output = [];

            if ($type && $type == 'label') {
                \Modules\Depot\Entities\Depot::find($id)->increment('label');
            }

            if ($type && $type == 'factor') {
                \Modules\Depot\Entities\Depot::find($id)->increment('factor');
            }

            $orders = \Illuminate\Support\Facades\DB::table('depot_order')
                ->where('depot_id', $id)

                ->get();



            foreach ($orders as $order) {
                $output[] = \Illuminate\Support\Facades\DB::select('call sp_order_info(?)', [$order->order_id])[0];
            }

            return response($output);

        });

        Route::put('/{id}/exit', function ($id) {

            \Modules\Depot\Entities\Depot::find($id)->update([
                'exit' => 1
            ]);

            return response(['status' => true]);

        });

    });
});

