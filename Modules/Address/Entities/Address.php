<?php

namespace Modules\Address\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Modules\Region\Entities\Region;

class Address extends Model
{
	# New update, use `rebuild` api to change columns from deleted to deleted_at
	use SoftDeletes;
    use HasFactory;

    protected $table = 'address';

    protected $guarded = [];

    protected static function newFactory()
    {
        return \Modules\Address\Database\factories\AddressFactory::new();
    }

    public function region()
    {
        return $this->belongsTo(Region::class, 'region_id');
    }
}
